import re


class NumberParsingException(Exception):
    pass


def can_parse_number(num: str) -> bool:
    try:
        parse_number(num)
    except:
        return False
    else:
        return True


def parse_number(num: str) -> float:
    if re.match(r'^[0-9]+(.[0-9]+)?$', num):
        return float(num)
    if num == '½':
        return 0.5
    elif num == '¼':
        return 0.25
    elif num == '¾':
        return 0.75
    raise NumberParsingException(num)
