(ns cljs-frontend.core
  (:require-macros [cljs.core.async.macros :refer [go]])
  (:require [reagent.core :as reagent :refer [atom]]
            [cljs-http.client :as http]
            [clojure.string :as str]
            [cljs.core.async :refer [<!]]
            [stylefy.core :as stylefy :refer [use-style]]
            [stylefy.reagent :as stylefy-reagent]
            ["moment" :as moment]))

(enable-console-print!)

; for developing on independent server:
; (def base-url "http://localhost:5000")
(def base-url "")

(defonce app-state (atom {:stock-map nil}))

; for each ingredient:
;   {:enough true/false,
;    :amount (number),
;    :unit (1-5)}

; TODO: should only be dirty if it's not in its original state
(defn set-dirty
  ([ingredient-id] (set-dirty ingredient-id true))
  ([ingredient-id new-dirty]
   (swap! app-state #(assoc-in % [:stock-map ingredient-id :dirty] new-dirty))))

(defn set-enough
  [ingredient-id new-enough]
  (.log js/console (str "change enough for " ingredient-id " to " new-enough))
  (swap! app-state #(assoc-in % [:stock-map ingredient-id :enough] new-enough))
  (set-dirty ingredient-id))

(defn set-comment
  [ingredient-id new-comment]
  (assert (string? new-comment))
  (swap! app-state #(assoc-in %
                      [:stock-map ingredient-id :comment]
                      new-comment))
  (set-dirty ingredient-id))

(defn set-amount
  [ingredient-id new-amount]
  (assert (number? new-amount))
  (swap! app-state #(assoc-in % [:stock-map ingredient-id :amount] new-amount))
  (set-dirty ingredient-id))

(defn set-unit
  [ingredient-id new-unit]
  (swap! app-state #(assoc-in % [:stock-map ingredient-id :unit] new-unit))
  (set-dirty ingredient-id))

(defn ingredient-state->json
  [ingredient-id ingredient-state]
  {:ingredient_id ingredient-id,
   :comment (:comment ingredient-state),
   :amount (if (:enough ingredient-state)
             "enough"
             (:amount (double ingredient-state))),
   :unit (:unit ingredient-state)})

(defn json-for-ingredient
  [ingredient-id]
  (let [ingredient-state (get-in @app-state [:stock-map ingredient-id])]
    (ingredient-state->json ingredient-id ingredient-state)))

; json:
;  {:/ingredient/123 {:amount "enough"}
;   :/ingredient/124 {:amount 123 :unit 2 :comment "hello"}}
;
; output:
;  {:/ingredient/123 {:enough true :amount 0 :unit 0}
;   :/ingredient/124 {:enough false :amount 0 :unit 0}}
;
; plus also: comment, timestamp (seconds since epoch)
(def default-unit 2); grams

(defn ingredient->state
  [ingredient-state]
  {:enough (= (:amount ingredient-state) "enough"),
   :amount (let [amount (:amount ingredient-state)]
             (if (number? amount) amount 0)),
   :unit (:unit ingredient-state default-unit),
   :comment (:comment ingredient-state ""),
   :timestamp (:timestamp ingredient-state)
   ;:dirty false
  })

(defn save-stock
  [ingredient-id]
  (go (let [response (<! (http/post (str base-url "/stock")
                                    {:json-params (json-for-ingredient
                                                    ingredient-id),
                                     :with-credentials? false}))]
        (.log js/console
              "response status" (:status response)
              "body" (:body response))
        (set-dirty ingredient-id false)
        (swap! app-state #(assoc-in %
                            [:stock-map ingredient-id]
                            (ingredient->state ((:body response) :state)))))))

(defn set-amount-x
  [ingredient-id s]
  (cond (and (string? s) (empty? s)) (.log js/console
                                           "ignoring update to empty string")
        :else (do (.log js/console "let's try to parse it as number...")
                  (set-amount ingredient-id (js/parseFloat s)))))

; Shows an amount. Ingredient state is:
;   {:/ingredient/123 {:enough true :amount 0 :unit 0}}
; call with: (amount-widget ingredient-id ingredient-state)
(defn ingredient-row
  [ingredient-id {:keys [enough unit amount dirty comment timestamp]}]
  [:tr
   [:td
    [:a {:href (str "/ingredient/" ingredient-id)}
     (str/replace ingredient-id "/ingredient" "")]]
   [:td
    (if timestamp
      (let [updated-moment (.unix moment timestamp)
            now-moment (moment)]
        [:span {:title (.format updated-moment)}
         (.from updated-moment now-moment)])
      "never")]
   ; TODO: "save all" button
   [:td
    [:input
     {:type "text",
      :placeholder "Comment",
      :default-value comment,
      :on-change #(set-comment ingredient-id
                               (-> %
                                   .-target
                                   .-value))}]
    [:label
     [:input
      {:type "checkbox",
       :default-checked enough,
       :on-change #(set-enough ingredient-id
                               (-> %
                                   .-target
                                   .-checked))}] "∞"]
    [:input
     (use-style {:width "4em"}
                {:type "number",
                 :step "any",
                 :default-value amount,
                 :disabled enough,
                 :min 0,
                 :on-change #(set-amount-x ingredient-id
                                           (-> %
                                               .-target
                                               .-value))})]
    [:select
     {:on-change #(set-unit ingredient-id
                            (-> %
                                .-target
                                .-value)),
      :disabled enough,
      :default-value unit}
     (for [[this-unit this-unit-name] {1 "ml", 2 "g", 3 "piece"}]
       ^{:key [ingredient-id this-unit]}
       [:option {:value this-unit} this-unit-name])]
    [:button {:on-click #(save-stock ingredient-id), :disabled (not dirty)}
     "Save"]]
   ;[:button
   ; {:on-click #(js/alert (str "delete " ingredient-id))}
   ; "Delete"]
  ])

; ugh...
(defn keyword-to-str [kw] (subs (str kw) 1))

(def empty-ingredient-state
  {:amount 0, :unit default-unit, :comment "", :timestamp nil, :enough false})

(defn json-to-state
  [json ingredients-json]
  (merge (zipmap ingredients-json (repeat empty-ingredient-state))
         (into {}
               (for [[ingredient-id ingredient-state] json]
                 [(keyword-to-str ingredient-id)
                  (ingredient->state ingredient-state)]))))

(defn fetch-stock
  []
  (go (let [response (<! (http/get (str base-url "/stock.json")
                                   {:with-credentials? false}))
            ingredients-response (<! (http/get (str base-url "/ingredient")
                                               {:json-params {},
                                                :with-credentials? false}))]
        (reset! app-state
                {:stock-map (json-to-state (:body response)
                                           (:body ingredients-response))}))))

(defn ingredient-stocked?
  [{:keys [amount comment enough dirty]}]
  (or (pos? amount) (not-empty comment) enough dirty))

(defn matches-filter?
  [ingredient-id ingredient-data filter]
  (if (empty? filter)
    ; with no filter, hide ingredients that are not interesting
    (ingredient-stocked? ingredient-data)
    ; with a filter, show even ingredients that are not currently present
    (str/includes? ingredient-id filter)))

; TODO: if filter is non-empty, show *all* ingredients that we *could* insert,
; not just those that are in stock.
(defn change-filter
  [new-filter]
  (swap! app-state #(assoc % :filter new-filter)))

(defn hello-world
  []
  [:div [:button {:on-click fetch-stock} "Fetch state"]
   [:input
    {:type "text",
     :default-value (:filter @app-state),
     :placeholder "Filter...",
     :on-change #(change-filter (-> %
                                    (.-target)
                                    (.-value)))}]
   (if-let [stock-map (:stock-map @app-state)]
     [:table [:thead [:tr [:th "Ingredient"] [:th "Update"] [:th "Controls"]]]
      ; TODO(agentydragon): sort the ingredients
      [:tbody
       (doall (for [[ingredient-id ingredient-data]
                      ; TODO(agentydragon): sort can be more sophisticated
                      (sort-by #(:timestamp (second %)) (vec stock-map))
                    :when (matches-filter? ingredient-id
                                           ingredient-data
                                           (:filter @app-state))]
                ; reagent item key for improving performance
                ; TODO: test square brackets work
                ^{:key ingredient-id}
                [ingredient-row ingredient-id ingredient-data]))]]
     "Not fetched yet.")])

(defn start
  []
  (stylefy/init {:dom (stylefy-reagent/init)})
  (reagent/render-component [hello-world]
                            (. js/document (getElementById "app")))
  (fetch-stock))

(defn ^:export init
  []
  ;; init is called ONCE when the page loads
  ;; this is called in the index.html and must be exported
  ;; so it is available even in :advanced release builds
  (start))

(defn stop
  []
  ;; stop is called before any code is reloaded
  ;; this is controlled by :before-load in the config
  (js/console.log "stop"))
