import hamcrest
import proto_matcher
from absl.testing import absltest
from google.protobuf import text_format
from meal_scheduler.data import conversions as conversions_lib
from meal_scheduler.data import ingredient_types as ingredient_types_lib
from meal_scheduler.inventory_py_proto_pb import inventory_pb2
from meal_scheduler.unit_py_proto_pb import unit_pb2


class ConversionsTest(absltest.TestCase):

    def test_convert_via_supertype(self):
        """Test that conversion via supertype is available for subtypes."""
        ingredient_database = ingredient_types_lib.IngredientDatabase(
            ingredient_types=[
                inventory_pb2.IngredientType(id="/bell_pepper/red"),
                inventory_pb2.IngredientType(id="/bell_pepper/generic",
                                             subtypes=["/bell_pepper/red"]),
            ],
            ingredient_names=[],
            ingredient_matter_rules=[])
        conversions = text_format.Parse(
            r"""
      conversion {
        ingredient_id: "/bell_pepper/generic"
        equivalent { unit: PIECE amount: 1 }
        equivalent { unit: GRAM amount: 200 }
      }
      """, inventory_pb2.Conversions())
        unit_amount = text_format.Parse(
            r"""
      unit: PIECE
      amount: 3
    """, inventory_pb2.UnitAmount())
        expected_unit_amount = text_format.Parse(
            r"""
      unit: GRAM
      amount: 600
    """, inventory_pb2.UnitAmount())
        hamcrest.assert_that(
            conversions_lib.convert_unit_amount_to_unit(unit_amount,
                                                        unit_pb2.GRAM,
                                                        conversions,
                                                        ingredient_database,
                                                        "/bell_pepper/red"),
            proto_matcher.equals_proto(expected_unit_amount))


if __name__ == '__main__':
    absltest.main()
