import collections

from absl.testing import absltest
from google.protobuf import text_format
from meal_scheduler import unit as unit_lib
from meal_scheduler.db import db as db_lib
from meal_scheduler.db import init as init_lib
from meal_scheduler.inventory_py_proto_pb import inventory_pb2
from meal_scheduler.parsing import parse_recipe_lib
from meal_scheduler.unit_py_proto_pb import unit_pb2
from rules_python.python.runfiles import runfiles


class ParseRecipeTest(absltest.TestCase):

    def setUp(self):
        self._db = db_lib.open_db(":memory:")
        init_lib.init_db(self._db)

        self._used_ingredient_name_map = collections.Counter()

    def _parse(self, plaintext_fields):
        ingredient_database = db_lib.load_ingredient_types(self._db)
        ingredient_name_to_id = ingredient_database.name_to_id
        return parse_recipe_lib.parse_plaintext_fields(
            ingredient_database, plaintext_fields, ingredient_name_to_id,
            self._used_ingredient_name_map)

    def test_parse_salt_unit(self):
        db_lib.insert_ingredient_type(
            self._db, inventory_pb2.IngredientType(id="/ingredient/salt"))
        db_lib.insert_ingredient_name(
            self._db,
            inventory_pb2.IngredientName(name=["Salz"],
                                         ingredient_id="/ingredient/salt"))

        plaintext_fields = text_format.Parse(
            r"""
        plaintext_ingredients {
          unit_plaintext: "Einheit"
          ingredient_plaintext: "Salz*"
        }
        """, inventory_pb2.PlaintextRecipeFields())
        recipe = self._parse(plaintext_fields)
        # TODO(agentydragon): check result is salt

    def test_garlic_half_clove(self):
        db_lib.insert_ingredient_type(
            self._db,
            inventory_pb2.IngredientType(id="/ingredient/vegetable/garlic"))

        plaintext_fields = text_format.Parse(
            r"""
        plaintext_ingredients {
          amount_plaintext: "0.5"
          ingredient_plaintext: "Knoblauchzehe"
        }
        """, inventory_pb2.PlaintextRecipeFields())
        recipe = self._parse(plaintext_fields)
        # TODO(agentydragon): check result is garlic, 0.5 clove


if __name__ == '__main__':
    absltest.main()
