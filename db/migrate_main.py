import os.path

from absl import app, flags, logging
from alembic import command, context
from alembic.config import Config
from alembic.runtime.environment import EnvironmentContext
from alembic.script import ScriptDirectory
from meal_scheduler.db import db as db_lib
from rules_python.python.runfiles import runfiles as runfiles_lib

_SQLITE_DB = flags.DEFINE_string("sqlite_db", None, "path to sqlite db")

_MIGRATIONS_MARK_PATH = 'meal_scheduler/db/migrations/MARK'


def main(_):
    runfiles = runfiles_lib.Create()
    location = runfiles.Rlocation(_MIGRATIONS_MARK_PATH)
    logging.info("loc: %s", location)
    assert location
    path = os.path.dirname(location)
    config = Config()
    config.set_main_option("script_location", path)
    config.set_main_option("version_locations", path)

    db = db_lib.open_db(_SQLITE_DB.value)
    # TODO: configurable upgrade/downgrade, and target

    # https://github.com/zzzeek/alembic/blob/322d8312d21b031b17044565b5e8cfc9ee7be141/alembic/command.py#L263
    script = ScriptDirectory.from_config(config)

    destination_revision = "head"

    def upgrade(rev, context):
        return script._upgrade_revs(destination_revision, rev)

    with EnvironmentContext(
            config,
            script,
            fn=upgrade,
            #as_sql=sql,
            as_sql=False,
            #tag=tag,
    ):
        context.configure(connection=db, target_metadata=None)

        with context.begin_transaction():
            context.run_migrations()

    db.close()


if __name__ == '__main__':
    flags.mark_flags_as_required([_SQLITE_DB.name])
    app.run(main)
