"""
bazel run //db:import_history_main -- \
  --alsologtostderr \
  --sqlite_db=$(pwd)/db.sqlite \
  --history=$(pwd)/history.textproto
"""

from absl import app, flags
from google.protobuf import text_format
from meal_scheduler.db import db as db_lib
from meal_scheduler.inventory_py_proto_pb import inventory_pb2

flags.DEFINE_string("sqlite_db", None, "path to sqlite db")
flags.DEFINE_string("history", None, "textproto to insert history from")

FLAGS = flags.FLAGS


def main(_):
    db = db_lib.open_db(FLAGS.sqlite_db)
    with open(FLAGS.history, 'rb') as f:
        proto = text_format.Parse(f.read(), inventory_pb2.History())
    for meal in proto.past_cooked_meal:
        db_lib.insert_past_cooked_meal(db, meal)
    db.close()


if __name__ == '__main__':
    flags.mark_flags_as_required(['sqlite_db', 'history'])
    app.run(main)
