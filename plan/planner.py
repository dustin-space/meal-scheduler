import collections
import dataclasses
from typing import Dict, List, Sequence

import z3
from absl import logging
from meal_scheduler.plan.planner_py_proto_pb.plan import planner_pb2


@dataclasses.dataclass
class _RecipeToCook:
    recipe_id: int
    portions: int
    per_ingredient_use: Dict[int, int]


def count_positives(array, length):
    return z3.Sum([z3.IntSort().cast(array[i] > 0) for i in range(length)])


def sum_array(array, length):
    return z3.Sum([array[i] for i in range(length)])


def count_equal(array, length, equal):
    return z3.Sum([z3.IntSort().cast(array[i] == equal) for i in range(length)])


class _Planner:

    def __init__(self, num_ingredients, recipes, products, s: List[int]):
        """
        Args:
          recipes: [{{ingredient}: amount, {optionA, optionB}: amount, ...}, ...]
          products: [{ingredient: amount, ...}, ...]
          s: [{ingredient: amount, ...}, ...]
        """
        logging.info("recipes: %s", recipes)
        self._solver = z3.Optimize()
        self._num_ingredients = num_ingredients
        self._recipes = recipes
        self._products = products
        self._add_product_order_units()
        self._num_recipes = len(recipes)

        self._available_in_stock = s
        self._add_bought_amounts(products)
        self._add_recipe_portions()

        # how much of each ingredient should we obtain externally; to be minimized
        # ingredient_to_obtain_externally[ingredient_id] = AMOUNT
        self._ingredient_to_obtain_externally = z3.Array(
            'ingredient_obtain_externally', z3.IntSort(), z3.IntSort())

        self._add_ingredient_vars()

    def _add(self, constraint):
        self._solver.add(constraint)

    def _add_recipe_portions(self):
        # how many portions of each recipe we want to cook
        # O(N) variables
        self._recipe_portions = z3.Array('recipe_portions', z3.IntSort(),
                                         z3.IntSort())
        # [recipe][ingredient] = amount
        self._recipe_ingredient_use = z3.Array(
            'recipe_ingredient_use', z3.IntSort(),
            z3.ArraySort(z3.IntSort(), z3.IntSort()))
        for i, recipe_portions, recipe in zip(range(self._num_recipes),
                                              self._recipe_portions,
                                              self._recipes):
            self._add(recipe_portions >= 0)
            for j, ingredient_use in zip(range(self._num_ingredients),
                                         self._recipe_ingredient_use[i]):
                self._add(ingredient_use == recipe_portions * recipe.get(j, 0))

    def _add_product_order_units(self):
        # how many of each product will we order
        # O(P) variables
        self._product_order_units = z3.Array('product_order_units',
                                             z3.IntSort(), z3.IntSort())
        for _, product_order_units in zip(self._products,
                                          self._product_order_units):
            self._add(product_order_units >= 0)
            # maximum of products to buy
            self._add(product_order_units <= 30)

    def _add_bought_amounts(self, products):
        self._available_from_bought_products = [
            z3.Sum([
                order_units * product.get(i, 0) for product, order_units in zip(
                    products, self._product_order_units)
            ]) for i in range(self._num_ingredients)
        ]
        self._needed = z3.Array('needed', z3.IntSort(), z3.IntSort())

    def _add_ingredient_vars(self):
        for i, (obtained_externally, in_stock, bought, needed) in enumerate(
                zip(self._ingredient_to_obtain_externally,
                    self._available_in_stock,
                    self._available_from_bought_products, self._needed)):
            # TODO(prvak): check that rounding did not lose too much.
            # TODO(agentydragon): handle min-max-range recipes more correctly
            self._add(needed == z3.Sum([
                self._recipe_ingredient_use[j][i]
                for j in range(self._num_recipes)
            ]))

            available_total = bought + in_stock

            self._add(obtained_externally >= 0)
            # if there's no LeShop product, allow pretending we can buy it.
            # and (ingredient_id not in products.not_sold_ingredient_ids):
            if not any(i in product for product in self._products):
                available_total += obtained_externally
                # TODO: range should be more reasonable
                self._add(
                    z3.Implies(obtained_externally > 0,
                               available_total == needed))
            else:
                self._add(obtained_externally == 0)

            # TODO(agentydragon): allow the solver to pretend there are also
            # conversions?

            self._add(available_total >= needed)

    def limit_added_ingredients(self, max_added_ingredients):
        num_added_ingredients = count_positives(
            self._ingredient_to_obtain_externally, self._num_ingredients)
        self._add(num_added_ingredients <= max_added_ingredients)

    def max_of_recipes(self, recipe_indices: Sequence[int], max_count: int):
        self._add(
            z3.Sum([
                z3.IntSort().cast(self._recipe_portions[i] > 0)
                for i in recipe_indices
            ]) <= max_count)

    def set_portion_counts(self, portion_counts: Dict[int, int]):
        for portions, cnt in portion_counts.items():
            # there must be exactly 'cnt' meals that are cooked for 'portions'
            # portions.
            self._add(
                count_equal(self._recipe_portions, self._num_recipes, portions)
                == cnt)

    def minimize_products_bought(self):
        if self._products:
            products_bought = z3.Sum(
                [x for _, x in zip(self._products, self._product_order_units)])
            logging.info("products_bought=%s", products_bought)
            self._solver.minimize(products_bought)
        else:
            logging.info(
                "no products that could be bought, not optimizing for it")

    def _get_recipes_to_cook(self) -> List[_RecipeToCook]:
        recipes_to_cook = []
        for i, portions_var, riu_var in zip(range(self._num_recipes),
                                            self._recipe_portions,
                                            self._recipe_ingredient_use):
            portions = self._eval_long(portions_var)
            if portions == 0:
                continue

            recipe = _RecipeToCook(recipe_id=i,
                                   portions=portions,
                                   per_ingredient_use={})

            for j, ingredient_var in zip(range(self._num_ingredients), riu_var):
                pcs = self._eval_long(ingredient_var)
                if pcs == 0:
                    continue
                recipe.per_ingredient_use[j] = pcs
            recipes_to_cook.append(recipe)
        return recipes_to_cook

    def plan(self):
        result = self._solver.check()
        assert result == z3.sat
        #logging.info("check result: %s", result)
        self._model = self._solver.model()
        #logging.info("model: %s", self._model)

    def _eval_long(self, var):
        return self._model.eval(var, model_completion=True).as_long()

    def get_plan_proto(self) -> planner_pb2.Plan:
        return planner_pb2.Plan(
            ingredient=[
                planner_pb2.PlanIngredient(
                    total_use=self._eval_long(total_use),
                    obtain_externally=self._eval_long(obtain_externally))
                for _, total_use, obtain_externally in zip(
                    range(self._num_ingredients), self._needed,
                    self._ingredient_to_obtain_externally)
            ],
            products=[
                self._eval_long(product_amount) for product_amount, _product in
                zip(self._product_order_units, self._products)
            ],
            meal=[
                planner_pb2.PlanMeal(recipe=recipe_to_cook.recipe_id,
                                     portions=recipe_to_cook.portions,
                                     ingredient_use=[
                                         recipe_to_cook.per_ingredient_use.get(
                                             i, 0)
                                         for i in range(self._num_ingredients)
                                     ])
                for recipe_to_cook in self._get_recipes_to_cook()
            ])


def plan(plan_input: planner_pb2.Input) -> planner_pb2.Output:
    ingredients_in_recipes = {
        item.ingredient for recipe in plan_input.recipe for item in recipe.item
    }
    ingredients_in_products = {
        content.ingredient
        for product in plan_input.product
        for content in product.content
    }
    num_ingredients = max(
        set.union(ingredients_in_recipes, ingredients_in_products)) + 1
    planner = _Planner(
        num_ingredients=num_ingredients,
        recipes=[{item.ingredient: item.amount
                  for item in recipe.item}
                 for recipe in plan_input.recipe],
        products=[{
            content.ingredient: content.amount
        } for product in plan_input.product for content in product.content],
        s=[ingredient.stock for ingredient in plan_input.ingredient])
    if plan_input.max_ingredients_obtained_externally >= 0:
        planner.limit_added_ingredients(
            plan_input.max_ingredients_obtained_externally)
    for max_of_recipes in plan_input.max_of_recipes:
        planner.max_of_recipes(max_of_recipes.recipe, max_of_recipes.max_meals)
    planner.set_portion_counts(
        collections.Counter(meal_request.portion_count
                            for meal_request in plan_input.meal_request))

    for i, var in zip(range(planner._num_recipes), planner._recipe_portions):
        if i in plan_input.banned_recipe:
            planner._solver.add(planner._recipe_portions[i] == 0)

    planner.minimize_products_bought()
    #logging.info("solver: %s", planner._solver)

    planner.plan()

    plan = planner.get_plan_proto()

    return planner_pb2.Output(plan=plan)
