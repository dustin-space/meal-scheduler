# meal-scheduler

## Update cljs

```
cd cljs-frontend
yarn run shadow-cljs release app
cd ..
cp cljs-frontend/public/js/compiled/main.js frontend/static/stock_index.js
```

## Getting started

Before checking out this repository, install Githooks
(https://github.com/gabyx/githooks). That should make sure pre-commit hooks run
for you.

Install Bazel from http://bazel.io.

The Python that runs when you launch `python` needs to be at least version 3.8.
This is the default on newer Ubuntu. On older Ubuntu, you can run:

```bash
apt install python-is-python3
```

You need Bizon and Flex (to build Minizinc):

```bash
sudo apt install bison flex
```

## Creating and populating a database

```bash
bazel run //db:init -- \
  --alsologtostderr \
  --sqlite_db=$(pwd)/db.sqlite
```

### Import recipes from repo

```bash
bazel build //data:recipe_import.textproto

bazel run //db:import_recipes_main -- \
  --alsologtostderr \
  --sqlite_db=$(pwd)/db.sqlite \
  --insert_recipes_from=$(git rev-parse --show-toplevel)/bazel-bin/data/recipe_import.textproto
```

## Generating a plan

Use the frontend to try generating a plan.

## Running celery worker

```bash
bazel run //frontend:celery_app_main -- \
  --alsologtostderr \
  --sqlite_db=$(pwd)/db.sqlite
```

## Adding data

### Adding recipes

Recipes are stored in `data/recipes/*.textproto`.
You can start by copying `data/recipes/cauliflower_curry.textproto`.

Make sure to fill out:

* `title`: title of the recipe
* `source.url`: where the recipe comes from
* `serves`: set `min` and `max` to the same number - the number of portions in
  the recipe.

Then, add ingredients. For each ingredient, you need to set `ingredient_id`
and `amount`.

Available units are listed in `unit.proto`.

Known `ingredient_id`'s are listed in `data/ingredient_types.textproto`.
If you don't find the ingredient you need in that file, just add it - see below.

TODO(agentydragon): Add test that all recipes in
`data/ingredient_types.textproto` are up to date.

#### Synchronizing into Dustin Space database

Once you add a new recipe, you also need to add it to the database.

### Adding ingredients

To add an ingredient, add a `ingredient` section to
`data/ingredient_types.textproto`.
Please keep the file sorted alphabetically by ingredient ID.

### Adding ingredient names

Ingredient names are stored in `data/ingredient_names.textproto`.
They are used for parsing semi-structured recipes from recipe sources.

TODO(agentydragon): Support more languages.

TODO(agentydragon): Describe more: what is a recipe source, etc.

### Adding a LeShop product

To add a LeShop product, open `data/leshop.textproto` and add a
`product` block. Set the product ID from the LeShop URL and set the ingredient
the product contains and how much of it.

If the product promises some exact weight, set amount with `amount_exact`, like
this:

```textproto
# Based on:
# https://www.leshop.ch/en/supermarket/fruits-vegetables/salad/salad-bags(smt:product/73494)
product {
  merchant_product_id : 73494  # comes from URL
  contents {
    # See data/ingredient_types.textproto to find matching ingredient.
    ingredient_id : "/ingredient/green/salad_mix"
    # LeShop description says it's 500 g.
    amount {unit : GRAM amount_exact : 500}
  }
}
```

### Checking correctness

TODO(agentydragon): Fix this.

TODO(agentydragon): Warn about missing universal units.

## Contributing

### Formatting code

```bash
pip3 install yapf
```

To check what diffs would yapf apply:

```bash
yapf --diff --recursive .
```

To change in-place:

```bash
yapf --in-place --recursive .
```

### Creating migration

```bash
openssl rand -hex 6
```

Create new file with this name comp.

### Migrating database to HEAD migration

```bash
bazel run //db:migrate_main -- --sqlite_db=(...)
```

### Checking versions of dependencies

Needs `requests`, `timeago`.

```bash
bazel run //:dep_versions -- --workspace=$(realpath WORKSPACE)
```
