import re


def _check_unique_ingredient_names(plan_input):
    assert len(set(
        ingredient.name for ingredient in plan_input.ingredient)) == len(
            plan_input.ingredient)


def _check_ingredient_name_format(plan_input):
    for ingredient in plan_input.ingredient:
        # Needs to be valid minizinc enum value name.
        assert re.fullmatch('[a-zA-Z_][0-9a-zA-Z_]+', ingredient.name)


def validate_plan_input(plan_input):
    _check_unique_ingredient_names(plan_input)
    _check_ingredient_name_format(plan_input)
