from typing import List

from google.protobuf import text_format
from meal_scheduler.db import db as db_lib
from meal_scheduler.inventory_py_proto_pb import inventory_pb2


class RecipeWrapper:

    def __init__(self, recipe: inventory_pb2.StoredRecipe):
        assert isinstance(recipe, inventory_pb2.StoredRecipe)
        self._stored_recipe = recipe

    @property
    def id(self):
        return self._stored_recipe.id

    @property
    def ingredient_ids(self):
        return {
            recipe_line.ingredient_id
            for recipe_line in self._stored_recipe.recipe.ingredients
        }

    def unit_amount_used(self, ingredient):
        for line in self._stored_recipe.recipe.ingredients:
            if line.ingredient_id == ingredient and line.amount.HasField(
                    'unit_amount'):
                return line.amount.unit_amount
        return None

    @property
    def importer_key(self):
        return self._stored_recipe.importer_key

    @property
    def unit_amount_ingredients(self):
        for line in self._stored_recipe.recipe.ingredients:
            if line.amount.HasField('unit_amount'):
                yield line.ingredient_id, line.amount.unit_amount

    @property
    def title(self):
        return self._stored_recipe.recipe.title

    @property
    def serves(self):
        return self._stored_recipe.recipe.serves

    @property
    def source(self):
        return self._stored_recipe.recipe.source

    @property
    def recipe_proto(self):
        return self._stored_recipe.recipe


def load_recipes_from_db(db) -> List[RecipeWrapper]:
    return [
        RecipeWrapper(stored_recipe)
        for stored_recipe in db_lib.load_all_recipes(db)
    ]


def find_recipe_by_id(recipes: List[RecipeWrapper], recipe_id) -> RecipeWrapper:
    for recipe in recipes:
        if recipe.id == recipe_id:
            return recipe
    raise KeyError(f"recipe {title} does not exist")
