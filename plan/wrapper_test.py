import functools
import os
from typing import List, Optional

import freezegun
import hamcrest
import proto_matcher
from absl import logging
from absl.testing import absltest, parameterized
from google.protobuf import text_format
from meal_scheduler import unit as unit_lib
from meal_scheduler.data import ingredient_types
from meal_scheduler.data import recipes as recipes_lib
from meal_scheduler.data import stock as stock_lib
from meal_scheduler.diet_mapping_py_proto_pb import diet_mapping_pb2
from meal_scheduler.inventory_py_proto_pb import inventory_pb2
from meal_scheduler.plan import cc_ortools_backend as cc_ortools_backend_lib
from meal_scheduler.plan import wrapper as wrapper_lib
from meal_scheduler.plan.minizinc import \
    minizinc_planner as minizinc_planner_lib
from meal_scheduler.plan import planner as planner_lib
from meal_scheduler.unit_py_proto_pb import unit_pb2


def _parse_ingredient_type(textproto):
    return text_format.Parse(textproto, inventory_pb2.IngredientType())


def _parse_stored_recipe(textproto):
    return text_format.Parse(textproto, inventory_pb2.StoredRecipe())


def _parse_stock_item(textproto):
    return text_format.Parse(textproto, inventory_pb2.StockItem())


def _parse_plan_request(textproto):
    return text_format.Parse(textproto, inventory_pb2.PlanRequest())


def _parse_product(textproto):
    return text_format.Parse(textproto, inventory_pb2.Product())


def _parse_diet_mappings(textproto):
    return text_format.Parse(textproto, diet_mapping_pb2.DietMappings())


EGG_INGREDIENT = inventory_pb2.IngredientType(id='/ingredient/egg')
SALT_INGREDIENT = inventory_pb2.IngredientType(id='/ingredient/salt')
APPLE_INGREDIENT = inventory_pb2.IngredientType(id='/ingredient/apple')
RED_APPLE_INGREDIENT = inventory_pb2.IngredientType(id='/ingredient/apple/red')
GENERIC_APPLE_INGREDIENT = inventory_pb2.IngredientType(
    id='/ingredient/apple/generic',
    subtypes=['/ingredient/apple/red'],
    canonical_unit=unit_pb2.GRAM)
STEAK_INGREDIENT = inventory_pb2.IngredientType(id='/ingredient/meat/steak')
SALT_STOCK = _parse_stock_item(r"""
    ingredient_id: "/ingredient/salt"
    amount { enough {} }""")
TWO_EGGS_STOCK = _parse_stock_item(r"""
    ingredient_id: "/ingredient/egg"
    amount { exact { unit: PIECE amount: 2 } }""")
TWO_APPLES_STOCK = _parse_stock_item(r"""
    ingredient_id: "/ingredient/apple"
    amount { exact { unit: PIECE amount: 2 } }""")
ONE_KILO_RED_APPLES_STOCK = _parse_stock_item(r"""
    ingredient_id: "/ingredient/apple/red"
    amount { exact { unit: GRAM amount: 1000 } }""")
FIFTY_GRAM_RED_APPLES_STOCK = _parse_stock_item(r"""
    ingredient_id: "/ingredient/apple/red"
    amount { exact { unit: GRAM amount: 50 } }""")
FIFTY_GRAM_GENERIC_APPLES_STOCK = _parse_stock_item(r"""
    ingredient_id: "/ingredient/apple/generic"
    amount { exact { unit: GRAM amount: 50 } }""")
TEN_STEAKS_STOCK = _parse_stock_item(r"""
    ingredient_id: "/ingredient/meat/steak"
    amount { exact { unit: PIECE amount: 10 } }""")
RAW_EGG_RECIPE = _parse_stored_recipe(r"""
    id: "raw-egg"
    recipe {
      title: "Raw egg"
      serves { min: 1 max: 1 }
      ingredients {
        ingredient_id: "/ingredient/egg"
        amount { unit_amount { unit: PIECE amount: 1 } }
      }
    }""")
RAW_EGG_WITH_SALT_RECIPE = _parse_stored_recipe(r"""
    id: "raw-egg-with-salt"
    recipe {
      title: "Raw egg with salt"
      serves { min: 1 max: 1 }
      ingredients {
        ingredient_id: "/ingredient/egg"
        amount { unit_amount { unit: PIECE amount: 1 } }
      }
      ingredients {
        ingredient_id: "/ingredient/salt"
        amount { to_taste {} }
      }
    }""")
HUNDRED_GRAM_RAW_APPLE_RECIPE = _parse_stored_recipe(r"""
    id: "raw-apple-100g"
    recipe {
      title: "Raw apple 100 grams"
      serves { min: 1 max: 1 }
      ingredients {
        ingredient_id: "/ingredient/apple/generic"
        amount { unit_amount { unit: GRAM amount: 100 } }
      }
    }""")
ONE_STEAK_RECIPE = _parse_stored_recipe(r"""
    id: "steak-1piece"
    recipe {
      title: "Steak 1 piece"
      serves { min: 1 max: 1 }
      ingredients {
        ingredient_id: "/ingredient/meat/steak"
        amount { unit_amount { unit: PIECE amount: 1 } }
      }
    }""")
RAW_APPLE_RECIPE = _parse_stored_recipe(r"""
    id: "raw-apple"
    recipe {
      title: "Raw apple"
      serves { min: 1 max: 1 }
      ingredients {
        ingredient_id: "/ingredient/apple"
        amount { unit_amount { unit: PIECE amount: 1 } }
      }
    }""")
SINGLE_EGG_PRODUCT = _parse_product(r"""
    merchant_product_id: 12345
    contents {
      ingredient_id: "/ingredient/egg"
      amount { unit: PIECE amount_exact: 1 }
    }""")
SINGLE_APPLE_PRODUCT = _parse_product(r"""
    merchant_product_id: 12346
    contents {
      ingredient_id: "/ingredient/apple"
      amount { unit: PIECE amount_exact: 1 }
    }""")
HUNDRED_GRAM_GENERIC_APPLE_PRODUCT = _parse_product(r"""
    merchant_product_id: 12347
    contents {
      ingredient_id: "/ingredient/apple/generic"
      amount { unit: GRAM amount_exact: 100 }
    }""")


def _plan(recipes: List[inventory_pb2.StoredRecipe],
          plan_request,
          ingredients,
          *,
          stock: Optional[List[inventory_pb2.StockItem]] = None,
          products: Optional[List[inventory_pb2.Product]] = None,
          diet_mappings: Optional[diet_mapping_pb2.DietMappings] = None,
          backend=None,
          force_same_quantum_for_connected_components: bool = None):
    return wrapper_lib.run_plan(
        stock_lib.StockDatabase(stock or []),
        [recipes_lib.RecipeWrapper(recipe) for recipe in recipes],
        plan_request,
        inventory_pb2.History(),
        (diet_mappings or diet_mapping_pb2.DietMappings()),
        inventory_pb2.Substitutions(),
        products=(products or []),
        convs=inventory_pb2.Conversions(),
        ingredient_database=ingredient_types.IngredientDatabase(
            ingredient_types=ingredients,
            ingredient_names=[],
            ingredient_matter_rules=[]),
        force_same_quantum_for_connected_components=(
            force_same_quantum_for_connected_components),
        backend=backend)


# TODO(agentydragon): make the tmpdir selection nicer
_SCOPED_MINIZINC_PLANNER_PLAN = functools.partial(
    minizinc_planner_lib.plan, tmp_dir=os.environ.get("TEST_TMPDIR"))

BACKEND_CC_ORTOOLS_NONFORCED = {
    'testcase_name': 'cc_ortools_nonforced',
    'backend': cc_ortools_backend_lib.run_plan_cc,
    'force_same_quantum_for_connected_components': False
}
BACKEND_CC_ORTOOLS_FORCED = {
    'testcase_name': 'cc_ortools_forced',
    'backend': cc_ortools_backend_lib.run_plan_cc,
    'force_same_quantum_for_connected_components': True
}
# TODO: z3 temporarily disabled 2022-04-21
# BACKEND_PYTHON_Z3_NONFORCED = {
#     'testcase_name': 'python_z3_nonforced',
#     'backend': planner_lib.plan,
#     'force_same_quantum_for_connected_components': False
# }
# BACKEND_PYTHON_Z3_FORCED = {
#     'testcase_name': 'python_z3_forced',
#     'backend': planner_lib.plan,
#     'force_same_quantum_for_connected_components': True
# }
BACKEND_PYTHON_MINIZINC_NONFORCED = {
    'testcase_name': 'python_minizinc_nonforced',
    'backend': _SCOPED_MINIZINC_PLANNER_PLAN,
    'force_same_quantum_for_connected_components': False
}
BACKEND_PYTHON_MINIZINC_FORCED = {
    'testcase_name': 'python_minizinc_forced',
    'backend': _SCOPED_MINIZINC_PLANNER_PLAN,
    'force_same_quantum_for_connected_components': True
}
BACKENDS = (
    BACKEND_CC_ORTOOLS_NONFORCED,
    BACKEND_CC_ORTOOLS_FORCED,
    # BACKEND_PYTHON_Z3_NONFORCED, BACKEND_PYTHON_Z3_FORCED,
    BACKEND_PYTHON_MINIZINC_NONFORCED,
    BACKEND_PYTHON_MINIZINC_FORCED)


class NoIngredientHierarchyTest(parameterized.TestCase):
    """Tests that do not need ingredient hierarchy and should work on all backends
    with any configuration."""

    @parameterized.named_parameters(*BACKENDS)
    @freezegun.freeze_time("2020-01-01")
    def test_cook_single_ingredient_without_temptation(
            self, backend, force_same_quantum_for_connected_components):
        """Tests cooking 2 raw eggs for 2 people."""
        plan_request = _parse_plan_request(r"""
            meal_request {
              portions: 2
              date { year: 2020 month: 10 day: 20 }
            }""")

        plan_proto = _plan(recipes=[RAW_EGG_RECIPE],
                           plan_request=plan_request,
                           ingredients=[EGG_INGREDIENT],
                           stock=[TWO_EGGS_STOCK],
                           backend=backend,
                           force_same_quantum_for_connected_components=(
                               force_same_quantum_for_connected_components))

        hamcrest.assert_that(
            plan_proto,
            proto_matcher.equals_proto("""
          planned_recipe {
            recipe_id: "raw-egg"
            portions: 2
            commitment {
              ingredient_id: "/ingredient/egg"
              amount { unit_amount { unit: PIECE amount: 2.0 } }
            }
            date { year: 2020 month: 10 day: 20 }
          }
          total_ingredient_use {
            ingredient_id: "/ingredient/egg"
            amount { unit_amount { unit: PIECE amount: 2 } }
          }"""))

    @parameterized.named_parameters(*BACKENDS)
    @freezegun.freeze_time("2020-01-01")
    def test_cook_single_ingredient_with_temptation(
            self, backend, force_same_quantum_for_connected_components):
        """Tests cooking 2 raw eggs for 2 people, but with option to buy more."""
        plan_request = _parse_plan_request(r"""
            meal_request {
              portions: 2
              date { year: 2020 month: 10 day: 20 }
            }""")

        plan_proto = _plan(recipes=[RAW_EGG_RECIPE],
                           plan_request=plan_request,
                           ingredients=[EGG_INGREDIENT],
                           stock=[TWO_EGGS_STOCK],
                           products=[SINGLE_EGG_PRODUCT],
                           backend=backend,
                           force_same_quantum_for_connected_components=(
                               force_same_quantum_for_connected_components))

        hamcrest.assert_that(
            plan_proto,
            proto_matcher.equals_proto("""
            planned_recipe {
              recipe_id: "raw-egg"
              portions: 2
              commitment {
                ingredient_id: "/ingredient/egg"
                amount { unit_amount { unit: PIECE amount: 2.0 } }
              }
              date { year: 2020 month: 10 day: 20 }
            }
            total_ingredient_use {
              ingredient_id: "/ingredient/egg"
              amount { unit_amount { unit: PIECE amount: 2.0 } }
            }"""))

    @parameterized.named_parameters(*BACKENDS)
    @freezegun.freeze_time("2020-01-01")
    def test_buy_and_cook(self, backend,
                          force_same_quantum_for_connected_components):
        """Tests buying and cooking 15 raw egg for 15 people."""
        plan_request = _parse_plan_request(r"""
            meal_request {
              portions: 15
              date { year: 2020 month: 10 day: 20 }
            }""")

        plan_proto = _plan(
            recipes=[RAW_EGG_RECIPE],
            plan_request=plan_request,
            ingredients=[EGG_INGREDIENT],
            stock=[],  # no eggs in stock
            products=[SINGLE_EGG_PRODUCT],
            backend=backend,
            force_same_quantum_for_connected_components=(
                force_same_quantum_for_connected_components))

        hamcrest.assert_that(
            plan_proto,
            proto_matcher.equals_proto("""
            planned_recipe {
              recipe_id: "raw-egg"
              portions: 15
              commitment {
                ingredient_id: "/ingredient/egg"
                amount { unit_amount { unit: PIECE amount: 15.0 } }
              }
              date { year: 2020 month: 10 day: 20 }
            }
            purchase { merchant_product_id: 12345 amount: 15 }
            total_ingredient_use {
              ingredient_id: "/ingredient/egg"
              amount { unit_amount { unit: PIECE amount: 15.0 } }
            }
            purchase_delivery_date { year: 2020 month: 10 day: 20 }"""))

    @parameterized.named_parameters(*BACKENDS)
    @freezegun.freeze_time("2020-01-01")
    def test_obtain_externally_and_cook(
            self, backend, force_same_quantum_for_connected_components):
        """Tests obtaining 15 raw eggs for 15 people externally."""
        plan_request = _parse_plan_request(r"""
            meal_request {
              portions: 15
              date { year: 2020 month: 10 day: 20 }
            }
            max_added_ingredients: 1
            """)

        plan_proto = _plan(
            recipes=[RAW_EGG_RECIPE],
            plan_request=plan_request,
            ingredients=[EGG_INGREDIENT],
            stock=[],  # no eggs in stock
            products=[],
            backend=backend,
            force_same_quantum_for_connected_components=(
                force_same_quantum_for_connected_components))

        hamcrest.assert_that(
            plan_proto,
            proto_matcher.equals_proto("""
            planned_recipe {
              recipe_id: "raw-egg"
              portions: 15
              commitment {
                ingredient_id: "/ingredient/egg"
                amount { unit_amount { unit: PIECE amount: 15 } }
              }
              date { year: 2020 month: 10 day: 20 }
            }
            obtain_externally {
              ingredient_id: "/ingredient/egg"
              amount { unit: PIECE amount: 15.0 }
            }
            total_ingredient_use {
              ingredient_id: "/ingredient/egg"
              amount { unit_amount { unit: PIECE amount: 15.0 } }
            }"""))

    @parameterized.named_parameters(*BACKENDS)
    @freezegun.freeze_time("2020-01-01")
    def test_cook_single_ingredient_with_salt(
            self, backend, force_same_quantum_for_connected_components):
        """Tests cooking 2 raw eggs for 2 people, with salt."""
        plan_request = _parse_plan_request(r"""
            meal_request {
              portions: 2
              date { year: 2020 month: 10 day: 20 }
            }""")

        plan_proto = _plan(recipes=[RAW_EGG_WITH_SALT_RECIPE],
                           plan_request=plan_request,
                           ingredients=[EGG_INGREDIENT, SALT_INGREDIENT],
                           stock=[TWO_EGGS_STOCK, SALT_STOCK],
                           products=[],
                           backend=backend,
                           force_same_quantum_for_connected_components=(
                               force_same_quantum_for_connected_components))

        hamcrest.assert_that(
            plan_proto,
            proto_matcher.equals_proto("""
            planned_recipe {
              recipe_id: "raw-egg-with-salt"
              portions: 2
              commitment {
                ingredient_id: "/ingredient/egg"
                amount { unit_amount { unit: PIECE amount: 2.0 } }
              }
              commitment {
                ingredient_id: "/ingredient/salt"
                amount { to_taste {} }
              }
              date { year: 2020 month: 10 day: 20 }
            }
            total_ingredient_use {
              ingredient_id: "/ingredient/egg"
              amount { unit_amount { unit: PIECE amount: 2.0 } }
            }"""))

    @parameterized.named_parameters(
        BACKEND_CC_ORTOOLS_NONFORCED,
        BACKEND_CC_ORTOOLS_FORCED,
        # BACKEND_PYTHON_Z3_FORCED,
        # TODO(agentydragon): for some reason z3_unforced does not work
        BACKEND_PYTHON_MINIZINC_NONFORCED,
        BACKEND_PYTHON_MINIZINC_FORCED)
    @freezegun.freeze_time("2020-01-01")
    def test_max_zero_of_type(self, backend,
                              force_same_quantum_for_connected_components):
        """Put 2 eggs into stock and ask to cook for 2 people.
        We will prefer buying 2 apples to cooking the 2 eggs, because we asked
        not to cook eggs.
        """
        # TODO(agentydragon): for some reason this test does not seem to work for
        # just 1 person. why?
        plan_request = _parse_plan_request(r"""
            meal_request {
              portions: 2
              date { year: 2020 month: 10 day: 20 }
            }
            max_recipe_type { name_part: "egg" max_amount: 0 }
            """)

        plan_proto = _plan(recipes=[RAW_EGG_RECIPE, RAW_APPLE_RECIPE],
                           plan_request=plan_request,
                           ingredients=[EGG_INGREDIENT, APPLE_INGREDIENT],
                           stock=[TWO_EGGS_STOCK],
                           products=[SINGLE_APPLE_PRODUCT],
                           backend=backend,
                           force_same_quantum_for_connected_components=(
                               force_same_quantum_for_connected_components))

        hamcrest.assert_that(
            plan_proto,
            proto_matcher.equals_proto("""
            planned_recipe {
              recipe_id: "raw-apple"
              portions: 2
              commitment {
                ingredient_id: "/ingredient/apple"
                amount { unit_amount { unit: PIECE amount: 2.0 } }
              }
              date { year: 2020 month: 10 day: 20 }
            }
            purchase { merchant_product_id: 12346 amount: 2 }
            total_ingredient_use {
              ingredient_id: "/ingredient/apple"
              amount { unit_amount { unit: PIECE amount: 2.0 } }
            }
            purchase_delivery_date { year: 2020 month: 10 day: 20 }"""))

    @parameterized.named_parameters(*BACKENDS)
    @freezegun.freeze_time("2020-01-01")
    def test_max_one_of_type(self, backend,
                             force_same_quantum_for_connected_components):
        """Put 2 eggs into stock and ask to cook 2 recipes for 1 person.
        We should not cook 2 egg recipes.
        """
        plan_request = _parse_plan_request(r"""
            meal_request {
              portions: 1
              date { year: 2020 month: 10 day: 20 }
            }
            meal_request {
              portions: 1
              date { year: 2020 month: 10 day: 21 }
            }
            max_recipe_type { name_part: "egg" max_amount: 1 }
            """)

        plan_proto = _plan(
            recipes=[
                RAW_EGG_RECIPE, RAW_EGG_WITH_SALT_RECIPE, RAW_APPLE_RECIPE
            ],
            plan_request=plan_request,
            ingredients=[EGG_INGREDIENT, SALT_INGREDIENT, APPLE_INGREDIENT],
            stock=[TWO_EGGS_STOCK, TWO_APPLES_STOCK, SALT_STOCK],
            backend=backend,
            force_same_quantum_for_connected_components=(
                force_same_quantum_for_connected_components))

        recipe_ids = {r.recipe_id for r in plan_proto.planned_recipe}
        egg_recipes = {'raw-egg', 'raw-egg-with-salt'}
        self.assertEqual(len(recipe_ids & egg_recipes), 1)
        self.assertIn('raw-apple', recipe_ids)

    # TODO(agentydragon): for some reason this test does not seem to work for
    # just 1 person. why?

    # TODO(agentydragon): for some reason this does not work with Z3 backend
    @parameterized.named_parameters(BACKEND_CC_ORTOOLS_NONFORCED,
                                    BACKEND_CC_ORTOOLS_FORCED,
                                    BACKEND_PYTHON_MINIZINC_NONFORCED,
                                    BACKEND_PYTHON_MINIZINC_FORCED)
    @freezegun.freeze_time("2020-01-01")
    def test_prefer_buying_over_obtaining_externally(
            self, backend, force_same_quantum_for_connected_components):
        """Tests cooking 2 raw eggs for 2 people. They can be either bought,
        or obtained externally. Since they can be bought, they should be bought
        rather than obtained externally."""
        plan_request = _parse_plan_request(r"""
            meal_request {
              portions: 2
              date { year: 2020 month: 10 day: 20 }
            }
            max_added_ingredients: 100
            """)

        plan_proto = _plan(recipes=[RAW_EGG_RECIPE],
                           plan_request=plan_request,
                           ingredients=[EGG_INGREDIENT],
                           products=[SINGLE_EGG_PRODUCT],
                           backend=backend,
                           force_same_quantum_for_connected_components=(
                               force_same_quantum_for_connected_components))

        hamcrest.assert_that(
            plan_proto,
            proto_matcher.equals_proto("""
             planned_recipe {
               recipe_id: "raw-egg"
               portions: 2
               commitment {
                 ingredient_id: "/ingredient/egg"
                 amount { unit_amount { unit: PIECE amount: 2.0 } }
               }
               date { year: 2020 month: 10 day: 20 }
             }
             purchase { merchant_product_id: 12345 amount: 2 }
             total_ingredient_use {
               ingredient_id: "/ingredient/egg"
               amount { unit_amount { unit: PIECE amount: 2.0 } }
             }
             purchase_delivery_date { year: 2020 month: 10 day: 20 }"""))


class ValidationTest(absltest.TestCase):

    @freezegun.freeze_time("2020-01-01")
    def test_validate_plan_request_unsorted_dates(self):
        with self.assertRaisesRegex(Exception, "Dates are not sorted"):
            wrapper_lib.validate_plan_request(
                _parse_plan_request(r"""
              meal_request {
                portions: 2
                date: { year: 2030 month: 1 day: 1 }
              }
              meal_request {
                portions: 2
                date: { year: 2025 month: 1 day: 1 }
              }"""))

    @freezegun.freeze_time("2020-01-01")
    def test_validate_plan_request_dates_in_past(self):
        with self.assertRaisesRegex(Exception, "dates are in the past"):
            wrapper_lib.validate_plan_request(
                _parse_plan_request(r"""
              meal_request {
                portions: 2
                date: { year: 2000 month: 1 day: 1 }
              }"""))

    @freezegun.freeze_time("2020-01-01")
    def test_validate_plan_request_ok(self):
        # Should raise no exception.
        wrapper_lib.validate_plan_request(
            _parse_plan_request(r"""
          meal_request {
            portions: 2
            date: { year: 2030 month: 1 day: 1 }
          }
          meal_request {
            portions: 2
            date: { year: 2030 month: 1 day: 2 }
          }"""))


BACKENDS_WITH_GENERIC_INGREDIENTS = ({
    'testcase_name': 'python_minizinc_same_quantum',
    'backend': _SCOPED_MINIZINC_PLANNER_PLAN,
    'force_same_quantum_for_connected_components': True
},)


class GenericIngredientPlanningTest(parameterized.TestCase):

    @parameterized.named_parameters(*BACKENDS_WITH_GENERIC_INGREDIENTS)
    @freezegun.freeze_time("2020-01-01")
    def test_cook_specific_in_place_of_generic(
            self, backend, force_same_quantum_for_connected_components):
        """Use a red apple as an instance of generic apple."""
        plan_request = _parse_plan_request(r"""
            meal_request {
              portions: 1
              date: { year: 2020 month: 1 day: 1 }
            }""")

        plan_proto = _plan(
            recipes=[HUNDRED_GRAM_RAW_APPLE_RECIPE],
            plan_request=plan_request,
            ingredients=[GENERIC_APPLE_INGREDIENT, RED_APPLE_INGREDIENT],
            stock=[ONE_KILO_RED_APPLES_STOCK],
            backend=backend,
            force_same_quantum_for_connected_components=(
                force_same_quantum_for_connected_components))

        hamcrest.assert_that(
            plan_proto,
            proto_matcher.equals_proto("""
            planned_recipe {
              recipe_id: "raw-apple-100g"
              portions: 1
              commitment {
                ingredient_id: "/ingredient/apple/red"
                amount { unit_amount { unit: GRAM amount: 100.0 } }
              }
              date { year: 2020 month: 1 day: 1 }
            }
            total_ingredient_use {
              ingredient_id: "/ingredient/apple/red"
              amount { unit_amount { unit: GRAM amount: 100.0 } }
            }
            """))

    @parameterized.named_parameters(*BACKENDS_WITH_GENERIC_INGREDIENTS)
    @freezegun.freeze_time("2020-01-01")
    def test_cook_specific_mixed_with_generic(
            self, backend, force_same_quantum_for_connected_components):
        """Cook 50 g generic apple + 50 g red apple as 100 g of generic apple."""
        plan_request = _parse_plan_request(r"""
            meal_request {
              portions: 1
              date: { year: 2020 month: 1 day: 1 }
            }""")

        plan_proto = _plan(
            recipes=[HUNDRED_GRAM_RAW_APPLE_RECIPE],
            plan_request=plan_request,
            ingredients=[GENERIC_APPLE_INGREDIENT, RED_APPLE_INGREDIENT],
            stock=[
                FIFTY_GRAM_RED_APPLES_STOCK, FIFTY_GRAM_GENERIC_APPLES_STOCK
            ],
            backend=backend,
            force_same_quantum_for_connected_components=(
                force_same_quantum_for_connected_components))

        hamcrest.assert_that(
            plan_proto,
            proto_matcher.equals_proto("""
            planned_recipe {
              recipe_id: "raw-apple-100g"
              portions: 1
              commitment {
                ingredient_id: "/ingredient/apple/generic"
                amount { unit_amount { unit: GRAM amount: 50.0 } }
              }
              commitment {
                ingredient_id: "/ingredient/apple/red"
                amount { unit_amount { unit: GRAM amount: 50.0 } }
              }
              date { year: 2020 month: 1 day: 1 }
            }
            total_ingredient_use {
              ingredient_id: "/ingredient/apple/generic"
              amount { unit_amount { unit: GRAM amount: 50.0 } }
            }
            total_ingredient_use {
              ingredient_id: "/ingredient/apple/red"
              amount { unit_amount { unit: GRAM amount: 50.0 } }
            }"""))

    # TODO(agentydragon): test: 1 pieces of red apples @ 50 g, 2 pieces of green
    # apples @ 25 g --> can be used together as 100 g of any apples


BACKENDS_WITH_PER_MEAL_DIETARY_REQUIREMENTS = ({
    'testcase_name': 'python_minizinc_same_quantum',
    'backend': _SCOPED_MINIZINC_PLANNER_PLAN,
    'force_same_quantum_for_connected_components': True
},)


class PerMealDietaryRequirementsTest(parameterized.TestCase):

    @parameterized.named_parameters(*BACKENDS_WITH_PER_MEAL_DIETARY_REQUIREMENTS
                                   )
    @freezegun.freeze_time("2020-01-01")
    def test_buy_to_avoid_banned_diet_bit(
            self, backend, force_same_quantum_for_connected_components):
        """Recipes are: steak, apple. At home, we have a steak. But we ban
        cooking the steak by the diet bit."""
        plan_request = _parse_plan_request(r"""
            meal_request {
              portions: 1
              date: { year: 2020 month: 1 day: 1 }
              disallowed_diet_bits: MEAT
            }""")

        plan_proto = _plan(recipes=[RAW_APPLE_RECIPE, ONE_STEAK_RECIPE],
                           plan_request=plan_request,
                           ingredients=[APPLE_INGREDIENT, STEAK_INGREDIENT],
                           products=[SINGLE_APPLE_PRODUCT],
                           stock=[TEN_STEAKS_STOCK],
                           backend=backend,
                           diet_mappings=_parse_diet_mappings(r"""
                diet_mapping {
                  ingredient_id_regex: "/ingredient/meat/.*"
                  diet_bit: MEAT
                }"""),
                           force_same_quantum_for_connected_components=(
                               force_same_quantum_for_connected_components))

        hamcrest.assert_that(
            plan_proto,
            proto_matcher.equals_proto("""
            planned_recipe {
              recipe_id: "raw-apple"
              portions: 1
              date { year: 2020 month: 1 day: 1 }
              commitment {
                ingredient_id: "/ingredient/apple"
                amount { unit_amount { unit: PIECE amount: 1.0 } }
              }
            }
            purchase { merchant_product_id: 12346 amount: 1 }
            purchase_delivery_date { year: 2020 month: 1 day: 1 }
            total_ingredient_use {
              ingredient_id: "/ingredient/apple"
              amount { unit_amount { unit: PIECE amount: 1.0 } }
            }"""))

    @parameterized.named_parameters(*BACKENDS_WITH_PER_MEAL_DIETARY_REQUIREMENTS
                                   )
    @freezegun.freeze_time("2020-01-01")
    def test_buy_to_avoid_banned_diet_bit_then_cook(
            self, backend, force_same_quantum_for_connected_components):
        """Recipes are: steak, apple. At home, we have a steak. But we ban
        cooking the steak by the diet bit. Then the second day we allow cooking
        the steak."""
        plan_request = _parse_plan_request(r"""
            meal_request {
              portions: 1
              date: { year: 2020 month: 1 day: 1 }
              disallowed_diet_bits: MEAT
            }
            meal_request {
              portions: 1
              date: { year: 2020 month: 1 day: 2 }
            }""")

        plan_proto = _plan(recipes=[RAW_APPLE_RECIPE, ONE_STEAK_RECIPE],
                           plan_request=plan_request,
                           ingredients=[APPLE_INGREDIENT, STEAK_INGREDIENT],
                           products=[SINGLE_APPLE_PRODUCT],
                           stock=[TEN_STEAKS_STOCK],
                           backend=backend,
                           diet_mappings=_parse_diet_mappings(r"""
                             diet_mapping {
                               ingredient_id_regex: "/ingredient/meat/.*"
                               diet_bit: MEAT
                             }"""),
                           force_same_quantum_for_connected_components=(
                               force_same_quantum_for_connected_components))

        hamcrest.assert_that(
            plan_proto,
            proto_matcher.equals_proto("""
                planned_recipe {
                  recipe_id: "raw-apple"
                  portions: 1
                  date { year: 2020 month: 1 day: 1 }
                  commitment {
                    ingredient_id: "/ingredient/apple"
                    amount { unit_amount { unit: PIECE amount: 1.0 } }
                  }
                }
                planned_recipe {
                  recipe_id: "steak-1piece"
                  portions: 1
                  date { year: 2020 month: 1 day: 2 }
                  commitment {
                    ingredient_id: "/ingredient/meat/steak"
                    amount { unit_amount { unit: PIECE amount: 1.0 } }
                  }
                }
                purchase { merchant_product_id: 12346 amount: 1 }
                purchase_delivery_date { year: 2020 month: 1 day: 1 }
                total_ingredient_use {
                  ingredient_id: "/ingredient/apple"
                  amount { unit_amount { unit: PIECE amount: 1.0 } }
                }
                total_ingredient_use {
                  ingredient_id: "/ingredient/meat/steak"
                  amount { unit_amount { unit: PIECE amount: 1.0 } }
                }
                """))

    # TODO(agentydragon): more tests for disallowed diet bits? ideas?


if __name__ == '__main__':
    absltest.main()
