from alembic import op
from sqlalchemy import Column, Integer, LargeBinary, String, UniqueConstraint

revision = 'fcb44e57bd0d'
down_revision = None
branch_labels = None
depends_on = None


def upgrade():
    op.create_table('Stock', Column('ingredient_id', String, nullable=False),
                    Column('timestamp', Integer),
                    Column('stock_amount', LargeBinary(), nullable=False),
                    Column('comment', String))

    op.create_table('Recipe',
                    Column('id', String, nullable=False, primary_key=True),
                    Column('importer', String), Column('importer_id', String),
                    Column('recipe', LargeBinary(), nullable=False),
                    UniqueConstraint('importer', 'importer_id'))

    op.create_table('Plan',
                    Column('plan_id', String, nullable=False, primary_key=True),
                    Column('created_at', Integer, nullable=False),
                    Column('plan', LargeBinary(), nullable=False),
                    Column('plan_request', LargeBinary(), nullable=False),
                    Column('comment', String))

    op.create_table('PastCookedMeal', Column('recipe_id',
                                             String,
                                             nullable=False),
                    Column('date', String, nullable=False))


def downgrade():
    pass
