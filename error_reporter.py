from absl import logging


class ErrorReporter:

    def __init__(self):
        self.errors = []

    def report(self, s):
        logging.error("%s", s)
        self.errors.append(s)

    def log_errors(self):
        if self.errors:
            logging.error(f"{len(self.errors)} errors found:")
            for error in self.errors:
                logging.error("%s", error)

    def ok(self) -> bool:
        return not self.errors
