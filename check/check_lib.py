import collections
import re
from typing import List

import z3
from absl import logging
from meal_scheduler.data import products as products_lib
from meal_scheduler.data import recipes
from meal_scheduler.data import stock as stock_lib
from meal_scheduler.inventory_py_proto_pb import inventory_pb2
from meal_scheduler.plan import translator as translator_lib
from meal_scheduler.unit_py_proto_pb import unit_pb2


def find_ingredients_to_map(all_recipes, products, s: stock_lib.StockDatabase):
    ingredients_by_recipe = {}

    for recipe in all_recipes:
        ingredients = []
        for line in recipe.ingredients:
            ingredients.append(line.ingredient_id)
        ingredients_by_recipe[recipe.title] = set(ingredients)

    all_ingredients = set()
    for ingredients in ingredients_by_recipe.values():
        all_ingredients = all_ingredients.union(set(ingredients))
    all_ingredients = list(sorted(all_ingredients))

    ingredients_with_products = products_lib.get_ingredient_ids_with_products(
        products)

    logging.info("recipes: %d", len(all_recipes))

    solver = z3.Optimize()

    # recipe_available[i] = whether we have everything for that recipe
    recipe_available = z3.Array('recipe_available', z3.IntSort(), z3.BoolSort())
    # ingredient_available[i] = whether the ingredient is in LeShop
    ingredient_available = z3.Array('ingredient_available', z3.IntSort(),
                                    z3.BoolSort())
    for recipe, rvar in zip(all_recipes, recipe_available):
        ivars = []
        for line in recipe.ingredients:
            ivars.append(ingredient_available[all_ingredients.index(
                line.ingredient_id)])
        constraint = (rvar == z3.And(ivars))
        solver.add(constraint)

    # TODO(agentydragon): try to find a vegan replacement
    for i, (ingredient,
            ivar) in enumerate(zip(all_ingredients, ingredient_available)):
        if (  # ingredient.startswith('/ingredient/dairy/') or
                ingredient.startswith('/ingredient/meat/')):
            solver.add(ivar == False)

    # count recipes that can be cooked
    recipe_count = z3.Sum([
        z3.IntSort().cast(recipe_available[i]) for i in range(len(all_recipes))
    ])

    got_enough = set()
    for item in s.stock:
        if item.amount.HasField('enough'):
            # Stock has enough, no need to keep details.
            got_enough.add(item.ingredient_id)

    ingredients_added_count = z3.Sum([
        z3.IntSort().cast(ingredient_available[i])
        for i, ingredient in enumerate(all_ingredients)
        if ((ingredient not in ingredients_with_products) and
            (ingredient not in got_enough))
    ])

    solver.add(ingredients_added_count <= 20)
    solver.maximize(recipe_count)
    logging.info("%s", solver.check())

    model = solver.model()

    logging.info("recipes we can do: %s",
                 model.eval(recipe_count, model_completion=True))
    for i, (ingredient,
            ivar) in enumerate(zip(all_ingredients, ingredient_available)):
        included = model.eval(ivar, model_completion=True)
        if (included and (ingredient not in ingredients_with_products) and
            (ingredient not in got_enough)):
            logging.info("%s: %s", ingredient, included)


def check_ingredients(ingredient_database, error_reporter):
    ids = [ingredient.id for ingredient in ingredient_database.ingredient]
    for ingredient_id in ids:
        check_ingredient_id_format(ingredient_id, error_reporter)
    counter = collections.Counter(ids)
    duplicated = [key for key, value in counter.items() if value > 1]
    if duplicated:
        error_reporter.report(f"Duplicated ingredients spec: {duplicated}")

    for ingredient in ingredient_database.ingredient:
        for subtype in ingredient.subtypes:
            if subtype not in ingredient_database.ingredient_ids:
                error_reporter.report(
                    f"{ingredient} has subtype {subtype} which does not exist")
    # TODO(agentydragon): check acyclic subtypes

    for name in ingredient_database._ingredient_names:
        if name.ingredient_id not in ingredient_database.ingredient_ids:
            error_reporter.report(
                f"name declared for unknown ingredient {name.ingredient_id}")

    counter = collections.Counter(
        name for name_proto in ingredient_database._ingredient_names
        for name in name_proto.name)
    duplicated = [key for key, count in counter.items() if count > 1]
    if duplicated:
        error_reporter.report(f"Duplicated ingredient names: {duplicated}")

    # TODO(agentydragon): check that if an ingredient has a set universal unit,
    # it's also the same for all its supertypes, and all its subtypes have
    # conversions to it.


def validate_ingredient_id_unit(ingredient_id, unit, ingredient_database,
                                error_reporter):
    matter_type = ingredient_database.get_matter_type(ingredient_id)

    if matter_type == 1:  # liquid
        if unit == unit_pb2.PIECE:
            error_reporter.report(
                f"{ingredient_id} is a LIQUID, can't have a PIECE of it")

    if matter_type == 2:  # powder
        if unit == unit_pb2.PIECE:
            error_reporter.report(
                f"{ingredient_id} is a POWDER, can't have a PIECE of it")


def _validate_recipe_line(recipe_line, ingredient_database, error_reporter):
    if recipe_line.amount.HasField('unit_amount'):
        validate_ingredient_id_unit(recipe_line.ingredient_id,
                                    recipe_line.amount.unit_amount.unit,
                                    ingredient_database, error_reporter)


def check_recipe(ingredient_database, recipe: inventory_pb2.Recipe,
                 error_reporter):
    recipe_ingredients = {line.ingredient_id for line in recipe.ingredients}
    missing = recipe_ingredients - ingredient_database.ingredient_ids
    if missing:
        error_reporter.report(
            f"Recipe {recipe.title} uses undeclared ingredients {missing}")
    for line in recipe.ingredients:
        _validate_recipe_line(line, ingredient_database, error_reporter)


def check_recipe_ingredients(ingredient_database,
                             all_recipes: List[recipes.RecipeWrapper],
                             error_reporter):
    for recipe in all_recipes:
        check_recipe(ingredient_database, recipe.recipe_proto, error_reporter)


def check_stock(ingredient_database, s: stock_lib.StockDatabase,
                error_reporter):
    for item in s.stock:
        if item.ingredient_id not in ingredient_database.ingredient_ids:
            error_reporter.report(
                f"Stock contains undeclared ingredient {item.ingredient_id}")


def check_conversions(conversions, error_reporter):
    # TODO(agentydragon): currently just checks that they're a valid proto
    pass


def check_products(ingredient_database, products: List[inventory_pb2.Product],
                   error_reporter):
    assert isinstance(products, list)
    # TODO(agentydragon): should only work across merchants
    merchant_product_ids = set()
    for product in products:
        if (product.contents.ingredient_id
                not in ingredient_database.ingredient_ids):
            error_reporter.report(
                f"Product {product.merchant_product_id} has unknown ingredient {product.contents.ingredient_id}"
            )

        if product.merchant_product_id in merchant_product_ids:
            error_reporter.report(
                f"LeShop id {product.merchant_product_id} is in multiple products!"
            )
        merchant_product_ids.add(product.merchant_product_id)
    logging.info("%s", merchant_product_ids)


def check_common_units(ingredient_database, recipes,
                       products: List[inventory_pb2.Product], stock, convs,
                       error_reporter):
    ingredient_ids = translator_lib._get_ingredient_ids_that_need_managing(
        recipes, products, stock)
    # TODO(agentydragon): the check that each cluster defines a single unit
    # should be enforced even on ingredients that have no uses right now.
    ingredient_unit_amounts = translator_lib.collect_unit_amounts(
        ingredient_ids, recipes, products, stock)
    translator_lib.find_common_units(
        ingredient_unit_amounts,
        convs,
        ingredient_database,
        error_reporter,
        force_same_quantum_for_connected_components=True)


def find_ingredients_with_no_recipes(s: stock_lib.StockDatabase, all_recipes,
                                     error_reporter):
    used_ingredients = set.union(
        *(recipe.ingredient_ids for recipe in all_recipes))

    ingredients_in_stock = set()
    for item in s.stock:
        if item.amount.HasField('exact') and item.amount.exact.amount == 0:
            continue
        # TODO(agentydragon): remove lower_bound
        if item.amount.HasField(
                'lower_bound') and item.amount.lower_bound.amount == 0:
            continue
        ingredients_in_stock.add(item.ingredient_id)

    # TODO(agentydragon): Once we support planning with generic ingredients,
    # account for it properly here.

    no_recipe = ingredients_in_stock - used_ingredients
    for ingredient_id in sorted(no_recipe):
        error_reporter.report(
            f"Ingredient {ingredient_id} is in stock, but no recipes use it.")


def check_ingredient_id_format(ingredient_id, error_reporter):
    if not re.fullmatch('[a-z_/-]+', ingredient_id):
        error_reporter.report(f"Invalid ingredient ID format: {ingredient_id}")


def check_all(ingredient_database, stock, all_recipes,
              products: List[inventory_pb2.Product], convs, error_reporter):
    assert isinstance(products, list)
    check_stock(ingredient_database, stock, error_reporter)
    check_ingredients(ingredient_database, error_reporter)
    check_recipe_ingredients(ingredient_database, all_recipes, error_reporter)
    check_products(ingredient_database, products, error_reporter)
    # TODO(agentydragon): check_lib.find_ingredients_to_map(all_recipes, products, s)
    # TODO(agentydragon): only prohibit meals from last $INTERVAL, not from all of history
    # TODO(agentydragon): recipes should have unique titles, URLs
    # TODO(agentydragon): check that conversions refer to existing ingredients
    # TODO(agentydragon): check LeShop
    find_ingredients_with_no_recipes(stock, all_recipes, error_reporter)
    check_common_units(ingredient_database, all_recipes, products, stock, convs,
                       error_reporter)
    check_conversions(convs, error_reporter)
