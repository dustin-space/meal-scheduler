import sqlalchemy
from absl import logging
from meal_scheduler.db import db as db_lib
from meal_scheduler.db import schema
from meal_scheduler.inventory_py_proto_pb import inventory_pb2


# TODO(agentydragon): validate imported recipes
def import_recipes(db,
                   recipe_import: inventory_pb2.RecipeImport,
                   delete_missing=False,
                   do_insert=True,
                   do_update=True):
    stats = {}

    importer = recipe_import.importer

    imported_recipe_by_importer_id = {
        recipe.importer_id: recipe.recipe
        for recipe in recipe_import.imported_recipe
    }

    logging.info("Importing recipes.")
    importer_ids = set(imported_recipe_by_importer_id.keys())
    rows = db.execute(
        sqlalchemy.select([
            schema.RECIPE_TABLE
        ]).where(schema.RECIPE_TABLE.c.importer == importer)).fetchall()
    # TODO(agentydragon): check unique imported recipe IDs

    existing_importer_map = {row['importer_id']: row['id'] for row in rows}
    existing_recipes = {row['id']: row['recipe'] for row in rows}
    new_importer_ids = importer_ids - set(existing_importer_map.keys())
    updated_importer_ids = importer_ids & set(existing_importer_map.keys())
    deleted_importer_ids = set(existing_importer_map.keys()) - importer_ids

    new_recipe_ids = {
        importer_id: db_lib.new_uuid()
        for importer_id in new_importer_ids
    }
    values = [{
        'id': new_recipe_ids[imported_recipe.importer_id],
        'importer': importer,
        'importer_id': imported_recipe.importer_id,
        'recipe': imported_recipe.recipe.SerializeToString()
    } for imported_recipe in recipe_import.imported_recipe
              if imported_recipe.importer_id in new_importer_ids]
    logging.info("inserts: %s", values)
    if values and do_insert:
        result = db.execute(schema.RECIPE_TABLE.insert(), values)
        assert result.rowcount == len(new_importer_ids)
    stats['insert'] = len(new_importer_ids)
    logging.info("added %d new recipes", len(new_importer_ids))

    updates = []
    for importer_id, recipe_id in existing_importer_map.items():
        if importer_id in deleted_importer_ids:
            continue

        new_recipe = imported_recipe_by_importer_id[
            importer_id].SerializeToString()
        if new_recipe == existing_recipes[recipe_id]:
            logging.info("Recipe %s will not be updated, it's the same",
                         recipe_id)
            continue

        updates.append({
            # NOTE: the name 'importer_id' is reserved by SQLAlchemy
            'existing_importer_id': importer_id,
            'new_recipe': new_recipe
        })
    logging.info("updates: %s", updates)
    stats['update'] = len(updates)
    if updates and do_update:
        result = db.execute(
            schema.RECIPE_TABLE.update().where(
                sqlalchemy.and_(
                    schema.RECIPE_TABLE.c.importer == importer,
                    schema.RECIPE_TABLE.c.importer_id ==
                    sqlalchemy.bindparam('existing_importer_id'))).values(
                        recipe=sqlalchemy.bindparam('new_recipe')), updates)
        assert result.rowcount == len(updates)
        logging.info("updated %d existing recipes", len(existing_importer_map))

    stats['delete'] = len(deleted_importer_ids)
    if delete_missing:
        logging.info("deleted importer IDs: %s", deleted_importer_ids)
        result = db.execute(schema.RECIPE_TABLE.delete().where(
            sqlalchemy.and_(
                schema.RECIPE_TABLE.c.importer == importer,
                schema.RECIPE_TABLE.c.importer_id.in_(deleted_importer_ids))))
        assert result.rowcount == len(deleted_importer_ids)
        logging.info("deleted %d missing recipes", len(deleted_importer_ids))
    else:
        logging.info("not deleting %d missing recipes",
                     len(deleted_importer_ids))

    return stats
