from alembic import op
from sqlalchemy import Column, Integer, LargeBinary, String, UniqueConstraint

revision = '9bd60f0bb17d'
down_revision = 'fcb44e57bd0d'
branch_labels = None
depends_on = None


def upgrade():
    op.create_table(
        'MerchantProductStatus', Column('merchant', String, nullable=False),
        Column('merchant_product_id', String, nullable=False),
        Column('timestamp', Integer, nullable=False),
        Column('merchant_product_status', LargeBinary(), nullable=False))


def downgrade():
    op.drop_table('MerchantProductStatus')
