workspace(
    name = "meal_scheduler",
)

# These rules are built-into Bazel but we need to load them first to download more rules
load("@bazel_tools//tools/build_defs/repo:git.bzl", "git_repository", "new_git_repository")
load("@bazel_tools//tools/build_defs/repo:http.bzl", "http_archive")

git_repository(
    name = "rules_python",
    commit = "4c7e63fe6842343f3fb30cd744aaba54596f537d",
    remote = "https://github.com/bazelbuild/rules_python",
    shallow_since = "1650065839 -0700",
)

load("@rules_python//python:pip.bzl", "pip_install")

pip_install(
    name = "my_deps",
    requirements = "//:requirements.txt",
)

git_repository(
    name = "rules_proto_grpc",
    commit = "2e8be8e27cc82203794f14dbdcf37189b02ab722",
    remote = "https://github.com/rules-proto-grpc/rules_proto_grpc",
    shallow_since = "1641403416 +0000",
)

load("@rules_proto_grpc//:repositories.bzl", "rules_proto_grpc_repos", "rules_proto_grpc_toolchains")

rules_proto_grpc_toolchains()

rules_proto_grpc_repos()

load("@rules_proto//proto:repositories.bzl", "rules_proto_dependencies", "rules_proto_toolchains")

rules_proto_dependencies()

rules_proto_toolchains()

load("@rules_proto_grpc//python:repositories.bzl", rules_proto_grpc_python_repos = "python_repos")

rules_proto_grpc_python_repos()

git_repository(
    name = "ortools",
    commit = "86d4c543f717fa8716a9d66c25143660668bf825",
    remote = "https://github.com/google/or-tools",
    shallow_since = "1633058038 +0200",
)

git_repository(
    name = "com_google_absl",
    commit = "3dccef2a91243460364312d3e1100ff1d573fb1d",
    remote = "https://github.com/abseil/abseil-cpp",
    shallow_since = "1650406120 -0400",
)

git_repository(
    name = "com_github_gflags_gflags",
    commit = "986e8eed00ded8168ef4eaa6f925dc6be50b40fa",
    remote = "https://github.com/gflags/gflags",
    shallow_since = "1604052972 +0000",
)

git_repository(
    name = "com_github_glog_glog",
    commit = "b33e3bad4c46c8a6345525fd822af355e5ef9446",
    remote = "https://github.com/google/glog",
    shallow_since = "1601711556 +0200",
)

git_repository(
    name = "rules_foreign_cc",
    commit = "c57b55f2f6207c0c44c2b4a51dc235955d4f47b2",
    remote = "https://github.com/bazelbuild/rules_foreign_cc",
    shallow_since = "1650493831 +0000",
)

load("@rules_foreign_cc//foreign_cc:repositories.bzl", "rules_foreign_cc_dependencies")

rules_foreign_cc_dependencies()

new_git_repository(
    name = "libminizinc",
    build_file_content = """filegroup(
    name = "all",
    # Tests disabled due to issue with file with Unicode in its name.
    srcs = glob(["**"], exclude=["tests/**"]),
    visibility = ["//visibility:public"],
)
filegroup(
    name = "stdlib",
    srcs = glob(["share/minizinc/std/**"]),
    visibility = ["//visibility:public"],
)""",
    commit = "a56602765b4294b796c063664733b28f5a663af7",
    remote = "https://github.com/MiniZinc/libminizinc",
    shallow_since = "1647925129 +1100",
)

new_git_repository(
    name = "gecode",
    build_file_content = """filegroup(
    name = "all",
    srcs = glob(["**"]),
    visibility = ["//visibility:public"],
)
filegroup(
    name = "stdlib",
    srcs = glob(["gecode/flatzinc/mznlib/**"]),
    visibility = ["//visibility:public"],
)""",
    commit = "027c57889d66dd26ad8e1a419c2cda22ab0cf305",
    remote = "https://github.com/Gecode/gecode",
    shallow_since = "1555071280 +0200",
)
