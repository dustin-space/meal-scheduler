# TODO(agentydragon): also examine Python deps in requirements.txt
# TODO(agentydragon): add mode to update WORKSPACE

import ast
import datetime
import time

import requests
import timeago
from absl import app, flags

_GITHUB_USERNAME = flags.DEFINE_string("github_username", None,
                                       "GitHub username")
_GITHUB_ACCESS_TOKEN = flags.DEFINE_string("github_access_token", None,
                                           "GitHub access token")
_WORKSPACE = flags.DEFINE_string("workspace", None, "WORKSPACE path")


def get_auth():
    if _GITHUB_USERNAME.value and _GITHUB_ACCESS_TOKEN.value:
        return (_GITHUB_USERNAME.value, _GITHUB_ACCESS_TOKEN.value)
    else:
        return None


def get_default_branch(remote):
    url = 'https://api.github.com/repos/' + remote
    r = requests.get(url, auth=get_auth())
    return r.json()['default_branch']


def get_last_commit_on_branch(remote, branch):
    url = 'https://api.github.com/repos/' + remote + '/branches/' + branch
    r = requests.get(url, auth=get_auth())
    return r.json()['commit']


def get_commit_time(remote, commit):
    url = 'https://api.github.com/repos/' + remote + '/commits/' + commit
    r = requests.get(url, auth=get_auth())
    return get_time_of_commit(r.json())


def get_time_of_commit(commit):
    try:
        return datetime.datetime.strptime(commit['commit']['author']['date'],
                                          "%Y-%m-%dT%H:%M:%SZ")
    except KeyError:
        print(commit)
        raise


def get_git_repos(tree):
    repo_to_commit = {}

    # TODO: it should be possible to get Git repositories by querying Bazel
    for expr in tree.body:
        func_name = expr.value.func.id
        if func_name not in {'git_repository', 'new_git_repository'}:
            continue
        #print(func_name)
        # for git_repository, new_git_repository: extract commit, remote

        #print(ast.dump(expr))
        values = {}
        for keyword in expr.value.keywords:
            if keyword.arg not in {'commit', 'remote'}:
                continue
            assert keyword.arg not in values
            assert isinstance(keyword.value, ast.Constant)
            values[keyword.arg] = keyword.value.value

        remote = values['remote']
        commit = values['commit']
        repo_to_commit[remote] = commit
    return repo_to_commit


def main(_):
    with open(_WORKSPACE.value) as f:
        tree = ast.parse(f.read())

    git_repos = get_git_repos(tree)

    for remote, commit in git_repos.items():
        assert 'github.com' in remote
        github_path = remote.replace('https://github.com/', '')

        used_commit_dt = get_commit_time(github_path, commit)
        default_branch = get_default_branch(github_path)
        last_commit = get_last_commit_on_branch(github_path, default_branch)
        last_commit_dt = get_time_of_commit(last_commit)

        if commit == last_commit['sha']:
            print(f"✓ {remote} at {commit} @ {used_commit_dt} (latest)")
        else:
            print(f"✘ {remote}:")
            print(
                f"   using {commit}, committed at {used_commit_dt} ({timeago.format(used_commit_dt, last_commit_dt)})"
            )
            print(
                f"   latest is {last_commit['sha']}, committed at {last_commit_dt}"
            )

        # TODO(agentydragon): print how far away are we


if __name__ == '__main__':
    flags.mark_flag_as_required(_WORKSPACE.name)
    app.run(main)
