#include <cstdio>
#include <fstream>
#include <iostream>
#include <string>

#include "absl/strings/str_join.h"
#include "google/protobuf/text_format.h"
#include "ortools/base/commandlineflags.h"
#include "ortools/base/integral_types.h"
#include "ortools/base/logging.h"
#include "ortools/sat/cp_model.h"
#include "ortools/sat/cp_model_checker.h"
#include "ortools/sat/model.h"
#include "plan/planner.pb.h"

ABSL_FLAG(std::string, input_textproto_path, "", "Input textproto.");
ABSL_FLAG(std::string, output_textproto_path, "", "Output textproto.");

// Variables:
//    int stock[time][ingredient]:
//          how much stock we have of each ingredient on the day
//    int buy_product[time][product]:
//          how much of the product do we get delivered on the day
//    int obtain_externally[time][ingredient]:
//          how much of the ingredient we need to get externally
//    int buy_ingredient[time][ingredient]:
//          how much of the ingredient do we get delivered on the day
//    int cook[time][ingredient]:
//          how much of the ingredient we use for cooking on the day
//    int portions[time]:
//          how many portions we cook on the day
//    int recipe[time]:
//          index of recipe we cook on the day
//   bool ordering[time]:
//          whether we are ordering on the day
//    int bought[time]:
//          amount of products bought on the day
//
// Flow:
//   1.    stock[t]
//   2. +  buy[t]
//   3. +  obtain_externally[t]
//   4. -  cook[t]
//   5. -> stock[t+1]

namespace {

using ::google::protobuf::RepeatedField;
using ::google::protobuf::RepeatedPtrField;
using ::google::protobuf::TextFormat;
using ::meal_scheduler_proto::planner::Input;
using ::meal_scheduler_proto::planner::MaxOfRecipes;
using ::meal_scheduler_proto::planner::Output;
using ::meal_scheduler_proto::planner::Plan;
using ::meal_scheduler_proto::planner::PlanIngredient;
using ::meal_scheduler_proto::planner::PlanMeal;
using ::meal_scheduler_proto::planner::Product;
using ::meal_scheduler_proto::planner::Recipe;
using ::operations_research::Domain;
using ::operations_research::sat::BoolVar;
using ::operations_research::sat::CpModelBuilder;
using ::operations_research::sat::CpSolverResponse;
using ::operations_research::sat::CpSolverStatus;
using ::operations_research::sat::DecisionStrategyProto;
using ::operations_research::sat::IntVar;
using ::operations_research::sat::LinearExpr;
using ::operations_research::sat::Model;
using ::operations_research::sat::NewSatParameters;
using ::operations_research::sat::Not;
using ::operations_research::sat::SatParameters;
using ::operations_research::sat::SolutionIntegerValue;
using ::operations_research::sat::TableConstraint;
using ::operations_research::sat::ValidateCpModel;

std::vector<IntVar> MakeIntVars(CpModelBuilder& cp_model, int n,
                                const Domain& domain, const std::string& name,
                                const std::string& index) {
  std::vector<IntVar> vars(n);
  for (int i = 0; i < n; ++i) {
    vars[i] = cp_model.NewIntVar(domain).WithName(
        absl::StrCat(name, "[", index, "=", i, "]"));
  }
  return vars;
}

std::vector<BoolVar> MakeBoolVars(CpModelBuilder& cp_model, int n,
                                  const std::string& name,
                                  const std::string& index) {
  std::vector<BoolVar> vars(n);
  for (int i = 0; i < n; ++i) {
    vars[i] = cp_model.NewBoolVar().WithName(
        absl::StrCat(name, "[", index, "=", i, "]"));
  }
  return vars;
}

class InputWrapper {
 public:
  explicit InputWrapper(const Input& input)
      : input_(input),
        ingredient_amount_domain_(0, GetIngredientAmountUpperBound()),
        // TODO(agentydragon): tighter bound
        product_amount_domain_(0, 100),
        // TODO(agentydragon): tighter bound
        portions_domain_(0, 100),
        recipe_index_domain_(0, input_.recipe().size() - 1) {}

  // TODO(agentydragon): have a separate domain per ingredient
  const Domain& product_amount_domain() const { return product_amount_domain_; }
  const Domain& portions_domain() const { return portions_domain_; }
  const Domain& recipe_index_domain() const { return recipe_index_domain_; }
  int num_products() const { return input_.product().size(); }
  int num_ingredients() const { return input_.ingredient().size(); }
  int num_timesteps() const { return input_.meal_request().size(); }
  int num_recipes() const { return input_.recipe().size(); }

  int portions(int i) const { return input_.meal_request(i).portion_count(); }
  int stock(int i) const { return input_.ingredient(i).stock(); }

  int GetPortionUpperBound() const {
    int sum = 0;
    for (const auto& request : input_.meal_request()) {
      sum += request.portion_count();
    }
    return sum;
  }

  Domain GetIngredientDomain(int ingredient) const {
    return Domain(0, GetIngredientAmountUpperBound(ingredient));
  }

  int GetIngredientAmountUpperBound(int ingredient) const {
    const int in_stock = stock(ingredient);
    int max_per_portion = 0;
    for (int i = 0; i < num_recipes(); ++i) {
      const int in_recipe = GetAmountInRecipe(i, ingredient);
      if (max_per_portion < in_recipe) {
        max_per_portion = in_recipe;
      }
    }
    int max_per_product = 0;
    for (int i = 0; i < num_products(); ++i) {
      const int in_product = GetAmountInProduct(i, ingredient);
      if (max_per_product < in_product) {
        max_per_product = in_product;
      }
    }
    const int max = std::max(
        max_per_portion * GetPortionUpperBound() + max_per_product, in_stock);
    LOG(INFO) << "Maximum amount per ingredient " << ingredient << ": " << max;
    return max;
  }

  int GetIngredientAmountUpperBound() const {
    // Maximum of:
    //   - How much we have in stock
    //   - max(recipe use * total portions) + how much any product sells
    int max = 0;
    for (int i = 0; i < num_ingredients(); ++i) {
      int max_per_this_ingredient = GetIngredientAmountUpperBound(i);
      if (max < max_per_this_ingredient) {
        max = max_per_this_ingredient;
      }
    }
    LOG(INFO) << "Maximum amount per ingredient: " << max;
    QCHECK(max > 0);
    QCHECK(max <= 1000000000);
    return max;
  }

  int GetAmountInRecipe(int recipe, int ingredient) const {
    const Recipe& recipe_proto = input_.recipe(recipe);
    int amount = 0;
    for (const auto& item : recipe_proto.item()) {
      if (item.ingredient() == ingredient) {
        amount += item.amount();
      }
    }
    return amount;
  }

  int GetAmountInProduct(int product, int ingredient) const {
    const Product& product_proto = input_.product(product);
    int amount = 0;
    for (const auto& content : product_proto.content()) {
      if (content.ingredient() == ingredient) {
        amount += content.amount();
      }
    }
    return amount;
  }

  std::vector<int64_t> GetRecipeIngredientUse(int recipe) const {
    QCHECK(recipe >= 0);
    QCHECK(recipe < num_recipes());
    std::vector<int64_t> ingredient_use(num_ingredients());
    for (const auto& line : input_.recipe(recipe).item()) {
      ingredient_use[line.ingredient()] += line.amount();
    }
    return ingredient_use;
  }

  bool IngredientCanBeBought(int ingredient) const {
    for (int i = 0; i < num_products(); ++i) {
      if (GetAmountInProduct(i, ingredient) > 0) {
        return true;
      }
    }
    return false;
  }

  const RepeatedField<int>& banned_recipe() const {
    return input_.banned_recipe();
  }

  int max_ingredients_obtained_externally() const {
    return input_.max_ingredients_obtained_externally();
  }

  const RepeatedPtrField<MaxOfRecipes>& max_of_recipes() {
    return input_.max_of_recipes();
  }

  const Input& input_;
  Domain ingredient_amount_domain_;
  Domain product_amount_domain_;
  Domain portions_domain_;
  Domain recipe_index_domain_;
};

void BindBoolEquivalentToOr(const BoolVar& or_var,
                            const std::vector<BoolVar>& literals,
                            const std::string& name, CpModelBuilder& cp_model) {
  for (const BoolVar& literal : literals) {
    cp_model.AddImplication(Not(or_var), Not(literal));
  }
  cp_model.AddBoolOr(literals).OnlyEnforceIf(or_var).WithName(
      absl::StrCat("BindBoolEquivalentToOr-", name));
}

void BindOneHotBoolean(const IntVar& hot_index,
                       const std::vector<BoolVar>& bools,
                       CpModelBuilder& cp_model) {
  std::vector<IntVar> bools_as_ints;
  for (const BoolVar& v : bools) {
    bools_as_ints.push_back(v);
  }
  cp_model.AddVariableElement(hot_index, bools_as_ints,
                              cp_model.NewConstant(1));
  cp_model.AddEquality(LinearExpr::BooleanSum(bools), cp_model.NewConstant(1));
}

std::vector<IntVar> MakeIngredientAmountVars(const InputWrapper& input,
                                             CpModelBuilder& cp_model,
                                             const std::string& name) {
  std::vector<IntVar> vars(input.num_ingredients());
  for (int i = 0; i < input.num_ingredients(); ++i) {
    vars[i] = cp_model.NewIntVar(input.GetIngredientDomain(i))
                  .WithName(absl::StrCat(name, "[ingredient=", i, "]"));
  }
  return vars;
}

struct Timestep {
  // Indexed by ingredient ID.
  std::vector<IntVar> stock;
  std::vector<IntVar> buy_ingredient;
  std::vector<IntVar> obtain_externally;
  std::vector<IntVar> cook_per_portion;
  std::vector<IntVar> cook;
  // Indexed by product ID.
  std::vector<IntVar> buy_product;
  std::vector<BoolVar> buying_product;

  // Indexed by recipe ID.
  std::vector<BoolVar> cooking_recipe;

  IntVar portions;
  IntVar recipe;
  // Whether we are ordering on this day.
  BoolVar ordering;
  IntVar bought;

  void AddVariables(const InputWrapper& input, int time,
                    CpModelBuilder& cp_model) {
    stock = MakeIngredientAmountVars(input, cp_model,
                                     absl::StrCat("stock[time=", time, "]"));
    buy_ingredient = MakeIngredientAmountVars(
        input, cp_model, absl::StrCat("buy_ingredient[time=", time, "]"));
    obtain_externally = MakeIngredientAmountVars(
        input, cp_model, absl::StrCat("obtain_externally[time=", time, "]"));
    cook = MakeIngredientAmountVars(input, cp_model,
                                    absl::StrCat("cook[time=", time, "]"));
    // TODO(agentydragon): cook_per_portion can have tighter bounds
    cook_per_portion = MakeIngredientAmountVars(
        input, cp_model, absl::StrCat("cook_per_portion[time=", time, "]"));
    buy_product = MakeIntVars(
        cp_model, input.num_products(), input.product_amount_domain(),
        absl::StrCat("buy_product[time=", time, "]"), "product");
    buying_product = MakeBoolVars(
        cp_model, input.num_products(),
        absl::StrCat("buying_product[time=", time, "]"), "product");
    portions = cp_model.NewIntVar(input.portions_domain())
                   .WithName(absl::StrCat("portions[time=", time, "]"));
    recipe = cp_model.NewIntVar(input.recipe_index_domain())
                 .WithName(absl::StrCat("recipe[time=", time, "]"));
    ordering = cp_model.NewBoolVar().WithName(
        absl::StrCat("ordering[time=", time, "]"));
    bought = cp_model.NewIntVar(input.product_amount_domain())
                 .WithName(absl::StrCat("bought[time=", time, "]"));

    cooking_recipe =
        MakeBoolVars(cp_model, input.num_recipes(),
                     absl::StrCat("cooking_recipe[time=", time, "]"), "recipe");
  }

  void AddConstraints(const InputWrapper& input, CpModelBuilder& cp_model);

  std::string IngredientDebugString(int ingredient,
                                    const CpSolverResponse& response) const {
    std::string result =
        absl::StrCat("@", SolutionIntegerValue(response, stock[ingredient]));
    int bought = SolutionIntegerValue(response, buy_ingredient[ingredient]);
    if (bought > 0) {
      absl::StrAppend(&result, " +", bought);
    }
    int external =
        SolutionIntegerValue(response, obtain_externally[ingredient]);
    if (external > 0) {
      absl::StrAppend(&result, " !", external);
    }
    int cooked = SolutionIntegerValue(response, cook[ingredient]);
    if (cooked > 0) {
      absl::StrAppend(&result, " -", cooked);
    }
    return result;
  }
};

void BindStock(const InputWrapper& input, const Timestep& before,
               const std::vector<IntVar>& stock_after,
               CpModelBuilder& cp_model) {
  for (int i = 0; i < input.num_ingredients(); ++i) {
    // stock_after[i] =
    //     before.stock[i] + before.buy_ingredient[i] +
    //     before.obtain_externally[i] - before.cook[i];
    cp_model.AddEquality(
        stock_after[i],
        LinearExpr::ScalProd({before.stock[i], before.buy_ingredient[i],
                              before.obtain_externally[i], before.cook[i]},
                             {1, 1, 1, -1}));
  }
}

void BindTimesteps(const InputWrapper& input, const Timestep& before,
                   const Timestep& after, CpModelBuilder& cp_model) {
  BindStock(input, before, after.stock, cp_model);
}

void BindNumIngredientsBought(const InputWrapper& input,
                              const std::vector<IntVar>& buy_ingredient,
                              const std::vector<IntVar>& buy_product,
                              CpModelBuilder& cp_model) {
  for (int i = 0; i < input.num_ingredients(); ++i) {
    std::vector<IntVar> product_vars;
    std::vector<int64_t> amounts;
    for (int j = 0; j < input.num_products(); ++j) {
      int amount_in_product = input.GetAmountInProduct(j, i);
      if (amount_in_product == 0) {
        continue;
      }
      product_vars.push_back(buy_product[j]);
      amounts.push_back(amount_in_product);
    }
    // buy_ingredient[i] = product bought * how many of ingredient it provides
    cp_model.AddEquality(buy_ingredient[i],
                         LinearExpr::ScalProd(product_vars, amounts));
  }
}

void BindCookPerPortion(const InputWrapper& input, const IntVar& recipe,
                        const std::vector<IntVar>& cook_per_portion,
                        CpModelBuilder& cp_model) {
  std::vector<IntVar> assignments;
  assignments.push_back(recipe);
  absl::c_copy(cook_per_portion, std::back_inserter(assignments));
  TableConstraint table = cp_model.AddAllowedAssignments(assignments);
  for (int i = 0; i < input.num_recipes(); ++i) {
    std::vector<int64_t> tuple;
    tuple.push_back(i);
    absl::c_copy(input.GetRecipeIngredientUse(i), std::back_inserter(tuple));
    table.AddTuple(tuple);
  }
}

void BindCook(const InputWrapper& input, const std::vector<IntVar>& cook,
              const std::vector<IntVar>& cook_per_portion,
              const IntVar& portions, CpModelBuilder& cp_model) {
  for (int i = 0; i < input.num_ingredients(); ++i) {
    // cook[i] = cook_per_portions[i] * portions
    cp_model.AddProductEquality(cook[i], {cook_per_portion[i], portions});
  }
}

void BindVarIsNonzero(const IntVar& int_var, const BoolVar& nonzero_var,
                      CpModelBuilder& cp_model) {
  cp_model.AddEquality(int_var, cp_model.NewConstant(0))
      .OnlyEnforceIf(Not(nonzero_var));
  cp_model.AddNotEqual(int_var, cp_model.NewConstant(0))
      .OnlyEnforceIf(nonzero_var);
}

void BindOrdering(const InputWrapper& input, const BoolVar& ordering,
                  const std::vector<IntVar>& buy_product,
                  const std::vector<BoolVar>& buying_product,
                  CpModelBuilder& cp_model) {
  if (input.num_products() == 0) {
    for (int i = 0; i < input.num_products(); ++i) {
      cp_model.AddEquality(buy_product[i], cp_model.NewConstant(0));
      cp_model.AddEquality(buying_product[i], cp_model.NewConstant(0));
    }
    cp_model.AddEquality(ordering, cp_model.NewConstant(0));
    return;
  }
  for (int i = 0; i < input.num_products(); ++i) {
    BindVarIsNonzero(buy_product[i], buying_product[i], cp_model);
  }
  BindBoolEquivalentToOr(ordering, buying_product, "ordering", cp_model);
}

void BindBought(const IntVar& bought, const std::vector<IntVar>& buy_product,
                CpModelBuilder& cp_model) {
  cp_model.AddEquality(bought, LinearExpr::Sum(buy_product));
}

void BanRecipe(int recipe, const std::vector<Timestep>& timesteps,
               CpModelBuilder& cp_model) {
  for (const Timestep& timestep : timesteps) {
    cp_model.AddNotEqual(timestep.recipe, cp_model.NewConstant(recipe));
  }
}

LinearExpr GetProductsBought(const std::vector<Timestep>& timesteps) {
  std::vector<IntVar> vars;
  for (const Timestep& timestep : timesteps) {
    vars.push_back(timestep.bought);
  }
  return LinearExpr::Sum(vars);
}

LinearExpr GetIngredientObtainedExternally(
    const InputWrapper& input, int ingredient,
    const std::vector<Timestep>& timesteps) {
  std::vector<IntVar> got_external(input.num_timesteps());
  for (int j = 0; j < input.num_timesteps(); ++j) {
    got_external[j] = timesteps[j].obtain_externally[ingredient];
  }
  return LinearExpr::Sum(got_external);
}

void Timestep::AddConstraints(const InputWrapper& input,
                              CpModelBuilder& cp_model) {
  // buy_ingredient[i] = product bought * how many of ingredient it provides
  BindNumIngredientsBought(input, buy_ingredient, buy_product, cp_model);

  // cook_per_portion[i] is indexed from recipe cook_per_portion by recipe
  BindCookPerPortion(input, recipe, cook_per_portion, cp_model);

  // cook[i] = cook_per_portion[i] * portions
  BindCook(input, cook, cook_per_portion, portions, cp_model);

  // ordering <=> any(buy_product[*] > 0)
  BindOrdering(input, ordering, buy_product, buying_product, cp_model);

  // bought = sum(buy_product[*])
  BindBought(bought, buy_product, cp_model);

  // cooking_recipe[i] <=> (recipe = i)
  BindOneHotBoolean(recipe, cooking_recipe, cp_model);
}

void BindInitialStock(const InputWrapper& input,
                      const std::vector<IntVar>& stock,
                      CpModelBuilder& cp_model) {
  for (int i = 0; i < input.num_ingredients(); ++i) {
    cp_model.AddEquality(stock[i], input.stock(i));
  }
}

void PrintObjective(const CpSolverResponse& r) {
  LOG(INFO) << "objective: " << r.objective_value()
            << " conflicts: " << r.num_conflicts()
            << " integer_propagations: " << r.num_integer_propagations()
            << " binary_propagations: " << r.num_binary_propagations()
            << " wall_time: " << r.wall_time();
}

Output GetOutputFromResponse(const InputWrapper& input,
                             const std::vector<Timestep>& timesteps,
                             const std::vector<IntVar>& final_stock,
                             const std::vector<IntVar>& total_gotten_externally,
                             const CpSolverResponse& response) {
  Output output;
  if (response.status() != CpSolverStatus::OPTIMAL &&
      response.status() != CpSolverStatus::FEASIBLE) {
    output.mutable_unfeasible();
    return output;
  }
  Plan* plan = output.mutable_plan();
  for (int i = 0; i < input.num_timesteps(); ++i) {
    const Timestep& timestep = timesteps[i];
    int recipe = SolutionIntegerValue(response, timestep.recipe);
    int portions = SolutionIntegerValue(response, timestep.portions);
    PlanMeal* meal = plan->add_meal();
    meal->set_recipe(recipe);
    meal->set_portions(portions);
    for (int j = 0; j < input.num_ingredients(); ++j) {
      meal->add_ingredient_use(
          SolutionIntegerValue(response, timestep.cook[j]));
    }
  }
  for (int i = 0; i < input.num_products(); ++i) {
    const Timestep& timestep = timesteps[0];
    plan->add_products(SolutionIntegerValue(response, timestep.buy_product[i]));
  }

  PrintObjective(response);
  for (int i = 0; i < input.num_ingredients(); ++i) {
    LOG(INFO) << "ingredient " << i << ": "
              << absl::StrJoin(
                     timesteps, " → ",
                     [&](std::string* out, const Timestep& timestep) {
                       absl::StrAppend(
                           out, timestep.IngredientDebugString(i, response));
                     })
              << " → " << SolutionIntegerValue(response, final_stock[i]);
    PlanIngredient* ingredient = plan->add_ingredient();
    ingredient->set_obtain_externally(
        SolutionIntegerValue(response, total_gotten_externally[i]));

    // TODO(agentydragon): the total_use field should eventually become
    // unnecessary.
    int total_use = 0;
    for (const Timestep& timestep : timesteps) {
      total_use += SolutionIntegerValue(response, timestep.cook[i]);
    }
    ingredient->set_total_use(total_use);
  }
  return output;
}

Output DoPlan(const Input& input_proto) {
  CpModelBuilder cp_model;

  InputWrapper input(input_proto);

  std::vector<Timestep> timesteps(input.num_timesteps());
  for (unsigned int i = 0; i < timesteps.size(); ++i) {
    Timestep& timestep = timesteps[i];
    timestep.AddVariables(input, i, cp_model);
    timestep.AddConstraints(input, cp_model);
  }
  for (int i = 0; i < input.num_timesteps() - 1; ++i) {
    BindTimesteps(input, timesteps[i], timesteps[i + 1], cp_model);
  }
  std::vector<IntVar> final_stock =
      MakeIngredientAmountVars(input, cp_model, "final_stock");
  BindStock(input, timesteps[input.num_timesteps() - 1], final_stock, cp_model);

  std::vector<BoolVar> recipe_cooked = MakeBoolVars(
      cp_model, input.num_recipes(), "recipe_cooked_anytime", "recipe");
  for (int i = 0; i < input.num_recipes(); ++i) {
    std::vector<BoolVar> cooked_at_time;
    for (const Timestep& timestep : timesteps) {
      cooked_at_time.push_back(timestep.cooking_recipe[i]);
    }
    BindBoolEquivalentToOr(recipe_cooked[i], cooked_at_time,
                           absl::StrCat("recipe_cooked[", i, "]"), cp_model);
  }

  // Set initial stock.
  BindInitialStock(input, timesteps[0].stock, cp_model);

  // Only allow buying on the first day.
  for (int i = 1; i < input.num_timesteps(); ++i) {
    cp_model.AddEquality(timesteps[i].ordering, cp_model.FalseVar());
  }

  // Set portion counts per day.
  for (int i = 0; i < input.num_timesteps(); ++i) {
    cp_model.AddEquality(timesteps[i].portions,
                         cp_model.NewConstant(input.portions(i)));
  }

  // Do not cook banned recipes.
  for (int recipe : input.banned_recipe()) {
    BanRecipe(recipe, timesteps, cp_model);
  }

  // Cook different meals.
  std::vector<IntVar> recipe_indices;
  for (int i = 0; i < input.num_timesteps(); ++i) {
    recipe_indices.push_back(timesteps[i].recipe);
  }
  cp_model.AddAllDifferent(recipe_indices);

  // Count externally obtained ingredients.
  std::vector<BoolVar> obtained_ingredient_externally =
      MakeBoolVars(cp_model, input.num_ingredients(),
                   "obtained_ingredient_externally", "ingredient");
  std::vector<IntVar> total_gotten_externally =
      MakeIngredientAmountVars(input, cp_model, "total_gotten_externally");
  // TODO(agentydragon): only allow obtaining externally on the first day?
  for (int i = 0; i < input.num_ingredients(); ++i) {
    cp_model.AddEquality(total_gotten_externally[i],
                         GetIngredientObtainedExternally(input, i, timesteps));
    BindVarIsNonzero(total_gotten_externally[i],
                     obtained_ingredient_externally[i], cp_model);
    // If we obtained an ingredient externally, we should use up all of it.
    cp_model.AddEquality(final_stock[i], cp_model.NewConstant(0))
        .OnlyEnforceIf(obtained_ingredient_externally[i]);

    // If the ingredient has a product, disallow getting it externally.
    if (input.IngredientCanBeBought(i)) {
      cp_model.AddEquality(obtained_ingredient_externally[i],
                           cp_model.NewConstant(0));
    }
  }
  if (input.max_ingredients_obtained_externally() >= 0) {
    cp_model.AddLessOrEqual(
        LinearExpr::BooleanSum(obtained_ingredient_externally),
        input.max_ingredients_obtained_externally());
  }

  // Bind max_of_recipes.
  for (const MaxOfRecipes& max_of_recipes : input.max_of_recipes()) {
    std::vector<BoolVar> cooked_in_group;
    for (int recipe_id : max_of_recipes.recipe()) {
      cooked_in_group.push_back(recipe_cooked[recipe_id]);
    }
    cp_model.AddLessOrEqual(LinearExpr::BooleanSum(cooked_in_group),
                            cp_model.NewConstant(max_of_recipes.max_meals()));
  }

  cp_model.Minimize(GetProductsBought(timesteps));

  SatParameters parameters;
  // parameters.set_search_branching(SatParameters::FIXED_SEARCH);
  parameters.set_num_search_workers(4);

  // model.Add(NewFeasibleSolutionObserver([&](const CpSolverResponse& r) {
  //  LOG(INFO) << ...;
  //}));

  const auto m = cp_model.Build();
  LOG(INFO) << "validate: " << ValidateCpModel(m);

  Model model;
  model.Add(NewSatParameters(parameters));
  model.Add(NewFeasibleSolutionObserver(PrintObjective));

  const CpSolverResponse response = SolveCpModel(m, &model);
  LOG(INFO) << response.DebugString();
  return GetOutputFromResponse(input, timesteps, final_stock,
                               total_gotten_externally, response);
}

}  // namespace

int main(int argc, char** argv) {
  absl::ParseCommandLine(argc, argv);
  QCHECK(!absl::GetFlag(FLAGS_input_textproto_path).empty());
  QCHECK(!absl::GetFlag(FLAGS_output_textproto_path).empty());

  std::ifstream t(absl::GetFlag(FLAGS_input_textproto_path));
  std::string str((std::istreambuf_iterator<char>(t)),
                  std::istreambuf_iterator<char>());
  Input input;
  CHECK(TextFormat::ParseFromString(str, &input));
  Output output = DoPlan(input);
  std::ofstream o(absl::GetFlag(FLAGS_output_textproto_path));
  std::string output_str;
  QCHECK(TextFormat::PrintToString(output, &output_str));
  o << output_str;
}
