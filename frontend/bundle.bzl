load("@my_deps//:requirements.bzl", "requirement")
load("@rules_python//python:python.bzl", "py_binary")

def bundle(name, modules):
    # TODO(agentydragon): is there a better way to refer to the current
    # Bazel workspace than explicitly naming it with @...?
    py_binary(
        name = name,
        srcs = ["@meal_scheduler//frontend:frontend_main.py"],
        deps = [
            "@meal_scheduler//frontend:celery_app",
            "@meal_scheduler//frontend:frontend_lib",
            "@meal_scheduler//frontend:sequential_hook",
            requirement("absl-py"),
            requirement("celery"),
        ] + modules,
        main = "@meal_scheduler//frontend:frontend_main.py",
    )
