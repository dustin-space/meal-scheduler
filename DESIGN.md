There are recipes.

For cooking a recipe, you need to have the right ingredients in the right amounts.

For example: to cook the "vegan chocolate ice cream" recipe for 2 people, you need 3 bananas and 4 tbsp of cocoa powder.

This program will, at any time, know:

* How much of which ingredients we have available right now
* How many people will want to cook in the next, say, week.

When we run it, it will generate a schedule of:

* What meals to cook in the future.
* What ingredients we need to order, and when will they be needed.

Constraints on the schedule:

* We need to be able to cook the scheduled recipes.
* Ingredients will be used before they expire.
  * Fresh ingredients: need to be used in a day or two after buying (e.g., fresh cilantro).
  * Durable ingredients: can stay OK for years (e.g., rice)
  * Canned ingredients: once opened, need to be consumed within, say, 2 weeks (e.g., canned coconut milk)
* Allow us to play with the schedule and automatically adjust - e.g., "hey I really want to cook eggs tomorrow" would make it recompute a plan, such that any orders that we currently have outstanding will be used.
* Compute and allow for optimizing the price.
* Satisficing for how much of what nutrients you should be getting.
* Prefer variety.

More features that would be nice:

* Some ingredients can be substituted - e.g., different types of rice, cow milk and rice milk, dried cranberries and raisins, etc.

# Planner plan

## Phase 1

### Input

* How many meals to prepare
* How many people will be eating in each meal

### Output

* Plan for future orders

### Constrains

* No repeating meals

### Optimization

* Minimize cost (including shipping)
* Minimize leftover supplies
* Minimize integral of amount of supplies in fridge over time.

## Phase 2

Phase 1 +

* Start with current content of fridge.
* Fresh food must be consumed before it spoils.

## Phase 3

* Maintain at least N reserve portions that can be cooked at any time.
* Integration with available LeShop delivery slots.
* Optional ingredients
* Replaceable ingredients
* Uncertainty

# Planner algorithm

## Convert to common units

First use equivalences to convert everything to grams / milliliters / pieces.

## Representation

* For each day:
  * `bool order`: whether we decide that we want an order to arrive on this day.
    (For days that are too close to the present, this may be fixed to false.)
  * For each LeShop product:
    * `unsigned int arrives`: How many copies should arrive in today's order.
      The order is assumed to arrive before our cooking.
      `!order ==> arrives = 0`.
  * For each ingredient:
    * `unsigned int available`: How much of it is currently available, in its
      common units.
      Constrain: `available_t >= 0`.

      * Without expiration: `available_t == available_{t-1} + arrives - used`
      * With expiration: `available_t == (how much we bought in the last
        freshness window) - (how much we used in the last freshness window)`.
    * `unsigned int used`: How much of it will be used in today's cooking.
      Comes from the recipe(s) that will be cooked today.
  * For each cooking today:
    * `unsigned int recipe`: ID of the recipe to cook
    * `unsigned int portions`: Fixed input: how many people will eat the food.
* `unsigned int cost`: total cost of order deliveries and products ordered.
* No repeating recipes in each N-day window.
