from typing import List

from meal_scheduler import unit_amount as unit_amount_lib
from meal_scheduler.inventory_py_proto_pb import inventory_pb2


def amount_in_stock_to_str(stock_amount):
    if not stock_amount:
        return "none"
    if stock_amount.HasField('exact'):
        return unit_amount_lib.unit_amount_to_str(stock_amount.exact)
    if stock_amount.HasField('lower_bound'):
        return '>= ' + unit_amount_lib.unit_amount_to_str(
            stock_amount.lower_bound)
    return str(stock_amount)


class StockDatabase:

    def __init__(self, stock_items: List[inventory_pb2.StockItem]):
        self._stock_items = stock_items

    def find_stock_item(self, ingredient_id):
        for item in self._stock_items:
            if item.ingredient_id == ingredient_id:
                return item
        return None

    def find_amount_in_stock(self, ingredient_id):
        for item in self._stock_items:
            if item.ingredient_id == ingredient_id:
                return item.amount
        return None

    @property
    def stock(self):
        return self._stock_items

    @property
    def ingredient_ids_in_stock(self):
        return {item.ingredient_id for item in self._stock_items}

    @property
    def ingredient_ids_with_enough(self):
        return {
            item.ingredient_id
            for item in self._stock_items
            if item.amount.HasField('enough')
        }
