"""
bazel run //db:import_products_main -- \
  --alsologtostderr \
  --sqlite_db=$(pwd)/db.sqlite \
  --products=$(pwd)/product.textproto
"""

from absl import app, flags
from google.protobuf import text_format
from meal_scheduler.db import db as db_lib
from meal_scheduler.inventory_py_proto_pb import inventory_pb2

flags.DEFINE_string("sqlite_db", None, "path to sqlite db")
flags.DEFINE_string("products", None, "textproto to insert products from")

FLAGS = flags.FLAGS


def main(_):
    db = db_lib.open_db(FLAGS.sqlite_db)
    with open(FLAGS.products, 'rb') as f:
        proto = text_format.Parse(f.read(), inventory_pb2.Products())
    for product in proto.product:
        if not product.merchant:
            product.merchant = 'leshop'
        db_lib.insert_product(db, product)
    db.close()


if __name__ == '__main__':
    flags.mark_flags_as_required(['sqlite_db', 'products'])
    app.run(main)
