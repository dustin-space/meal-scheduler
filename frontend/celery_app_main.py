"""
To just run a worker:

bazel run //frontend:celery_app_main -- \
  --alsologtostderr \
  --config_yaml=$(pwd)/../dustin-space/config.yaml \
  --sqlite_db=$(pwd)/../dustin-space/meal_scheduler_db/dustinspace.sqlite

To purge:

bazel run //frontend:celery_app_main -- \
  --alsologtostderr \
  --config_yaml=$(pwd)/../dustin-space/config.yaml \
  --sqlite_db=$(pwd)/../dustin-space/meal_scheduler_db/dustinspace.sqlite \
  purge

Unfortunately we cannot use celery inspect or Flower because the transport
does not support broadcasts...
"""

import importlib
import yaml
from absl import flags, app, logging
from meal_scheduler.frontend import celery_app, sequential_hook

_SQLITE_DB = flags.DEFINE_string("sqlite_db", None, "path to sqlite db")
_CONFIG_YAML = flags.DEFINE_string("config_yaml", None, "path to yaml config")


def main(celery_argv):
    with open(_CONFIG_YAML.value) as f:
        config = yaml.load(f)

    celery_app.CELERY_APP.conf.update(
        broker_url=('sqla+sqlite:///' + _SQLITE_DB.value),
        result_backend=('db+sqlite:///' + _SQLITE_DB.value))

    hooks = sequential_hook.SequentialHook()
    for module_name in config['modules']:
        module = importlib.import_module(module_name)
        hooks.add_hook(module.HOOKS)

    # TODO(agentydragon): do this per module instead
    hooks.apply_config(config)
    hooks.register_celery_tasks(celery_app.CELERY_APP)

    argv = ['worker', '--loglevel=INFO']
    if len(celery_argv) > 1:
        argv = celery_argv[1:]
    celery_app.CELERY_APP.start(argv)


if __name__ == '__main__':
    flags.mark_flags_as_required((_SQLITE_DB.name, _CONFIG_YAML.name))
    app.run(main)
