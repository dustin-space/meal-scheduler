import datetime

from google.protobuf import timestamp_pb2
from meal_scheduler.inventory_py_proto_pb import inventory_pb2


def timestamp_proto_to_epoch_seconds(
        timestamp: timestamp_pb2.Timestamp) -> float:
    return timestamp.seconds + timestamp.nanos / 1_000_000_000


def timestamp_proto_to_nanos(timestamp: timestamp_pb2.Timestamp) -> int:
    return timestamp.seconds * 1_000_000_000 + timestamp.nanos


def datetime_to_nanos(dt: datetime.datetime) -> int:
    return dt.timestamp() * 1_000_000_000


def date_to_datetime(date: inventory_pb2.Date) -> datetime.datetime:
    return datetime.datetime(item.date.year, item.date.month, item.date.day)


def timestamp_proto_to_datetime(
        timestamp: timestamp_pb2.Timestamp) -> datetime.datetime:
    return datetime.datetime.fromtimestamp(
        timestamp_proto_to_epoch_seconds(timestamp))


def timestamp_proto_from_epoch_nanos(
        epoch_nanos: int) -> timestamp_pb2.Timestamp:
    seconds, ns = divmod(epoch_nanos, 1_000_000_000)
    return timestamp_pb2.Timestamp(seconds=int(seconds), nanos=int(ns))


def datetime_from_nanos(epoch_nanos: int) -> datetime.datetime:
    return timestamp_proto_to_datetime(
        timestamp_proto_from_epoch_nanos(epoch_nanos))


def date_proto_to_date(d):
    return datetime.date(year=d.year, month=d.month, day=d.day)


def date_proto_from_iso8601(s):
    return date_to_date_proto(datetime.date.fromisoformat(s))


def date_proto_to_iso8601(d):
    return f'{d.year:04d}-{d.month:02d}-{d.day:02d}'


def date_to_date_proto(d):
    return inventory_pb2.Date(year=d.year, month=d.month, day=d.day)
