"""
bazel run //db:import_recipes_main -- \
  --alsologtostderr \
  --sqlite_db=$(pwd)/db.sqlite \
  --insert_recipes_from=$(pwd)/recipes_import.textproto
"""

from absl import app, flags, logging
from google.protobuf import text_format
from meal_scheduler.db import db as db_lib
from meal_scheduler.db import import_recipes as import_recipes_lib
from meal_scheduler.inventory_py_proto_pb import inventory_pb2

_SQLITE_DB = flags.DEFINE_string("sqlite_db", None, "path to sqlite db")
_INSERT_RECIPES_FROM = flags.DEFINE_string("insert_recipes_from", None,
                                           "textproto to insert recipes from")

FLAGS = flags.FLAGS


def main(_):
    db = db_lib.open_db(_SQLITE_DB.value)
    with open(_INSERT_RECIPES_FROM.value, 'rb') as f:
        proto = text_format.Parse(f.read(), inventory_pb2.RecipeImport())
    stats = import_recipes_lib.import_recipes(db, proto)
    logging.info("Stats: %s", stats)
    db.close()


if __name__ == '__main__':
    flags.mark_flags_as_required([_SQLITE_DB.name, _INSERT_RECIPES_FROM.name])
    app.run(main)
