from meal_scheduler import unit_amount
from meal_scheduler.inventory_py_proto_pb import inventory_pb2


def amount_to_str(amount: inventory_pb2.RecipeAmount) -> str:
    if amount.HasField('unit_amount'):
        return unit_amount.unit_amount_to_str(amount.unit_amount)
    return str(amount)
