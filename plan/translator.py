import collections
import datetime
import functools
import re
from google.protobuf import text_format
from typing import Dict, FrozenSet, List, Optional, Set, Tuple

from absl import logging
from meal_scheduler import time_util
from meal_scheduler import unit as unit_lib
from meal_scheduler import unit_amount as unit_amount_lib
from meal_scheduler.data import conversions as conversions_lib
from meal_scheduler.data import products as products_lib
from meal_scheduler.data import recipes as recipe_lib
from meal_scheduler.data import stock as stock_lib
from meal_scheduler.diet_mapping_py_proto_pb import diet_mapping_pb2
from meal_scheduler.inventory_py_proto_pb import inventory_pb2
from meal_scheduler.plan import approximate_gcd
from meal_scheduler.plan.planner_py_proto_pb.plan import planner_pb2
from meal_scheduler.unit_py_proto_pb import unit_pb2


def _get_ingredient_diet_bits(ingredient_id: str,
                              diet_map: diet_mapping_pb2.DietMappings):
    diet_bits = set()
    for mapping in diet_map.diet_mapping:
        for regex in mapping.ingredient_id_regex:
            if re.match(regex, ingredient_id):
                diet_bits = diet_bits.union(set(mapping.diet_bit))
    return diet_bits


def _get_ingredient_ids_that_need_managing(
        recipes, products: List[inventory_pb2.Product],
        stock: stock_lib.StockDatabase):
    ingredient_ids = set.union(
        products_lib.get_ingredient_ids_with_products(products),
        stock.ingredient_ids_in_stock,
        *(recipe.ingredient_ids for recipe in recipes))
    ingredient_ids -= stock.ingredient_ids_with_enough
    return list(sorted(ingredient_ids))


def _sanitize_recipe_name(name):
    return re.sub('[^a-zA-Z0-9]', '_', name)


def _sanitize_ingredient_name(name):
    # TODO(agentydragon): test all cases
    return name.replace('/ingredient/', '').replace('/', '_').replace('-', '_')


class _IngredientAmountTranslator:

    def __init__(self, universal_unit, conversions, scale_factor):
        self._universal_unit = universal_unit
        self._scale_factor = scale_factor
        self._conversions = conversions

    def to_universal_unit(self,
                          unit_amount: inventory_pb2.UnitAmount,
                          scale=1) -> int:
        unit = unit_amount.unit
        if unit not in self._conversions:
            raise conversions_lib.NoConversionException(
                f"no conversion from {unit_lib.unit_name(unit)} to {unit_lib.unit_name(self._universal_unit)}"
            )
        # TODO(agentydragon): throw if not representable closely enough
        return round(unit_amount.amount * self._conversions[unit] *
                     self._scale_factor * scale)

    def universal_unit_to_unit_amount(
            self, universal_units: int) -> inventory_pb2.UnitAmount:
        return inventory_pb2.UnitAmount(unit=self._universal_unit,
                                        amount=universal_units /
                                        self._scale_factor)


def _pick_universal_unit(sources_to_unit_amounts):
    units = set(unit_amount.unit
                for unit_amounts in sources_to_unit_amounts.values()
                for unit_amount in unit_amounts)
    if 'stock' in sources_to_unit_amounts:
        # TODO(agentydragon): ugly
        unit = min(unit_amount.unit
                   for unit_amount in sources_to_unit_amounts['stock'])
        logging.info("picked unit %s because it's the one of stock", unit)
        return unit
    if any(key.startswith('product') for key in sources_to_unit_amounts):
        key_product = min(
            key for key in sources_to_unit_amounts if key.startswith('product'))
        # TODO(agentydragon): ugly
        unit = min(unit_amount.unit
                   for unit_amount in sources_to_unit_amounts[key_product])
        logging.info(
            "picked unit %s because it's the one of the minimal product (%s)",
            unit, key_product)
        return unit
    unit = min(units)
    logging.info("picked unit %s arbitrarily (because it's the minimum)", unit)
    return unit


def _get_scale_factor(amounts_in_universal_unit) -> float:
    nonzeros = list(filter(lambda x: x > 0, amounts_in_universal_unit))
    if not nonzeros:
        logging.info("all zeros")
        return 1
    else:
        gcd = approximate_gcd.approximate_gcd(nonzeros, tolerance=0.05)
        logging.info("approximate gcd: %f", gcd)
        return 1.0 / gcd


def _find_common_unit_for_ingredient(
        ingredient_id, sources_to_unit_amounts, convs,
        ingredient_database) -> _IngredientAmountTranslator:
    logging.info("ingredient %s", ingredient_id)
    logging.info(
        "sources_to_unit_amounts: %s", {
            key: ', '.join(
                text_format.MessageToString(item, as_one_line=True)
                for item in value)
            for key, value in sources_to_unit_amounts.items()
        })
    universal_unit = _pick_universal_unit(sources_to_unit_amounts)
    units = {
        unit_amount.unit
        for unit_amounts in sources_to_unit_amounts.values()
        for unit_amount in unit_amounts
    }
    conversions = conversions_lib.find_conversions(ingredient_id,
                                                   universal_unit, units, convs,
                                                   ingredient_database)

    isolated_units = units - set(conversions.keys())
    if isolated_units:
        logging.warning("isolated units: %s",
                        {unit_lib.unit_name(u) for u in isolated_units})

    amounts_in_universal_unit = {
        unit_amount.amount * conversions[unit_amount.unit]
        for unit_amounts in sources_to_unit_amounts.values()
        for unit_amount in unit_amounts
        if unit_amount.unit in conversions
    }
    logging.info("amounts in universal unit (%s): %s",
                 unit_lib.unit_name(universal_unit), amounts_in_universal_unit)
    scale_factor = _get_scale_factor(amounts_in_universal_unit)
    logging.info("scale factor: %s", scale_factor)

    return _IngredientAmountTranslator(universal_unit,
                                       conversions,
                                       scale_factor=scale_factor)


def find_ingredient_components(ingredient_database,
                               ingredient_ids) -> Set[FrozenSet[str]]:
    """Decomposes ingredient_database into connected components by subtype."""
    component = {
        ingredient_id: frozenset([ingredient_id])
        for ingredient_id in ingredient_ids
    }

    def _connect(a, b):
        set_a = component[a]
        set_b = component[b]
        u = set_a.union(set_b)
        for ingredient in u:
            component[ingredient] = u

    for ingredient in ingredient_database.ingredient:
        if ingredient.id not in ingredient_ids:
            continue
        for subtype in ingredient.subtypes:
            if subtype not in ingredient_ids:
                continue
            _connect(ingredient.id, subtype)

    return set(component.values())


def find_common_units(ingredient_unit_amounts, convs: inventory_pb2.Conversions,
                      ingredient_database, error_reporter,
                      force_same_quantum_for_connected_components: bool):
    ingredient_ids_mentioned = set(ingredient_unit_amounts.keys())
    if not (ingredient_ids_mentioned <= ingredient_database.ingredient_ids):
        raise Exception(
            f"used non-declared ingredients ({ingredient_ids_mentioned - ingredient_database.ingredient_ids})"
        )

    translators = {}

    #  - decompose ingredient hierarchy into connected components.
    if force_same_quantum_for_connected_components:
        components = find_ingredient_components(ingredient_database,
                                                ingredient_ids_mentioned)
    else:
        components = set(
            frozenset([ingredient_id])
            for ingredient_id in ingredient_ids_mentioned)

    # Go through components alphabetically, for logging.
    components = sorted(components, key=min)

    #  - within each connected component, we will choose *one* canonical
    #    unit. the canonical unit will be set explicitly for the supertypes.
    #  - for an ingredient, we can use conversions defined for
    #    the ingredient itself, or for its supertypes.
    #  - within each connected component, gather the unit amounts of all
    #    ingredients underneath, and convert them all into one quantum.
    for component in components:
        # Choose a canonical unit.
        canonical_units = {
            ingredient_database.find_ingredient_by_id(
                ingredient_id).canonical_unit for ingredient_id in component
        }
        if unit_pb2.UNKNOWN_UNIT in canonical_units:
            canonical_units.remove(unit_pb2.UNKNOWN_UNIT)
        if len(canonical_units) == 0:
            if len(component) > 1:
                error_reporter.report(
                    f"component {', '.join(sorted(component))} does not set a canonical unit"
                )
                continue
            ingredient_id = list(component)[0]
            sources_to_unit_amounts = ingredient_unit_amounts[ingredient_id]
            if len(sources_to_unit_amounts) == 0:
                # Component not used anywhere for anything.
                continue
            translators[ingredient_id] = _find_common_unit_for_ingredient(
                ingredient_id, sources_to_unit_amounts, convs,
                ingredient_database)
            continue

        canonical_unit = list(canonical_units)[0]
        if len(canonical_units) > 1:
            error_reporter.report(
                f"multiple canonical units forced in component, picked {canonical_unit} arbitrarily"
            )

        # Convert all unit_amounts for everything in this ingredient into
        # the canonical unit.
        canonical_unit_amounts = set()
        for ingredient_id in component:
            sources_to_unit_amounts = ingredient_unit_amounts[ingredient_id]
            all_unit_amounts = [
                unit_amount
                for unit_amounts in sources_to_unit_amounts.values()
                for unit_amount in unit_amounts
            ]
            cannot_convert = []

            for unit_amount in all_unit_amounts:
                converted_to_canonical = conversions_lib.convert_unit_amount_to_unit(
                    unit_amount, canonical_unit, convs, ingredient_database,
                    ingredient_id)
                if not converted_to_canonical:
                    # TODO(agentydragon): use multiset instead
                    cannot_convert.append(unit_amount)
                    continue

                if converted_to_canonical.amount not in canonical_unit_amounts:
                    # Only log each non-canonical amount once.
                    logging.info(
                        "from %s: %s -> %s", ingredient_id,
                        unit_amount_lib.unit_amount_to_str(unit_amount),
                        unit_amount_lib.unit_amount_to_str(
                            converted_to_canonical))
                    canonical_unit_amounts.add(converted_to_canonical.amount)

            if cannot_convert:
                message = ', '.join([
                    unit_amount_lib.unit_amount_to_str(unit_amount)
                    for unit_amount in cannot_convert
                ])
                error_reporter.report(
                    f"cannot convert {message} of {ingredient_id} to {unit_lib.unit_name(canonical_unit)}"
                )

        logging.info("canonical unit amounts: %s", canonical_unit_amounts)
        scale_factor = _get_scale_factor(canonical_unit_amounts)

        # Check the scale factor.
        # HACK: remove zeroes; stock can say 0
        nonzeroes = [amount for amount in canonical_unit_amounts if amount > 0]
        if nonzeroes:
            minimal = min(nonzeroes) * scale_factor
            maximal = max(nonzeroes) * scale_factor
            logging.info("range: %d - %d", minimal, maximal)
            assert minimal >= 1
            assert maximal < 1_000_000

        # Create translators with a common scale factor and canonical unit
        # for this component.
        for ingredient_id in component:
            sources_to_unit_amounts = ingredient_unit_amounts[ingredient_id]
            units = {
                unit_amount.unit
                for unit_amounts in sources_to_unit_amounts.values()
                for unit_amount in unit_amounts
            }
            units.add(canonical_unit)
            conversions = conversions_lib.find_conversions(
                ingredient_id, canonical_unit, units, convs,
                ingredient_database)
            logging.info(f"conversions for {ingredient_id}: {conversions}")
            translators[ingredient_id] = _IngredientAmountTranslator(
                canonical_unit, conversions, scale_factor=scale_factor)

    return translators


def _collect_unit_amounts_from_recipe(recipe, ingredient_units):
    # TODO(agentydragon): this can be reused in recipe conversions
    # to avoid having the division by served amount twice
    for ingredient_id, unit_amount in recipe.unit_amount_ingredients:
        if ingredient_id not in ingredient_units:
            continue
        identifier = 'recipe ' + recipe.title
        ingredient_units[ingredient_id][identifier].append(
            inventory_pb2.UnitAmount(unit=unit_amount.unit,
                                     amount=unit_amount.amount /
                                     recipe.serves.min))


def _get_product_unit_amount(
        product: inventory_pb2.Product) -> inventory_pb2.UnitAmount:
    ingredient_id = product.contents.ingredient_id
    return inventory_pb2.UnitAmount(
        unit=product.contents.amount.unit,
        amount=products_lib.find_unit_amount_in_product(product, ingredient_id))


def _collect_unit_amounts_from_product(product, ingredient_units):
    ingredient_id = product.contents.ingredient_id
    if ingredient_id not in ingredient_units:
        return
    identifier = f'product {product.merchant_product_id}'
    ingredient_units[ingredient_id][identifier].append(
        _get_product_unit_amount(product))


def _collect_unit_amounts_from_stock_item(item, ingredient_units):
    ingredient_id = item.ingredient_id
    if ingredient_id not in ingredient_units:
        return
    assert not item.amount.HasField(
        'enough'), "items with enough should already be filtered out"
    if item.amount.HasField('exact'):
        unit_amount = item.amount.exact
    elif item.amount.HasField('lower_bound'):
        unit_amount = item.amount.lower_bound
    else:
        raise Exception("unimplemented case")
    identifier = 'stock'
    ingredient_units[ingredient_id][identifier].append(unit_amount)


def collect_unit_amounts(
        ingredient_ids, recipeslist, products,
        stock) -> Dict[str, Dict[str, List[inventory_pb2.UnitAmount]]]:
    # {
    #   ingredient_id: {
    #     where it came from: List[UnitAmount]
    #   }
    # }
    ingredient_units = {
        ingredient_id: collections.defaultdict(list)
        for ingredient_id in ingredient_ids
    }
    for recipe in recipeslist:
        _collect_unit_amounts_from_recipe(recipe, ingredient_units)
    for product in products:
        _collect_unit_amounts_from_product(product, ingredient_units)
    for item in stock.stock:
        _collect_unit_amounts_from_stock_item(item, ingredient_units)
    return ingredient_units


class Translator:
    """Translates between database representation and planner representation.

    To make the planning problem easier to represent, the planner input/output
    is simplified compared to the database's representation.

    For the planner:
      - Ingredient amounts are integers.
      - Ingredient stock levels are also integers, and they:
        - Increase by being bought in products or obtained externally
        - Decrease by amount used for cooking.

    So:
      - We need to remove ingredients of which we have "enough" in stock, since
        they will not decrease from being cooked.
      - For each ingredient, we need to select a "quantum", which will be the
        unit for the ingredient's integer amounts.

        The "quantum"'s unit will be chosen as follows:
          - If the ingredient is in stock, the stock unit will be used.
          - Otherwise, if there are any products for the ingredient, the unit
            of the product with the lexicographically least ID will be picked.
          - Otherwise, it will be the Unit enum value of the minimal value that
            is present.

        The amount of the unit in the quantum will be used as the approximate
        GCD of all amounts convertible to the quantum.
    """

    def __init__(self,
                 recipes: List[recipe_lib.RecipeWrapper],
                 products: inventory_pb2.Products,
                 stock,
                 convs,
                 error_reporter,
                 ingredient_database,
                 diet_map: diet_mapping_pb2.DietMappings,
                 force_same_quantum_for_connected_components: bool,
                 merchant_product_statuses: Optional[List[
                     inventory_pb2.MerchantProductStatus]] = None):
        self._force_same_quantum_for_connected_components = force_same_quantum_for_connected_components
        self._ingredient_database = ingredient_database
        self._ingredient_ids = _get_ingredient_ids_that_need_managing(
            recipes, products, stock)
        self._error_reporter = error_reporter
        self._diet_map = diet_map
        # TODO(prvak): make this toggleable
        self._merchant_product_statuses = merchant_product_statuses

        ingredient_unit_amounts = collect_unit_amounts(self._ingredient_ids,
                                                       recipes, products, stock)

        self._ingredient_amount_translators = find_common_units(
            ingredient_unit_amounts,
            convs,
            ingredient_database,
            error_reporter,
            force_same_quantum_for_connected_components=(
                force_same_quantum_for_connected_components))
        kept_products = []
        for product in products:
            ingredient_id = product.contents.ingredient_id
            if ingredient_id not in self._ingredient_amount_translators:
                logging.info(
                    "not keeping product %d, it's %s which has no translator",
                    product.merchant_product_id, ingredient_id)
            elif ingredient_id not in self._ingredient_ids:
                raise Exception("argh")
            else:
                try:
                    self._ingredient_amount_translators[
                        ingredient_id].to_universal_unit(
                            _get_product_unit_amount(product))
                except conversions_lib.NoConversionException as e:
                    self._error_reporter.report(
                        f"Discarding product {product} due to unconvertable amount of {ingredient_id}: {e}"
                    )
                else:
                    kept_products.append(product)

        self._products = kept_products
        self._convs = convs
        # TODO(agentydragon): this should NOT actually do the conversion!
        # TODO(agentydragon): discard recipes that do not have an universal unit
        kept_recipes = []
        for recipe in recipes:
            used_bad_ingredient_ids = {
                ingredient_id for ingredient_id in recipe.ingredient_ids
                if ((ingredient_id not in self._ingredient_amount_translators)
                    and (ingredient_id not in stock.ingredient_ids_with_enough))
            }
            if used_bad_ingredient_ids:
                # TODO(agentydragon): explain the problem here
                self._error_reporter.report(
                    f"recipe {recipe.title} discarded due to use of {used_bad_ingredient_ids}"
                )
                continue

            # HACK: Recipes can also be discarded due to unconvertable amounts,
            # but that happens only when generating the PlanInput, not already
            # at this phase. Check here that the per-ingredient usage can be
            # generated so we can correctly discard recipes that cannot be
            # converted to the universal unit.
            ppr = self._get_recipe_ingredient_usage_per_portion(recipe)
            if ppr is None:
                # Recipe discarded.
                continue

            kept_recipes.append(recipe)
        self._recipes = kept_recipes
        self._stock = stock

    def _get_recipe_ingredient_usage_per_portion(
            self, recipe) -> List[planner_pb2.IngredientAmount]:
        do_discard = False
        requires = {}
        for recipe_line in recipe.recipe_proto.ingredients:
            if recipe_line.ingredient_id not in self._ingredient_ids:
                # TODO(agentydragon)
                continue
            # TODO(agentydragon): plus the others
            iid = self._get_ingredient_index(recipe_line.ingredient_id)
            # TODO(agentydragon): test this in check_lib
            assert iid not in requires, f"Recipe {recipe.title} duplicates lines for {recipe_line.ingredient_id}"

            # TODO: what if recipe does not have an unit amount??

            try:
                amount = (self._ingredient_amount_translators[
                    recipe_line.ingredient_id].to_universal_unit(
                        recipe_line.amount.unit_amount,
                        scale=1.0 / recipe.serves.min))
            except KeyError as e:
                # TODO(agentydragon): figure out how this can happen and fix it
                self._error_reporter.report(
                    f"Discarding recipe {recipe.title} due to {e}")
                do_discard = True
                continue
            except conversions_lib.NoConversionException as e:
                self._error_reporter.report(
                    f"Discarding recipe {recipe.title} due to unconvertable amount of {recipe_line.ingredient_id}: {e}"
                )
                do_discard = True
                continue
            if amount == 0:
                raise Exception(
                    f"zero in recipe {recipe.id} {recipe.title}: {recipe_line}")
            requires[iid] = amount

        if do_discard:
            return None

        return [
            planner_pb2.IngredientAmount(ingredient=ingredient, amount=amount)
            for ingredient, amount in requires.items()
        ]

    def _get_recipes(self):
        # TODO(agentydragon): this will return an empty recipe if None was returned.
        return [
            planner_pb2.Recipe(
                item=self._get_recipe_ingredient_usage_per_portion(recipe),
                name=_sanitize_recipe_name(recipe.title))
            for recipe in self._recipes
        ]

    def _get_ingredients(self, plan_request) -> List[planner_pb2.Ingredient]:
        # Convert stock to universal units.
        sk = []
        for ingredient_id in self._ingredient_ids:
            amount = self._stock.find_amount_in_stock(ingredient_id)
            if not amount:
                sk.append(0)
                continue

            if amount.HasField('exact'):
                amt = amount.exact
            if amount.HasField('lower_bound'):
                amt = amount.lower_bound
            try:
                sk.append(self._ingredient_amount_translators[ingredient_id].
                          to_universal_unit(amt))
            except KeyError as e:
                # TODO(agentydragon): figure out how this can happen and fix it
                self._error_reporter.report(
                    f"not including stock of unknown ingredient {ingredient_id}: {e}"
                )
                sk.append(0)
                continue

        ingredients = []
        for i, ingredient_id in enumerate(self._ingredient_ids):
            ingredient = planner_pb2.Ingredient(
                name=_sanitize_ingredient_name(ingredient_id),
                stock=sk[i],
                ingredient_bits=[
                    int(diet_bit) for diet_bit in _get_ingredient_diet_bits(
                        ingredient_id, self._diet_map)
                ])
            # Only emit subtype information if quanta match.
            if self._force_same_quantum_for_connected_components:
                ingredient.subtypes.extend([
                    self._ingredient_ids.index(subtype_id)
                    for subtype_id in self._ingredient_database.
                    get_subtypes_recursive(ingredient_id)
                    if subtype_id in self._ingredient_ids
                ])
            ingredient.banned_external = (
                ingredient_id in plan_request.banned_external_ingredient_id)
            # TODO: required_ingredient_id is incompatible with "has enough"
            ingredient.is_required = (ingredient_id
                                      in plan_request.required_ingredient_id)
            ingredients.append(ingredient)
        return ingredients

    def _get_product(self,
                     product: inventory_pb2.Product) -> planner_pb2.Product:
        ingredient_id = product.contents.ingredient_id
        amount = self._ingredient_amount_translators[
            ingredient_id].to_universal_unit(_get_product_unit_amount(product))
        planner_product = planner_pb2.Product(
            content=[
                planner_pb2.IngredientAmount(
                    ingredient=self._get_ingredient_index(ingredient_id),
                    amount=amount)
            ],
            name=f"LeShop_{product.merchant_product_id}")
        if self._merchant_product_statuses:
            # find product in merchant status blob
            for prodstatus in self._merchant_product_statuses:
                if prodstatus.product_id == str(product.merchant_product_id):
                    break
            else:
                self._error_reporter.report(
                    f"no status known for product {product.merchant_product_id}, assuming it can be bought unlimited"
                )
                planner_product.unlimited.SetInParent()
                return planner_product

            if (datetime.datetime.now() - time_util.timestamp_proto_to_datetime(
                    prodstatus.timestamp)).total_seconds() > 60 * 60 * 24:
                logging.info("status is >1 day old (%s), ignoring", prodstatus)
                planner_product.unlimited.SetInParent()
                return planner_product

            if prodstatus.HasField('not_available'):
                planner_product.max_buyable = 0
            elif prodstatus.HasField('available_exact'):
                planner_product.max_buyable = prodstatus.available_exact
            elif prodstatus.HasField('available_lower_bound'):
                # TODO(agentydragon): set to "unlimited" instead maybe?
                planner_product.max_buyable = prodstatus.available_lower_bound
            else:
                raise Exception("unexpected")
        else:
            planner_product.unlimited.SetInParent()

        return planner_product

    def _get_products(self) -> List[planner_pb2.Product]:
        result = []
        for product in self._products:
            result.append(self._get_product(product))
        return result

    def _get_ingredient_index(self, ingredient_id):
        return self._ingredient_ids.index(ingredient_id)

    def _translate_ingredient_amounts(
            self, amounts: List[int]) -> Dict[str, inventory_pb2.UnitAmount]:
        return {
            ingredient_id: self._ingredient_amount_translators[ingredient_id].
            universal_unit_to_unit_amount(amount)
            for ingredient_index, (
                ingredient_id,
                amount) in enumerate(zip(self._ingredient_ids, amounts))
            if amount > 0
        }

    def _translate_product_amounts(
            self, product_amounts: List[int]
    ) -> List[Tuple[int, inventory_pb2.Product]]:
        return [(amount, product)
                for index, (
                    amount,
                    product) in enumerate(zip(product_amounts, self._products))
                if amount > 0]

    def _get_diet_bits(self):
        return [
            planner_pb2.DietBitInput()
            for value in diet_mapping_pb2.DietBit.values()
        ]

    def translate_to_plan_input(self, plan_request: inventory_pb2.PlanRequest,
                                history: inventory_pb2.History):
        # TODO: add test for:
        #   - banning recipes cooked after a threshold
        #   - if there's no threshold set, no recipe will be banned
        banned_recipe_ids = set()
        if plan_request.no_repeats_cutoff_date.year:
            cutoff_date = time_util.date_proto_to_date(
                plan_request.no_repeats_cutoff_date)
            for meal in history.past_cooked_meal:
                # If this meal was cooked before cutoff, do not consider it "already cooked".
                if time_util.date_proto_to_date(meal.date) >= cutoff_date:
                    banned_recipe_ids.add(meal.recipe_id)
        return planner_pb2.Input(
            recipe=self._get_recipes(),
            product=self._get_products(),
            ingredient=self._get_ingredients(plan_request),
            diet_bit=self._get_diet_bits(),
            max_ingredients_obtained_externally=plan_request.
            max_added_ingredients,
            meal_request=[
                planner_pb2.MealRequest(
                    portion_count=meal_request.portions,
                    blocked_diet_bits=[
                        int(diet_bit)
                        for diet_bit in meal_request.disallowed_diet_bits
                    ])
                for meal_request in plan_request.meal_request
            ],
            max_of_recipes=[
                planner_pb2.MaxOfRecipes(recipe=[
                    i
                    for i, recipe in enumerate(self._recipes)
                    if max_recipe_type.name_part.lower() in
                    recipe.title.lower()
                ],
                                         max_meals=max_recipe_type.max_amount)
                for max_recipe_type in plan_request.max_recipe_type
            ],
            # disallow cooking anything that was already in history
            banned_recipe=[
                i for i, recipe in enumerate(self._recipes)
                if recipe.id in banned_recipe_ids
            ],
            timeout_seconds=plan_request.timeout_seconds)

    def translate_plan(
            self, plan: planner_pb2.Plan,
            plan_request: inventory_pb2.PlanRequest) -> inventory_pb2.Plan:
        planned_recipes = []
        for plan_meal, meal_request in zip(plan.meal,
                                           plan_request.meal_request):
            commitments = []

            recipe = self._recipes[plan_meal.recipe]
            ingredient_amounts = self._translate_ingredient_amounts(
                plan_meal.ingredient_use)

            for ingredient_id, amount in ingredient_amounts.items():
                commitments.append(
                    inventory_pb2.Commitment(
                        ingredient_id=ingredient_id,
                        amount=inventory_pb2.RecipeAmount(unit_amount=amount)))

            for recipe_line in recipe.recipe_proto.ingredients:
                ingredient_id = recipe_line.ingredient_id
                if ingredient_id in set(ingredient_amounts.keys()):
                    continue
                amount = recipe_line.amount
                if amount.HasField('unit_amount'):
                    # TODO(agentydragon): this is not managed because we probably
                    # have enough stock, but we should actually include it!
                    continue
                commitments.append(
                    inventory_pb2.Commitment(ingredient_id=ingredient_id,
                                             amount=amount))

            planned_recipes.append(
                inventory_pb2.PlannedRecipe(date=meal_request.date,
                                            recipe_id=recipe.id,
                                            commitment=commitments,
                                            portions=plan_meal.portions))

        out_of_system = self._translate_ingredient_amounts([
            plan_ingredient.obtain_externally
            for plan_ingredient in plan.ingredient
        ])

        totals = self._translate_ingredient_amounts(
            [plan_ingredient.total_use for plan_ingredient in plan.ingredient])

        # TODO: compute cost

        # TODO(agentydragon): figure out:
        #  - how much of the product do we need to buy (and when)

        purchases = []
        for amount, product in self._translate_product_amounts(plan.products):
            purchases.append(
                inventory_pb2.Purchase(
                    merchant_product_id=product.merchant_product_id,
                    amount=amount))
        plan = inventory_pb2.Plan(
            planned_recipe=planned_recipes,
            purchase=purchases,
            obtain_externally=[
                inventory_pb2.ObtainExternally(ingredient_id=ingredient_id,
                                               amount=amount)
                for ingredient_id, amount in out_of_system.items()
            ],
            # TODO(agentydragon): Currently ingredients that are not mapped within
            # the translator (such as salt) are not included. But they should be!
            total_ingredient_use=[
                inventory_pb2.PlanIngredientUse(
                    ingredient_id=ingredient_id,
                    amount=inventory_pb2.RecipeAmount(unit_amount=amount))
                for ingredient_id, amount in totals.items()
                if amount.amount > 0
            ])
        if purchases:
            plan.purchase_delivery_date.MergeFrom(
                plan_request.meal_request[0].date)
        return plan
