"""
bazel run //db:import_stock -- \
  --alsologtostderr \
  --sqlite_db=$(pwd)/db.sqlite
"""

import datetime
import sqlite3

from absl import app, flags, logging
from google.protobuf import text_format
from google.protobuf.timestamp_pb2 import Timestamp
from meal_scheduler.db import db as db_lib
from meal_scheduler.db import schema
from meal_scheduler.inventory_py_proto_pb import inventory_pb2
from rules_python.python.runfiles import runfiles

flags.DEFINE_string("sqlite_db", None, "path to sqlite db")

FLAGS = flags.FLAGS


def init_db(db):
    schema.METADATA.create_all(db.engine)


def main(_):
    db = db_lib.open_db(FLAGS.sqlite_db)
    init_db(db)


if __name__ == '__main__':
    app.run(main)
