from typing import List

from meal_scheduler import unit as unit_lib
from meal_scheduler import unit_amount as unit_amount_lib
from meal_scheduler.inventory_py_proto_pb import inventory_pb2


# TODO(agentydragon): this assumes there's only 1 product for any
# ingredient
def find_direct_products_for_ingredient(ingredient_id,
                                        products: List[inventory_pb2.Product]):
    result = []
    assert isinstance(products, list)
    for product in products:
        if product.contents.ingredient_id == ingredient_id:
            result.append(product)
    return result


def find_indirect_products_for_ingredient(
        ingredient_db,  # IngredientDatabase
        ingredient_id,
        products: List[inventory_pb2.Product]):
    supertypes = ingredient_db.get_supertypes(ingredient_id)
    result = []
    assert isinstance(products, list)
    for product in products:
        if product.contents.ingredient_id in supertypes:
            result.append(product)
    return result


def find_leshop_product_by_id(merchant_product_id,
                              products: List[inventory_pb2.Product]):
    assert isinstance(products, list)
    for product in products:
        if product.merchant_product_id == merchant_product_id:
            return product
    raise KeyError("no such product")


def find_by_merchant_merchant_product_id(products: List[inventory_pb2.Product],
                                         merchant, merchant_product_id):
    for product in products:
        if (product.merchant,
                product.merchant_product_id) == (merchant, merchant_product_id):
            return product
    raise KeyError("no such product")


class NoProductFoundException(Exception):
    pass


def find_unit_amount_in_product(product, ingredient_id):
    if product.contents.ingredient_id != ingredient_id:
        return None
    if product.contents.amount.HasField('amount_exact'):
        return product.contents.amount.amount_exact
    elif product.contents.amount.HasField('amount_range'):
        # TODO
        return (product.contents.amount.amount_range.min +
                product.contents.amount.amount_range.max) / 2
    raise Exception("uhh...")


def leshop_link(product_id):
    return f'https://www.leshop.ch/en/direct/product/{product_id}'


def product_unit_amount_to_str(unit_amount):
    if unit_amount.HasField('amount_exact'):
        return unit_amount_lib.unit_amount_to_str_bare(
            unit_amount.unit, unit_amount.amount_exact) or str(unit_amount)
    if unit_amount.HasField('amount_range'):
        return f'{unit_amount.amount_range.min: .3g} - {unit_amount.amount_range.max: .3g} {unit_amount_lib.UNIT_NAMES[unit_amount.unit]}'
    return str(unit_amount)


def get_ingredient_ids_with_products(products: List[inventory_pb2.Product]):
    assert isinstance(products, list)
    return {product.contents.ingredient_id for product in products}
