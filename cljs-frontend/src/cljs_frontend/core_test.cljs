(ns cljs-frontend.core-test
  (:require [cljs.test :refer [deftest is testing run-tests]]
            [cljs-frontend.core :refer
             [json-to-state ingredient-state->json matches-filter?]]))

(deftest json-to-state-test
  (testing
    "json-to-state test"
    (is
      (= {"/ingredient/salt"
            {:enough true, :amount 0, :unit 2, :comment "", :timestamp nil}}
         (json-to-state {(keyword "/ingredient/salt") {:amount "enough"}} [])))
    (is (= {"/ingredient/salt"
              {:enough true, :amount 0, :unit 2, :comment "", :timestamp nil},
            "/ingredient/pepper"
              {:enough false, :amount 0, :unit 2, :comment "", :timestamp nil}}
           (json-to-state {(keyword "/ingredient/salt") {:amount "enough"}}
                          ["/ingredient/pepper"])))))

(deftest ingredient-state->json-test
  (testing "ingredient-state->json"
           (is (= (ingredient-state->json
                    "/ingredient/salt"
                    {:enough true, :amount 0, :unit 2, :comment "Hello"})
                  {:ingredient_id "/ingredient/salt",
                   :amount "enough",
                   :unit 2,
                   :comment "Hello"}))))

(deftest matches-filter?-test
  (testing
    "matches-filter?"
    (testing
      "empty filter"
      ; has enough -> matches
      (is (matches-filter? "/ingredient/salt"
                           {:enough true, :amount 0, :unit 2, :comment ""}
                           ""))
      ; no amount, but has comment -> matches
      (is (matches-filter? "/ingredient/salt"
                           {:enough false, :amount 0, :unit 2, :comment "hello"}
                           ""))
      ; some amount -> matches
      (is (matches-filter? "/ingredient/salt"
                           {:enough false, :amount 150, :unit 2, :comment ""}
                           ""))
      ; no amount, but dirty -> matches
      (is (matches-filter?
            "/ingredient/salt"
            {:enough false, :amount 0, :unit 2, :comment "", :dirty true}
            ""))
      ; not enough, no amount, no comment -> does not match
      (is (not (matches-filter? "/ingredient/salt"
                                {:enough false, :amount 0, :unit 2, :comment ""}
                                ""))))
    (testing
      "non-empty filter"
      ; filter "salt" matches "salt" that has finite nonzero amount
      (is (matches-filter? "/ingredient/salt"
                           {:enough false, :amount 100, :unit 2, :comment ""}
                           "salt"))
      ; filter "salt" matches "salt" that has zero amount, no comment
      (is (matches-filter? "/ingredient/salt"
                           {:enough false, :amount 0, :unit 2, :comment ""}
                           "salt"))
      ; filter "salt" does not match pepper
      (is (not (matches-filter? "/ingredient/pepper"
                                {:enough false, :amount 0, :unit 2, :comment ""}
                                "salt"))))))
