import os
import os.path

from absl import app, flags, logging
from google.protobuf import text_format
from meal_scheduler.inventory_py_proto_pb import inventory_pb2

flags.DEFINE_string("recipe_import_out", None, "output import file")

FLAGS = flags.FLAGS


def main(args):
    recipes = []
    paths = args[1:]
    unique_basenames = {os.path.basename(path) for path in paths}
    assert len(unique_basenames) == len(paths), "non-unique basenames"
    for path in paths:
        logging.info("Appending: %s", path)
        with open(path, 'rb') as f:
            recipes.append(
                inventory_pb2.ImportedRecipe(importer_id=os.path.basename(path),
                                             recipe=text_format.Parse(
                                                 f.read(),
                                                 inventory_pb2.Recipe())))
    with open(FLAGS.recipe_import_out, 'w') as f:
        f.write(
            text_format.MessageToString(
                inventory_pb2.RecipeImport(importer='meal-scheduler-fixture',
                                           imported_recipe=recipes)))


if __name__ == '__main__':
    flags.mark_flags_as_required(['recipe_import_out'])
    app.run(main)
