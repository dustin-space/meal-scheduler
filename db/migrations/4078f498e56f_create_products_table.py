from alembic import op
from sqlalchemy import Column, Integer, LargeBinary, String, UniqueConstraint

revision = '4078f498e56f'
down_revision = '9bd60f0bb17d'
branch_labels = None
depends_on = None


def upgrade():
    # TODO(agentydragon): Also add product ID, globally unique
    op.create_table('Product', Column('merchant', String, nullable=False),
                    Column('merchant_product_id', String, nullable=False),
                    Column('content', LargeBinary, nullable=False),
                    Column('comment', String))


def downgrade():
    op.drop_table('Product')
