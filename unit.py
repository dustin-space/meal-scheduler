from meal_scheduler.unit_py_proto_pb import unit_pb2

UNIT_TO_NAME_DICT = {value: name for name, value in unit_pb2.Unit.items()}


def unit_name(unit):
    return UNIT_TO_NAME_DICT[unit]
