from absl import logging
from absl.testing import absltest
from google.protobuf import text_format
from meal_scheduler.data import recipes as recipes_lib
from meal_scheduler.data import stock as stock_lib
from meal_scheduler.inventory_py_proto_pb import inventory_pb2
from meal_scheduler.stickers import generate


class StickersTest(absltest.TestCase):

    def test_cups_ppd_parsing(self):
        assert generate.A4_IMAGEABLE_AREA_REGEX.fullmatch(
            '*ImageableArea A4: "12.245669291339 12.245669291339 583.029921259842 829.644094488189"'
        )
        assert generate.A4_PAPER_DIMENSION_REGEX.fullmatch(
            '*PaperDimension A4: "595.275590551181 841.889763779528"')

    def test_render_simple_stickers(self):
        plan = text_format.Parse(
            r"""
          planned_recipe {
            recipe_id: "raw-egg"
            portions: 2
            date { year: 2020 month: 1 day: 2 }
            commitment {
              ingredient_id: "/ingredient/egg"
              amount {
                unit_amount { unit: PIECE amount: 2 }
              }
            }
          }""", inventory_pb2.Plan())
        recipe = text_format.Parse(
            r"""
            id: "raw-egg"
            recipe { title: "Raw egg recipe" }
            """, inventory_pb2.StoredRecipe())
        stock = stock_lib.StockDatabase([])
        result = generate.generate_stickers(plan, stock,
                                            [recipes_lib.RecipeWrapper(recipe)])
        self.assertEquals(len(result), 1)
        result = result[0]
        self.assertIn("2 piece", result)
        self.assertIn("/egg", result)
        self.assertIn("2020-01-02", result)
        self.assertIn("Raw egg recipe", result)


if __name__ == '__main__':
    absltest.main()
