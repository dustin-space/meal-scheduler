from google.protobuf import text_format
from meal_scheduler.db import db as db_lib
from meal_scheduler.inventory_py_proto_pb import inventory_pb2


def import_ingredients(db, ingredients):
    ingredient_database = db_lib.load_ingredient_types(db)
    ingredient_ids = ingredient_database.ingredient_ids

    for ingredient_type in ingredients:
        if ingredient_type.id not in ingredient_ids:
            db_lib.insert_ingredient_type(db, ingredient_type)
        else:
            db_lib.update_ingredient_type(db, ingredient_type)


def import_ingredient_types_from_file(db, path):
    with open(path, 'rb') as f:
        proto = text_format.Parse(f.read(), inventory_pb2.IngredientTypes())

    ingredient_database = db_lib.load_ingredient_types(db)
    ingredient_ids = ingredient_database.ingredient_ids

    for ingredient_type in proto.ingredient:
        if ingredient_type.id not in ingredient_ids:
            db_lib.insert_ingredient_type(db, ingredient_type)
        else:
            db_lib.update_ingredient_type(db, ingredient_type)

    for rule in proto.ingredient_matter_rule:
        db_lib.insert_ingredient_matter_rule(db, rule)


def import_ingredient_names_from_file(db, path):
    with open(path, 'rb') as f:
        proto = text_format.Parse(f.read(), inventory_pb2.IngredientNames())
    for name in proto.name:
        db_lib.insert_ingredient_name(db, name)
