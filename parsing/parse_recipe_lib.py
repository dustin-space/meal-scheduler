import collections
import os
import os.path
import re

from absl import logging
from google.protobuf import text_encoding, text_format
from meal_scheduler import error_reporter as error_reporter_lib
from meal_scheduler.check import check_lib
from meal_scheduler.data import ingredient_types
from meal_scheduler.data import recipes as recipes_lib
from meal_scheduler.inventory_py_proto_pb import inventory_pb2
from meal_scheduler.parsing import parse_util
from meal_scheduler.unit_py_proto_pb import unit_pb2
from rules_python.python.runfiles import runfiles

IGNORED_THINGS = {
    'kann Spuren von Allergenen enthalten',
    '(kann Spuren von Allergenen enthalten)',
    'Beinhaltet',
    '(BeinhaltetGluten, Soja)',
    'Gluten, Soja',
}


class UnknownIngredientsException(Exception):

    def __init__(self, names):
        self.names = names


class UnknownUnitException(Exception):

    def __init__(self, name):
        self.name = name


UNIT_NAME_DICT = {
    'g': unit_pb2.GRAM,
    'ml': unit_pb2.MILLILITER,
    'dl': unit_pb2.DECILITER,
    'slice': unit_pb2.SLICE,
    'Stück': unit_pb2.PIECE,
    'Einheit': unit_pb2.PIECE,
    'Bund': unit_pb2.BUNCH,
    'Teelöffel': unit_pb2.TEASPOON,
    'Esslöffel': unit_pb2.TABLESPOON,
    'Dose': unit_pb2.DOSE,
    'pinch': unit_pb2.PINCH,
    'Packung': unit_pb2.PACKAGE,
    'Zehe': unit_pb2.CLOVE,
    'Zweig': unit_pb2.STICK,
    'Becher': unit_pb2.CUP_PACKAGE,
    'bag(s)': unit_pb2.BAG_PACKAGE,
    # TODO(agentydragon): aaa this is bad should use
    # inventory_pb2.Unit.TABLESPOON et al.

    # for onion
    'stalk': unit_pb2.PIECE,
    # for honey
    'piece': unit_pb2.PIECE,
}


def parse_unit(unit: str) -> unit_pb2.Unit:
    if unit in UNIT_NAME_DICT:
        return UNIT_NAME_DICT[unit]
    raise UnknownUnitException(unit)


# TODO(agentydragon): add tests


def get_garlic_clove_amount(amount, unit):
    if unit == '' and amount == '':
        return inventory_pb2.RecipeAmount(unknown=inventory_pb2.UnknownAmount())
    if parse_util.can_parse_number(amount):
        if (unit == '') or (unit in UNIT_NAME_DICT and UNIT_NAME_DICT[unit]
                            in (unit_pb2.PIECE, unit_pb2.CLOVE)):
            return inventory_pb2.RecipeAmount(
                unit_amount=inventory_pb2.UnitAmount(
                    amount=parse_util.parse_number(
                        amount), unit=unit_pb2.CLOVE))
        elif unit in UNIT_NAME_DICT and UNIT_NAME_DICT[unit] == unit_pb2.GRAM:
            return inventory_pb2.RecipeAmount(
                unit_amount=inventory_pb2.UnitAmount(
                    amount=parse_util.parse_number(amount),
                    unit=UNIT_NAME_DICT[unit]))
    if unit == 'ml':
        return inventory_pb2.RecipeAmount(unknown=inventory_pb2.UnknownAmount())
    raise Exception(f"garlic clove: {amount=} {unit=}")


HAVE_AT_HOME = {
    "/ingredient/oil/generic",
    "/ingredient/oil/olive",
    "/ingredient/spice/pepper/generic",
    "/ingredient/spice/salt",
    "/ingredient/sugar",
    "/ingredient/vinegar/generic",
}


def parse_ingredient(amount, unit, ingredient_name, ingredient_name_to_id):
    if ingredient_name == 'Knoblauchzehe':
        return inventory_pb2.RecipeLine(
            ingredient_id='/ingredient/vegetable/garlic',
            amount=get_garlic_clove_amount(amount, unit))
    elif ingredient_name not in ingredient_name_to_id:
        raise UnknownIngredientsException({ingredient_name})

    ingredient_id = ingredient_name_to_id[ingredient_name]

    if re.match(r'/ingredient/spice/.*', ingredient_id) and unit in ('Stück'):
        return inventory_pb2.RecipeLine(
            ingredient_id=ingredient_id,
            amount=inventory_pb2.RecipeAmount(
                unknown=inventory_pb2.UnknownAmount()))
    elif (ingredient_name_to_id[ingredient_name]
          in HAVE_AT_HOME) and unit in ('Einheit', '', 'Stück'):
        return inventory_pb2.RecipeLine(
            ingredient_id=ingredient_id,
            amount=inventory_pb2.RecipeAmount(
                unknown=inventory_pb2.UnknownAmount()))
    elif unit in UNIT_NAME_DICT:
        if parse_util.can_parse_number(amount):
            amount = inventory_pb2.RecipeAmount(
                unit_amount=inventory_pb2.UnitAmount(amount=float(amount),
                                                     unit=UNIT_NAME_DICT[unit]))
        elif amount == '':
            amount = inventory_pb2.RecipeAmount(
                unit_amount=inventory_pb2.UnitAmount(amount=1,
                                                     unit=UNIT_NAME_DICT[unit]))
    elif unit == "nach Geschmack":
        amount = inventory_pb2.RecipeAmount(to_taste=inventory_pb2.ToTaste())
    elif unit in ('Einheit', '', 'ml') and amount == '':
        amount = inventory_pb2.RecipeAmount(
            unknown=inventory_pb2.UnknownAmount())
    elif unit == '' and re.match(r'^[0-9]+(.[0-9]+)?$', amount):
        # TODO(agentydragon): only if it's whole pieces, and it comes in pieces
        amount = inventory_pb2.RecipeAmount(
            unit_amount=inventory_pb2.UnitAmount(
                amount=parse_util.parse_number(amount), unit=unit_pb2.PIECE))
    else:
        raise Exception(f"{amount} - {unit} for {ingredient_name}")
    return inventory_pb2.RecipeLine(
        ingredient_id=ingredient_name_to_id[ingredient_name], amount=amount)


def parse_plaintext_fields(
        ingredient_database: ingredient_types.IngredientDatabase,
        plaintext_fields: inventory_pb2.PlaintextRecipeFields,
        ingredient_name_to_id,
        used_ingredient_name_map) -> inventory_pb2.Recipe:
    ingredients = []
    unknown_ingredients = set()

    for plaintext_ingredient in plaintext_fields.plaintext_ingredients:
        amount = plaintext_ingredient.amount_plaintext
        unit = plaintext_ingredient.unit_plaintext
        # remove star at end, marks "you should have this at home"
        ingredient_name = plaintext_ingredient.ingredient_plaintext.rstrip("*")
        if ingredient_name == '':
            raise Exception("Empty ingredient: " + path)
        logging.info(f"{amount=} {unit=} {ingredient_name=}")

        used_ingredient_name_map[ingredient_name] += 1

        try:
            ingredient = parse_ingredient(amount, unit, ingredient_name,
                                          ingredient_name_to_id)
        except UnknownIngredientsException as e:
            unknown_ingredients.update(e.names)
        else:
            ingredients.append(ingredient)

    if unknown_ingredients:
        raise UnknownIngredientsException(unknown_ingredients)

    recipe = inventory_pb2.Recipe(title=plaintext_fields.title,
                                  source=plaintext_fields.source,
                                  ingredients=ingredients,
                                  instructions=plaintext_fields.instructions,
                                  serves=inventory_pb2.ServingAmountRange(
                                      min=plaintext_fields.yields,
                                      max=plaintext_fields.yields))
    error_reporter = error_reporter_lib.ErrorReporter()
    check_lib.check_recipe(ingredient_database, recipe, error_reporter)
    if not error_reporter.ok():
        raise Exception("... errors ...")
    return recipe


def parse_recipe_from_file(path: str, ingredient_name_to_id,
                           ingredient_database,
                           used_ingredient_name_map) -> inventory_pb2.Recipe:
    with open(path, 'rb') as f:
        plaintext_fields = text_format.Parse(
            f.read(), inventory_pb2.PlaintextRecipeFields())
    return parse_plaintext_fields(ingredient_database, plaintext_fields,
                                  ingredient_name_to_id,
                                  used_ingredient_name_map)
