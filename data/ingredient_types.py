from absl import logging
from google.protobuf import text_format
from meal_scheduler.inventory_py_proto_pb import inventory_pb2
from typing import List
import re

TYPES_RUNFILES_PATH = 'meal_scheduler/data/ingredient_types.textproto'
NAMES_RUNFILES_PATH = 'meal_scheduler/data/ingredient_names.textproto'


class IngredientDatabase:

    def __init__(
            self, ingredient_types: List[inventory_pb2.IngredientType],
            ingredient_matter_rules: List[inventory_pb2.IngredientMatterRule],
            ingredient_names: List[inventory_pb2.IngredientName]):
        assert isinstance(ingredient_types, list)

        self._ingredient_types = ingredient_types
        self._ingredient_matter_rules = ingredient_matter_rules
        self._ingredient_names = ingredient_names

    @property
    def ingredient(self):
        return self._ingredient_types

    @property
    def ingredient_ids(self):
        return set(ingredient.id for ingredient in self._ingredient_types)

    @property
    def names(self):
        return self._ingredient_names

    @property
    def ingredient_matter_rules(self):
        return self._ingredient_matter_rules

    @property
    def name_to_id(self):
        return {
            name_string: name_proto.ingredient_id
            for name_proto in self._ingredient_names
            for name_string in name_proto.name
        }

    def find_ingredient_by_id(self, ingredient_id):
        for ingredient in self._ingredient_types:
            if ingredient.id == ingredient_id:
                return ingredient
        raise KeyError(f"no such ingredient: {ingredient_id}")

    def get_matter_type(self, ingredient_id):
        matter_type = inventory_pb2.UNKNOWN_MATTER_TYPE
        for rule in self._ingredient_matter_rules:
            if re.fullmatch(rule.ingredient_id_regex, ingredient_id):
                matter_type = rule.matter_type
        return matter_type

    def get_supertypes(self, ingredient_id):
        """Returns supertypes of the ingredient, not including the ingredient itself."""
        # TODO(agentydragon): optimize
        supertypes = []
        for ingredient in self._ingredient_types:
            if ingredient_id in self.get_subtypes_recursive(ingredient.id):
                supertypes.append(ingredient.id)
        return supertypes

    def get_subtypes_recursive(self, ingredient_id):
        """Returns the subtypes of the ingredient, not including the ingredient
        itself."""
        recursive_subtypes = set()
        for subtype in self.find_ingredient_by_id(ingredient_id).subtypes:
            recursive_subtypes.add(subtype)
            recursive_subtypes = recursive_subtypes.union(
                self.get_subtypes_recursive(subtype))
        return recursive_subtypes
