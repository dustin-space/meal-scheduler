"""
bazel run //parsing:parse_recipe -- \
  --alsologtostderr \
  --importer=manual-import \
  --sqlite_db=$(pwd)/db.sqlite \
  ${HOME}/*.plaintext_recipe.textproto
"""

import collections
import os
import os.path

from absl import app, flags, logging
from google.protobuf import text_encoding, text_format
from meal_scheduler.db import db as db_lib
from meal_scheduler.inventory_py_proto_pb import inventory_pb2
from meal_scheduler.parsing import parse_recipe_lib, parse_util
from rules_python.python.runfiles import runfiles

_RECIPE_IMPORT_OUT = flags.DEFINE_string("recipe_import_out", None,
                                         "recipe import out")
_IMPORTER = flags.DEFINE_string("importer", None, "importer system")
_SQLITE_DB = flags.DEFINE_string("sqlite_db", None, "sqlite db")

FLAGS = flags.FLAGS


def main(argv):
    conn = db_lib.open_db(_SQLITE_DB.value)

    ingredient_database = db_lib.load_ingredient_types(conn)
    ingredient_name_to_id = ingredient_database.name_to_id

    unknown_units = set()

    used_ingredient_name_map = collections.Counter()

    imported_recipe = []
    # TODO(agentydragon): accept directories, globs

    error_reasons = collections.Counter()

    for path in argv[1:]:
        logging.info("Processing: %s", path)

        try:
            recipe = parse_recipe_lib.parse_recipe_from_file(
                path, ingredient_name_to_id, ingredient_database,
                used_ingredient_name_map)
        except parse_recipe_lib.UnknownIngredientsException as e:
            logging.info(f"Unknown ingredients: {e.names}")
            error_reasons['unknown_ingredients'] += 1
            error_reasons[f'unknown_ingredients_{len(e.names)}'] += 1
            continue
        except parse_recipe_lib.UnknownUnitException as e:
            logging.exception("Unknown unit: %s in %s", e.name, path)
            unknown_units.add(e.name)
            error_reasons['unknown_unit'] += 1
            continue
        except parse_util.NumberParsingException as e:
            logging.exception("Error parsing a number in %s", path)
            error_reasons['number_parsing'] += 1
            continue

        assert recipe.source.url

        imported_recipe.append(
            inventory_pb2.ImportedRecipe(importer_id=recipe.source.url,
                                         recipe=recipe))

    logging.info("successfully created imports for %d of %d inputs",
                 len(imported_recipe), len(argv[1:]))
    logging.info("%s", error_reasons)
    logging.info("unknown units: %s", unknown_units)
    logging.info("Used names:")
    for name, count in reversed(used_ingredient_name_map.most_common()):
        # TODO(agentydragon): this will not work for "knoblauchzehe"...
        logging.info("%-50s %03d %-40s %s",
                     ingredient_name_to_id.get(name, '?'), count, name,
                     text_encoding.CEscape(name.encode('utf-8'), as_utf8=False))

    with open(_RECIPE_IMPORT_OUT.value, 'w') as f:
        f.write(
            text_format.MessageToString(
                inventory_pb2.RecipeImport(importer=_IMPORTER.value,
                                           imported_recipe=imported_recipe)))


if __name__ == '__main__':
    flags.mark_flags_as_required(
        [_IMPORTER.name, _RECIPE_IMPORT_OUT.name, _SQLITE_DB.name])
    app.run(main)
