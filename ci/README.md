This directory builds the Docker image used for CI for this project.
Unfortunately Bazel only supports `amd64` right now, so this image won't work
on any other platform.

## Building

```bash
docker build -t registry.gitlab.com/dustin-space/meal-scheduler/cirunner .
```

## Pushing

Get personal access token from: https://gitlab.com/-/profile/personal_access_tokens

```bash
docker login registry.gitlab.com -u agentydragon -p <token>
```

```bash
docker push registry.gitlab.com/dustin-space/meal-scheduler/cirunner
```

