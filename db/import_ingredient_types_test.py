from absl.testing import absltest
from meal_scheduler.db import db as db_lib
from meal_scheduler.db import import_ingredient_types_lib
from meal_scheduler.db import init as init_lib
from meal_scheduler.inventory_py_proto_pb import inventory_pb2


class ImportIngredientsTest(absltest.TestCase):

    def setUp(self):
        self._db = db_lib.open_db(":memory:")
        init_lib.init_db(self._db)

    def tearDown(self):
        self._db.close()

    def test_import_single(self):
        ingredient = inventory_pb2.IngredientType(id="/ingredient/a",
                                                  comment=["before"])
        import_ingredient_types_lib.import_ingredients(self._db, [ingredient])

        ingredient_database = db_lib.load_ingredient_types(self._db)
        i = ingredient_database.ingredient
        self.assertEqual(len(i), 1)
        self.assertEqual(i[0].comment, ["before"])
        self.assertEqual(i[0].id, "/ingredient/a")

    def test_overwrite_single(self):
        ingredient = inventory_pb2.IngredientType(id="/ingredient/a",
                                                  comment=["before"])
        import_ingredient_types_lib.import_ingredients(self._db, [ingredient])
        ingredient = inventory_pb2.IngredientType(id="/ingredient/a",
                                                  comment=["after"])
        import_ingredient_types_lib.import_ingredients(self._db, [ingredient])

        ingredient_database = db_lib.load_ingredient_types(self._db)
        i = ingredient_database.ingredient
        self.assertEqual(len(i), 1)
        self.assertEqual(i[0].comment, ["after"])
        self.assertEqual(i[0].id, "/ingredient/a")


if __name__ == '__main__':
    absltest.main()
