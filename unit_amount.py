from meal_scheduler import unit as unit_lib
from meal_scheduler.unit_py_proto_pb import unit_pb2
from google.protobuf import text_format

UNIT_NAMES = {
    unit_pb2.GRAM: 'g',
    unit_pb2.MILLILITER: 'ml',
    unit_pb2.PIECE: 'piece',
    unit_pb2.TABLESPOON: 'tablespoon',
    unit_pb2.TEASPOON: 'teaspoon',
    unit_pb2.CLOVE: 'clove',
}


def unit_amount_to_str_bare(unit, amount):
    if unit in UNIT_NAMES:
        return f'{amount:.3g} {UNIT_NAMES[unit]}'


def unit_amount_to_str(unit_amount):
    return (unit_amount_to_str_bare(unit_amount.unit, unit_amount.amount) or
            text_format.MessageToString(unit_amount, as_one_line=True))
