"""
Run with:

bazel run //bundle -- \
  --alsologtostderr \
  --sqlite_db=$(pwd)/db.sqlite \
  --config_yaml=$(pwd)/config.yaml
"""

import yaml
import importlib
from absl import app, flags, logging
from meal_scheduler.frontend import celery_app
from meal_scheduler.frontend import frontend
from meal_scheduler.frontend import sequential_hook

_SQLITE_DB = flags.DEFINE_string("sqlite_db", None, "path to sqlite db")
_CONFIG_YAML = flags.DEFINE_string("config_yaml", None, "path to yaml config")

_MODE_SERVER = 'server'
_MODE_WORKER = 'worker'
_MODE = flags.DEFINE_enum("mode", _MODE_SERVER, [_MODE_SERVER, _MODE_WORKER],
                          "mode to run")


def main(_):
    with open(_CONFIG_YAML.value) as f:
        config = yaml.load(f)

    celery_app.CELERY_APP.conf.update(
        broker_url=('sqla+sqlite:///' + _SQLITE_DB.value),
        result_backend=('db+sqlite:///' + _SQLITE_DB.value))

    hooks = sequential_hook.SequentialHook()
    for module_name in config['modules']:
        module = importlib.import_module(module_name)
        hooks.add_hook(module.HOOKS)

    # TODO(agentydragon): do this per module instead
    logging.info("Loading config: %s", config)
    hooks.apply_config(config)
    hooks.register_celery_tasks(celery_app.CELERY_APP)

    if _MODE.value == _MODE_SERVER:
        flask_app = frontend.create_app(database_path=_SQLITE_DB.value,
                                        hooks=hooks)
        flask_app.run(debug=True, use_reloader=False)
    elif _MODE.value == _MODE_WORKER:
        celery_app.CELERY_APP.start(
            ['worker', '--loglevel=INFO', '--pool=prefork'])
    else:
        raise Exception("bad mode")


if __name__ == '__main__':
    flags.mark_flags_as_required(
        (_SQLITE_DB.name, _CONFIG_YAML.name, _MODE.name))
    app.run(main)
