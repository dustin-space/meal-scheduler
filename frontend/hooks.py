"""Public interface to be implemented by each module."""

import abc
from typing import Optional, List, Tuple


class ModuleHooks(abc.ABC):

    def sample_hook(self, arg):
        pass

    def init_flask_app(self, flask):
        pass

    def register_celery_tasks(self, celery_app):
        pass

    def build_navigation_bar(self) -> Optional[List[Tuple[str, str, str]]]:
        """If module wants to render pages, return [('/url', 'page_name',
        'Title'), ...]"""
        pass

    def apply_config(self, config):
        """Config is loaded from YAML."""
        pass
