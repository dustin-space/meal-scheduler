from absl import logging
from absl.testing import absltest, parameterized
from meal_scheduler.plan import approximate_gcd


class ApproximateGcdTest(parameterized.TestCase):

    @parameterized.parameters(
        ([10, 20, 30], 0.001, 10), ([100, 40, 45], 0.001, 5),
        ([1000.0, 800.0, 17.5, 3.9, 3.9, 7.8, 25.0], 0.001, 0.0999),
        ([17.5, 3.90499, 3.90499, 7.80999, 25.0, 1000.0, 800.0], 0.1, 1.30166)
        # TODO(agentydragon): add testcase for: [32.46670894102727, 8.116677235256818, 12.5, 225.0]
        # ([
        #     1000.0, 50.0, 3.9049999713897705, 3.9049999713897705,
        #     7.809999942779541, 25.0
        # ], 0.1, 5),
    )
    def test_approximate_gcd(self, inputs, tolerance, expected_approximate_gcd):
        epsilon = 0.0001
        actual = approximate_gcd.approximate_gcd(inputs, tolerance=tolerance)
        self.assertTrue(
            abs(actual - expected_approximate_gcd) < epsilon,
            f"actual {actual} is not close to expected {expected_approximate_gcd}"
        )


if __name__ == '__main__':
    absltest.main()
