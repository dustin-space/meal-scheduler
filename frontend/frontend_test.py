import contextlib
import os.path

import hamcrest
import proto_matcher
from absl import flags, logging
from absl.testing import absltest
from google.protobuf import text_format
from meal_scheduler.db import db as db_lib
from meal_scheduler.db import init as init_lib
from meal_scheduler.frontend import frontend
from meal_scheduler.frontend import sequential_hook
from meal_scheduler.frontend import hooks as hooks_lib
from meal_scheduler.inventory_py_proto_pb import inventory_pb2

FLAGS = flags.FLAGS

_NOOP_HOOKS = sequential_hook.SequentialHook()


class FrontendTest(absltest.TestCase):

    def setUp(self):
        test_name = self.id().replace('__main__.FrontendTest.', '')
        self._database_path = os.path.join(FLAGS.test_tmpdir,
                                           test_name + '.sqlite')
        self._app = frontend.create_app(database_path=self._database_path,
                                        hooks=_NOOP_HOOKS)
        self._app.config['TESTING'] = True
        with self._app.app_context():
            init_lib.init_db(frontend.get_db())

    @contextlib.contextmanager
    def _db_context(self):
        with self._app.app_context():
            yield frontend.get_db()

    def test_shows_stock(self):
        """Tests that if there's an item in stock, it's shown on the stock index."""
        with self._db_context() as db:
            db_lib.insert_ingredient_type(
                db, inventory_pb2.IngredientType(id="/ingredient/spice/salt"))
            db_lib.update_stock(
                db,
                text_format.Parse(
                    r"""
                    ingredient_id: "/ingredient/spice/salt"
                    amount { exact { unit: GRAM amount: 100 } }
                    timestamp { seconds: 12345 }""", inventory_pb2.StockItem()))
        with self._app.test_client() as client:
            rv = client.get('/')
            assert b'/ingredient/spice/salt' in rv.data

    def test_stock_json(self):
        """Tests stock JSON get."""
        with self._db_context() as db:
            db_lib.insert_ingredient_type(
                db, inventory_pb2.IngredientType(id="/ingredient/spice/salt"))
            db_lib.update_stock(
                db,
                text_format.Parse(
                    r"""
                    ingredient_id: "/ingredient/spice/salt"
                    amount { exact { unit: GRAM amount: 100 } }
                    timestamp { seconds: 12345 }""", inventory_pb2.StockItem()))
        with self._app.test_client() as client:
            rv = client.get('/stock.json')
            assert b'/ingredient/spice/salt' in rv.data

    def test_stock_json_insert(self):
        """Tests stock update with JSON."""
        with self._db_context() as db:
            db_lib.insert_ingredient_type(
                db, inventory_pb2.IngredientType(id="/ingredient/spice/salt"))
        with self._app.test_client() as client:
            client.post(
                '/stock',
                json={
                    'ingredient_id': '/ingredient/spice/salt',
                    'amount': 150,
                    'unit': 2,  # GRAM
                    'comment': 'Hello'
                })
        with self._db_context() as db:
            stock = db_lib.load_stock(db)
        assert len(stock.stock) == 1
        hamcrest.assert_that(
            stock.stock[0],
            proto_matcher.partially(
                proto_matcher.equals_proto(r"""
                    ingredient_id: "/ingredient/spice/salt"
                    amount { exact { unit: GRAM amount: 150 } }
                    comment: "Hello"
                """)))

    def test_shows_recipes_index(self):
        """Tests that a recipe is shown on recipes index."""
        recipe = text_format.Parse(
            r"""
            title: "Raw egg"
            """, inventory_pb2.Recipe())
        with self._db_context() as db:
            db_lib.insert_recipe(db, "raw-egg-id", recipe)
        with self._app.test_client() as client:
            rv = client.get('/recipe')
            assert b'Raw egg' in rv.data
            assert b'raw-egg-id' in rv.data

    def test_shows_past_cooked_meal(self):
        """Tests that a past cooked meal is shown."""
        recipe = text_format.Parse(
            r"""
            title: "Raw egg"
            """, inventory_pb2.Recipe())
        past_cooked_meal = text_format.Parse(
            r"""
            recipe_id: "raw-egg-id"
            date { year: 2000 month: 1 day: 2 }
            """, inventory_pb2.PastCookedMeal())
        with self._db_context() as db:
            db_lib.insert_recipe(db, "raw-egg-id", recipe)
            db_lib.insert_past_cooked_meal(db, past_cooked_meal)
        with self._app.test_client() as client:
            rv = client.get('/history')
            assert b'raw-egg-id' in rv.data
            assert b'2000-01-02' in rv.data

    def test_adds_past_cooked_meal(self):
        """Tests that a past cooked meal can be added."""
        recipe = text_format.Parse(
            r"""
            title: "Raw egg"
            """, inventory_pb2.Recipe())
        with self._db_context() as db:
            db_lib.insert_recipe(db, "raw-egg-id", recipe)
        with self._app.test_client() as client:
            client.post('/history',
                        data={
                            'recipe_id': 'raw-egg-id',
                            'date': '2020-01-02'
                        })
        with self._db_context() as db:
            history = db_lib.load_history(db)
        hamcrest.assert_that(
            history,
            proto_matcher.equals_proto(r"""
                past_cooked_meal {
                  recipe_id: "raw-egg-id"
                  date { year: 2020 month: 1 day: 2 }
                }"""))

    def test_ingredient_index(self):
        with self._db_context() as db:
            db_lib.insert_ingredient_type(
                db, inventory_pb2.IngredientType(id="/ingredient/spice/salt"))
        with self._app.test_client() as client:
            rv = client.get('/ingredient')
            assert b'/ingredient/spice/salt' in rv.data

    def test_json_ingredient_index(self):
        with self._db_context() as db:
            db_lib.insert_ingredient_type(
                db, inventory_pb2.IngredientType(id="/ingredient/spice/salt"))
        with self._app.test_client() as client:
            rv = client.get('/ingredient', json={})
            assert rv.json == ['/ingredient/spice/salt']

    def test_shows_ingredient(self):
        """Tests page for showing ingredient."""
        # TODO(agentydragon): use fixture
        product = text_format.Parse(
            r"""
            merchant: "testmerchant"
            merchant_product_id: 12345
            comment: "blahblah"
            contents {
              ingredient_id: "/ingredient/spice/salt"
              amount { unit: GRAM amount_exact: 100 }
            }
            """, inventory_pb2.Product())
        with self._db_context() as db:
            db_lib.insert_ingredient_type(
                db, inventory_pb2.IngredientType(id="/ingredient/spice/salt"))
            db_lib.insert_product(db, product)
        with self._app.test_client() as client:
            rv = client.get('/ingredient/ingredient/spice/salt')
            # Should show link to product that has the ingredient directly.
            assert b'/product/testmerchant/12345' in rv.data

    def test_shows_product(self):
        """Tests that a product is shown."""
        product = text_format.Parse(
            r"""
            merchant: "testmerchant"
            merchant_product_id: 12345
            comment: "blahblah"
            contents {
              ingredient_id: "/ingredient/abc"
              amount { unit: GRAM amount_exact: 100 }
            }
            """, inventory_pb2.Product())
        with self._db_context() as db:
            db_lib.insert_product(db, product)
        with self._app.test_client() as client:
            rv = client.get('/product/testmerchant/12345')
            assert b'testmerchant' in rv.data
            assert b'12345' in rv.data
            assert b'blahblah' in rv.data
            assert b'/ingredient/abc' in rv.data

    def test_shows_product_on_index(self):
        """Tests that a product is shown on product index."""
        product = text_format.Parse(
            r"""
            merchant: "testmerchant"
            merchant_product_id: 12345
            comment: "blahblah"
            contents {
              ingredient_id: "/ingredient/abc"
              amount { unit: GRAM amount_exact: 100 }
            }
            """, inventory_pb2.Product())
        with self._db_context() as db:
            db_lib.insert_product(db, product)
        with self._app.test_client() as client:
            rv = client.get('/product')
            assert b'testmerchant' in rv.data
            assert b'12345' in rv.data
            assert b'blahblah' in rv.data
            assert b'/ingredient/abc' in rv.data

            # Should contain link to the product's page.
            assert b'<a href="/product/testmerchant/12345"' in rv.data

    def test_deletes_product(self):
        """Tests that a product can be deleted."""
        product = text_format.Parse(
            r"""
            merchant: "testmerchant"
            merchant_product_id: 12345
            comment: "blahblah"
            contents {
              ingredient_id: "/ingredient/abc"
              amount { unit: GRAM amount_exact: 100 }
            }
            """, inventory_pb2.Product())
        with self._db_context() as db:
            db_lib.insert_product(db, product)
        with self._app.test_client() as client:
            rv = client.get('/product')
            assert b'12345' in rv.data

            client.post('/product/testmerchant/12345/delete')
            # TODO(agentydragon): test redirect

            rv = client.get('/product')
            assert b'12345' not in rv.data

    def test_adds_product(self):
        """Tests that a product can be added."""
        with self._app.test_client() as client:
            client.post(
                '/product',
                data={
                    'merchant':
                        'testmerchant',
                    'merchant_product_id':
                        '12345',
                    'comment':
                        'blahblah',
                    'contents':
                        'ingredient_id: "/ingredient/abc" amount { unit: GRAM amount_exact: 100 }'
                })
            # TODO(agentydragon): test redirect

        with self._db_context() as db:
            products = db_lib.load_products(db)
            assert len(products) == 1

            hamcrest.assert_that(
                products[0],
                proto_matcher.equals_proto(r"""
            merchant: "testmerchant"
            merchant_product_id: 12345
            comment: "blahblah"
            contents {
              ingredient_id: "/ingredient/abc"
              amount { unit: GRAM amount_exact: 100 }
            }
            """))


# TODO(agentydragon): test for making and rendering a plan

if __name__ == '__main__':
    absltest.main()
