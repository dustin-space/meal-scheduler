import collections

from absl import logging
from google.protobuf import text_format
from meal_scheduler import unit as unit_lib
from meal_scheduler.data import ingredient_types as ingredient_types_lib
from meal_scheduler.inventory_py_proto_pb import inventory_pb2

CONVERSIONS_RUNFILES_PATH = 'meal_scheduler/data/conversions.textproto'


def load_conversions(runfiles):
    path = runfiles.Rlocation(CONVERSIONS_RUNFILES_PATH)
    with open(path, 'rb') as f:
        return text_format.Parse(f.read(), inventory_pb2.Conversions())


class NoConversionException(Exception):
    pass


def find_conversions(ingredient, to_unit, units, convs, ingredient_database):
    result = {}
    # YAPF does not like assignment expressions :(
    for from_unit in units:
        ratio = find_conversion_ratio(ingredient, from_unit, to_unit, convs,
                                      ingredient_database)
        if ratio:
            result[from_unit] = ratio
    return result


def find_conversion_ratio(
        ingredient: str, source_unit, target_unit,
        convs: inventory_pb2.Conversions,
        ingredient_database: ingredient_types_lib.IngredientDatabase):
    if source_unit == target_unit:
        return 1.0
    conversions = []
    applicable_types = set(ingredient_database.get_supertypes(ingredient))
    applicable_types.add(ingredient)

    # TODO: Should be able to use multiple conversions - i.e., if I have one
    # conversion saying 100 g and 100 ml are equivalent, and another one saying
    # 200 ml and 1 cup are equivalent, it should imply a conversion between
    # grams and cups.
    for conversion in convs.conversion:
        if not (set(conversion.ingredient_id) & applicable_types):
            continue

        source_amount = None
        target_amount = None
        for unit_amount in conversion.equivalent:
            if unit_amount.unit == source_unit:
                source_amount = unit_amount.amount
            if unit_amount.unit == target_unit:
                target_amount = unit_amount.amount

        if source_amount and target_amount:
            return target_amount / source_amount

    return None


def convert_unit_amount_to_unit(
        unit_amount, unit, convs: inventory_pb2.Conversions,
        ingredient_database: ingredient_types_lib.IngredientDatabase,
        ingredient):
    ratio = find_conversion_ratio(ingredient, unit_amount.unit, unit, convs,
                                  ingredient_database)
    if not ratio:
        return None
    return inventory_pb2.UnitAmount(amount=unit_amount.amount * ratio,
                                    unit=unit)
