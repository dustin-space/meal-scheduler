import collections
import datetime
import os.path
from typing import List, Optional
import timeago

# TODO(agentydragon): This only exists for purpose of debugging frontend
# JavaScript, it should be eventually removed.
from flask_cors import cross_origin

import celery
from celery.result import AsyncResult
import jinja2
from absl import logging
from flask import Flask, Response, current_app, g, redirect, request, jsonify
from google.protobuf import text_format, timestamp_pb2
from meal_scheduler import error_reporter as error_reporter_lib
from meal_scheduler import recipe_amount, time_util, unit_amount
from meal_scheduler.check import check_lib
from meal_scheduler.data import conversions, diet_mappings
from meal_scheduler.data import products as products_lib
from meal_scheduler.data import recipes as recipes_lib
from meal_scheduler.data import stock as stock_lib
from meal_scheduler.data import substitutions
from meal_scheduler.db import db as db_lib
from meal_scheduler.frontend import calendar_export, runfiles_loader
from meal_scheduler.frontend import tasks
from meal_scheduler.frontend import celery_app
from meal_scheduler.frontend import hooks as hooks_lib
from meal_scheduler.inventory_py_proto_pb import inventory_pb2
from meal_scheduler.plan import wrapper as wrapper_lib
from meal_scheduler.plan.minizinc import \
    minizinc_planner as minizinc_planner_lib
from meal_scheduler.stickers import generate as generate_lib
from rules_python.python.runfiles import runfiles as runfiles_lib


def create_app(database_path: str, hooks: hooks_lib.ModuleHooks):
    flask_app = Flask(__name__)
    flask_app.config.from_mapping(DATABASE=database_path)

    def render_template(template, **args):
        results = hooks.build_navigation_bar()
        navigation_bar = [
            ('/', 'stock', 'Stock'),
            ('/check', 'check', 'Check database'),
            ('/recipe', 'recipe', 'Recipes'),
            ('/plan', 'plan', 'Plans'),
            ('/ingredient', 'ingredient', 'Ingredients'),
            ('/history', 'history', 'Past cooked meals'),
            ('/product', 'product', 'Products'),
        ]
        for result in results:
            if not result:
                continue
            navigation_bar.extend(result)
        args['navigation_bar'] = navigation_bar
        return get_jinja_env().get_template(template).render(**args)

    @flask_app.teardown_appcontext
    def close_connection(exception):
        logging.info("Closing database connection.")
        if '_database' in g:
            g.pop('_database').close()

    def stock_item_content_json(item):
        i = {
            "comment":
                item.comment,
            # epoch seconds
            "timestamp":
                time_util.timestamp_proto_to_epoch_seconds(item.timestamp)
        }
        if item.amount.HasField('enough'):
            i["amount"] = "enough"
        elif item.amount.HasField('exact'):
            i["unit"] = item.amount.exact.unit
            i["amount"] = item.amount.exact.amount
        else:
            assert item.amount.HasField('lower_bound')
            i["unit"] = item.amount.lower_bound.unit
            i["amount"] = item.amount.lower_bound.amount
        return i

    @flask_app.route('/stock.json')
    @cross_origin()
    def json_stock_index():
        stock = get_stock()
        # convert list of StockItem into json
        j = {}
        for item in stock.stock:
            j[item.ingredient_id] = stock_item_content_json(item)

        return jsonify(j)

    @flask_app.route('/')
    def stock_index():
        stock = get_stock()
        plan_request = inventory_pb2.PlanRequest(
            meal_request=[
                inventory_pb2.MealRequest(
                    portions=4,
                    date=time_util.date_to_date_proto(datetime.date.today() +
                                                      datetime.timedelta(
                                                          days=i)))
                for i in range(5)
            ],
            max_added_ingredients=10,
            max_recipe_type=[
                inventory_pb2.MaxRecipeType(name_part='curry', max_amount=2),
                inventory_pb2.MaxRecipeType(name_part='risotto', max_amount=2),
                inventory_pb2.MaxRecipeType(name_part='nudel', max_amount=1),
            ])
        wrapper_lib.validate_plan_request(plan_request)
        return render_template(
            'stock_index.html',
            stock=stock,
            plan_request_textproto=text_format.MessageToString(plan_request))

    @flask_app.route('/stock', methods=['POST'])
    @cross_origin()
    def upsert_stock():
        if request.is_json:
            # input format is same as in json_stock_index:
            amount = inventory_pb2.StockAmount()
            if request.json['amount'] == "enough":
                amount.enough.SetInParent()
            else:
                # TODO: fix - sometimes it's given as string from FE
                amount.exact.unit = int(request.json['unit'])
                amount.exact.amount = request.json['amount']
            stock_item = inventory_pb2.StockItem(
                ingredient_id=request.json['ingredient_id'],
                amount=amount,
                comment=request.json['comment'])
        else:
            data = request.form
            stock_item = inventory_pb2.StockItem(
                ingredient_id=data['ingredient_id'],
                amount=text_format.Parse(data['amount'],
                                         inventory_pb2.StockAmount()),
                comment=data['comment'])

        db = get_db()
        logging.info("Updating stock: %s", stock_item)
        # TODO(agentydragon): check_stock_item should happen inside update_stock...
        ingredient_database = get_ingredient_database()
        db_lib.check_stock_item(stock_item, ingredient_database)
        db_lib.update_stock(db, stock_item)

        # load to get current timestamp (ugh)
        stock = db_lib.load_stock(db)
        stock_item = stock.find_stock_item(stock_item.ingredient_id)

        if request.is_json:
            return jsonify({
                'success': True,
                'state': stock_item_content_json(stock_item)
            })
        else:
            return redirect('/')

    @flask_app.route('/check')
    def check():
        db = get_db()
        stock = db_lib.load_stock(db)
        ingredient_database = get_ingredient_database()
        issues = []
        error_reporter = error_reporter_lib.ErrorReporter()
        all_recipes = get_all_recipes()
        products = get_products()
        convs = get_conversions()
        check_lib.check_all(ingredient_database, stock, all_recipes, products,
                            convs, error_reporter)
        error_reporter.log_errors()
        return render_template('check.html', issues=error_reporter.errors)

    @flask_app.route('/recipe', methods=['GET'])
    def recipe_index():
        db = get_db()
        all_recipes = get_all_recipes()
        return render_template('recipe_index.html', recipes=all_recipes)

    @flask_app.route('/recipe/<recipe_id>', methods=['GET'])
    def show_recipe(recipe_id):
        db = get_db()
        all_recipes = get_all_recipes()
        recipe = recipes_lib.find_recipe_by_id(all_recipes, recipe_id)
        return render_template('recipe.html', recipe=recipe)

    @flask_app.route('/plan', methods=['GET'])
    def plan_index():
        db = get_db()
        plans = db_lib.load_all_plans(db)
        return render_template('plan_index.html', plans=plans)

    @flask_app.route('/plan', methods=['POST'])
    def plan():
        plan_request = text_format.Parse(request.form['plan_request_textproto'],
                                         inventory_pb2.PlanRequest())
        ingredient_database = get_ingredient_database()
        products = get_products()
        diet_map = get_diet_mappings()
        convs = get_conversions()
        subs = get_substitutions()
        h = get_history()
        s = get_stock()
        all_recipes = get_all_recipes()
        plan_proto = wrapper_lib.run_plan(
            s,
            all_recipes,
            plan_request,
            h,
            diet_map,
            subs,
            products,
            convs,
            ingredient_database,
            backend=minizinc_planner_lib.plan,
            force_same_quantum_for_connected_components=True,
            merchant_product_statuses=get_merchant_product_statuses())
        #force_same_quantum_for_connected_components=False)
        db = get_db()
        plan_id = db_lib.insert_plan(db, plan_proto, plan_request)
        #db.commit()
        return redirect(f'/plan/{plan_id}')

    # TODO(agentydragon): would be nice to support DELETE but it can't be sent
    # from HTML forms
    @flask_app.route('/plan/<plan_id>/delete', methods=['POST'])
    def delete_plan(plan_id):
        db = get_db()
        db_lib.delete_plan(db, plan_id)
        return redirect(f'/plan')

    @flask_app.route('/plan/<plan_id>/comment', methods=['POST'])
    def update_plan_comment(plan_id):
        db = get_db()
        db_lib.update_plan_comment(db, plan_id, request.form['comment'])
        return redirect(f'/plan/{plan_id}')

    @flask_app.route('/plan/<plan_id>/stickers/<int:page>.svg', methods=['GET'])
    def get_plan_stickers(plan_id, page):
        db = get_db()
        stored_plan = db_lib.get_plan(db, plan_id)
        all_recipes = get_all_recipes()
        s = get_stock()
        stickers_contents = generate_lib.generate_stickers(
            stored_plan.plan, s, all_recipes)
        return Response(stickers_contents[page], mimetype='image/svg+xml')

    @flask_app.route('/plan/<plan_id>/calendar.ics', methods=['GET'])
    def get_plan_calendar(plan_id):
        all_recipes = get_all_recipes()
        db = get_db()
        stored_plan = db_lib.get_plan(db, plan_id)
        return Response(calendar_export.export_plan_ics(stored_plan.plan,
                                                        all_recipes),
                        mimetype='text/calendar')

    @flask_app.route('/plan/<plan_id>', methods=['GET'])
    def get_plan(plan_id):
        db = get_db()
        products = get_products()
        s = get_stock()
        all_recipes = get_all_recipes()
        stored_plan = db_lib.get_plan(db, plan_id)
        ingredient_database = get_ingredient_database()

        # TODO: print public recipe URL
        stock_state = {}
        pc = {}
        plan_proto = stored_plan.plan

        product_amounts = []
        for purchase in plan_proto.purchase:
            product = products_lib.find_leshop_product_by_id(
                purchase.merchant_product_id, products)
            product_amounts.append((purchase.amount, product))

        selected_recipes = []
        amounts_by_ingredient = collections.defaultdict(dict)
        for planned_recipe in plan_proto.planned_recipe:
            recipe = recipes_lib.find_recipe_by_id(all_recipes,
                                                   planned_recipe.recipe_id)
            selected_recipes.append((recipe, planned_recipe.portions))
            for commitment in planned_recipe.commitment:
                amounts_by_ingredient[commitment.ingredient_id][
                    planned_recipe.recipe_id] = commitment.amount

        for ingredient, amounts in amounts_by_ingredient.items():
            in_stock = s.find_stock_item(ingredient)
            stock_state[ingredient] = {
                'enough':
                    in_stock.amount.HasField('enough') if in_stock else False,
                'amount':
                    in_stock.amount if in_stock else None,
                'last_checked':
                    in_stock.timestamp if in_stock else None,
            }
            products = products_lib.find_direct_products_for_ingredient(
                ingredient,
                products) + products_lib.find_indirect_products_for_ingredient(
                    ingredient_database, ingredient, products)
            if products:
                # TODO(agentydragon): assumes just 1 product per ingredient
                pc[ingredient] = products[0]
            else:
                pass
                # TODO(agentydragon)!
                # If this is not sold in LeShop but needed, there should be a
                # warning

        out_of_system = {}
        for obtain_externally in plan_proto.obtain_externally:
            out_of_system[
                obtain_externally.ingredient_id] = obtain_externally.amount

        totals = {}
        for total_ingredient_use in plan_proto.total_ingredient_use:
            totals[total_ingredient_use.
                   ingredient_id] = total_ingredient_use.amount.unit_amount

        # TODO(agentydragon): optimize
        stickers_pages_count = len(
            generate_lib.generate_stickers(stored_plan.plan, s, all_recipes))

        return render_template('plan.html',
                               stored_plan=stored_plan,
                               product_amounts=product_amounts,
                               selected_recipes=selected_recipes,
                               amounts_by_ingredient=sorted(
                                   amounts_by_ingredient.items()),
                               stock_state=stock_state,
                               totals=totals,
                               pc=pc,
                               out_of_system=out_of_system,
                               stickers_pages_count=stickers_pages_count)

    @flask_app.route('/history', methods=['GET'])
    def history_index():
        history = get_history()
        all_recipes = get_all_recipes()
        recipes_by_id = {recipe.id: recipe for recipe in all_recipes}
        return render_template('history.html',
                               past_cooked_meals=history.past_cooked_meal,
                               recipes_by_id=recipes_by_id)

    @flask_app.route('/history', methods=['POST'])
    def add_past_cooked_meal():
        data = request.form
        past_cooked_meal = inventory_pb2.PastCookedMeal(
            recipe_id=data['recipe_id'],
            date=time_util.date_proto_from_iso8601(data['date']))
        db = get_db()
        db_lib.insert_past_cooked_meal(db, past_cooked_meal)
        return redirect('/history')

    @flask_app.route('/ingredient', methods=['GET'])
    @cross_origin()
    def ingredient_index():
        ingredient_database = get_ingredient_database()
        if request.is_json:
            # Returns list of all ingredient IDs.
            return jsonify(list(ingredient_database.ingredient_ids))
        else:
            db = get_db()
            merchant_product_statuses = db_lib.load_merchant_product_statuses(
                db)
            products = db_lib.load_products(db)

            # TODO(agentydragon): add a test for this
            products_by_ingredient_id = {}
            for product in products:
                if product.contents.ingredient_id not in products_by_ingredient_id:
                    products_by_ingredient_id[
                        product.contents.ingredient_id] = []
                products_by_ingredient_id[
                    product.contents.ingredient_id].append(product)

            product_available_by_product_id = {}
            for product_status in merchant_product_statuses:
                assert product_status.product_id not in product_available_by_product_id
                product_available_by_product_id[product_status.product_id] = (
                    product_status.available_exact > 0 or
                    product_status.available_lower_bound > 0)

            ingredient_product_states = {}
            for ingredient_id in ingredient_database.ingredient_ids:
                subtypes = set(
                    ingredient_database.get_subtypes_recursive(ingredient_id))
                subtypes.add(ingredient_id)

                products = []

                for subtype_id in subtypes:
                    if subtype_id not in products_by_ingredient_id:
                        continue
                    # TODO(agentydragon): make it unique?
                    products.extend(products_by_ingredient_id[subtype_id])

                if not products:
                    ingredient_product_states[ingredient_id] = 'no_product'
                    continue

                availables = [
                    str(product.merchant_product_id)
                    for product in products
                    if product_available_by_product_id.get(
                        str(product.merchant_product_id), False)
                ]
                if availables:
                    ingredient_product_states[
                        ingredient_id] = 'available (' + ', '.join(
                            availables) + ')'
                    continue
                ingredient_product_states[ingredient_id] = (
                    'no_product_available (' + ', '.join(
                        str(product.merchant_product_id)
                        for product in products) + ')')

            ingredient_used_directly = collections.Counter()
            ingredient_used_indirectly = collections.Counter()
            all_recipes = get_all_recipes()
            for recipe in all_recipes:
                for ingredient_id in recipe.ingredient_ids:
                    ingredient_used_directly[ingredient_id] += 1
                    for supertype in ingredient_database.get_subtypes_recursive(
                            ingredient_id):
                        ingredient_used_indirectly[supertype] += 1

            stock = get_stock()
            stock_item_by_ingredient_id = {
                item.ingredient_id: item for item in stock.stock
            }

            # TODO(agentydragon): mark ingredient as available (buyable) if any
            # subtype is buyable

            return render_template(
                'ingredient_index.html',
                ingredient_database=ingredient_database,
                ingredient_product_states=ingredient_product_states,
                stock_item_by_ingredient_id=stock_item_by_ingredient_id,
                ingredient_used_directly=ingredient_used_directly,
                ingredient_used_indirectly=ingredient_used_indirectly)

    # TODO(agentydragon): add test
    @flask_app.route('/ingredient/<path:ingredient_id>', methods=['GET'])
    def get_ingredient(ingredient_id):
        ingredient_database = get_ingredient_database()
        products = get_products()
        # TODO(agentydragon): 404 if no such ingredient
        ingredient_id = '/' + ingredient_id
        all_recipes = get_all_recipes()
        used_in_recipes_directly = []
        used_in_recipes_indirectly = []
        supertypes = ingredient_database.get_supertypes(ingredient_id)
        for recipe in all_recipes:
            if ingredient_id in recipe.ingredient_ids:
                used_in_recipes_directly.append(recipe)
            elif set(supertypes) & recipe.ingredient_ids:
                used_in_recipes_indirectly.append(recipe)

        direct_ingredient_products = products_lib.find_direct_products_for_ingredient(
            ingredient_id, products)
        indirect_ingredient_products = products_lib.find_indirect_products_for_ingredient(
            ingredient_database, ingredient_id, products)

        ingredient = ingredient_database.find_ingredient_by_id(ingredient_id)
        return render_template(
            'ingredient.html',
            ingredient_id=ingredient_id,
            used_in_recipes_directly=used_in_recipes_directly,
            ingredient=ingredient,
            supertypes=supertypes,
            used_in_recipes_indirectly=used_in_recipes_indirectly,
            direct_ingredient_products=direct_ingredient_products,
            indirect_ingredient_products=indirect_ingredient_products)

    @flask_app.route('/product', methods=['GET'])
    def product_index():
        products = get_products()
        return render_template('product_index.html', products=products)

    @flask_app.route('/product/<merchant>/<int:merchant_product_id>',
                     methods=['GET'])
    def get_product(merchant, merchant_product_id):
        db = get_db()
        products = get_products()
        # db_lib.delete_product(db, merchant, merchant_product_id)
        product = products_lib.find_by_merchant_merchant_product_id(
            products, merchant, merchant_product_id)
        merchant_product_status_history = db_lib.load_product_merchant_status_history(
            db, product.merchant, str(product.merchant_product_id))
        return render_template(
            'product.html',
            product=product,
            merchant_product_status_history=merchant_product_status_history)

    @flask_app.route('/product/<merchant>/<merchant_product_id>/delete',
                     methods=['POST'])
    def delete_product(merchant, merchant_product_id):
        db = get_db()
        db_lib.delete_product(db, merchant, merchant_product_id)
        return redirect(f'/product')

    @flask_app.route('/product', methods=['POST'])
    def add_product():
        data = request.form
        product = inventory_pb2.Product(
            merchant=data['merchant'],
            merchant_product_id=int(data['merchant_product_id']),
            contents=text_format.Parse(data['contents'],
                                       inventory_pb2.InventoryLine()),
            comment=data['comment'])
        db = get_db()
        db_lib.insert_product(db, product)
        return redirect('/product')

    @flask_app.route('/celery/start/<name>', methods=['GET'])
    def celery_start(name):
        result = tasks.test_task.delay(name)
        return f'<a href=/celery/get/{result.id}>{result.id}</a>'

    @flask_app.route('/celery/get/<task_id>', methods=['GET'])
    def celery_get(task_id):
        res = AsyncResult(task_id, app=celery_app.CELERY_APP)
        result = f"{res.state}"
        if res.successful():
            result += " " + res.result
        return result + " <a href=/celery/forget/{task_id}>forget {task_id}</a>"

    @flask_app.route('/celery/forget/<task_id>', methods=['GET'])
    def celery_forget(task_id):
        res = AsyncResult(task_id, app=celery_app.CELERY_APP)
        res.forget()
        return "forgotten"

    @flask_app.route('/static/<path:filename>')
    def download_file(filename):
        runfiles = get_runfiles()
        path = runfiles.Rlocation(STATIC_RUNFILES_MARKER)
        return send_from_directory(os.path.dirname(path), filename)

    hooks.init_flask_app(flask_app)

    return flask_app


def get_db():
    if '_database' not in g:
        path = current_app.config['DATABASE']
        logging.info("Opening database from %s", path)
        db = db_lib.open_db(path)
        g._database = db
    return g._database


def get_runfiles():
    if '_runfiles' not in g:
        g._runfiles = runfiles_lib.Create()
    return g._runfiles


TEMPLATE_RUNFILES_ROOT = 'meal_scheduler/frontend/templates'
STATIC_RUNFILES_MARKER = 'meal_scheduler/frontend/static/MARK'


def format_timestamp_proto(ts: timestamp_pb2.Timestamp) -> str:
    return time_util.timestamp_proto_to_datetime(ts).isoformat()


def proto_to_textproto(proto) -> str:
    return text_format.MessageToString(proto)


def time_ago_timestamp_proto(ts: timestamp_pb2.Timestamp) -> str:
    dt = time_util.timestamp_proto_to_datetime(ts)
    return timeago.format(dt, datetime.datetime.now())


def get_jinja_env():
    env = jinja2.Environment(loader=runfiles_loader.RunfilesLoader(
        get_runfiles(), TEMPLATE_RUNFILES_ROOT))
    env.filters['format_timestamp_proto'] = format_timestamp_proto
    env.filters['amount_to_str'] = recipe_amount.amount_to_str
    env.filters['amount_in_stock_to_str'] = stock_lib.amount_in_stock_to_str
    env.filters[
        'product_unit_amount_to_str'] = products_lib.product_unit_amount_to_str
    env.filters['leshop_link'] = products_lib.leshop_link
    env.filters['unit_amount_to_str'] = unit_amount.unit_amount_to_str
    env.filters['proto_to_textproto'] = proto_to_textproto
    env.filters['date_proto_to_iso8601'] = time_util.date_proto_to_iso8601
    env.filters['time_ago_timestamp_proto'] = time_ago_timestamp_proto
    return env


def get_stock():
    return db_lib.load_stock(get_db())


def get_ingredient_database():
    return db_lib.load_ingredient_types(get_db())


def get_products():
    return db_lib.load_products(get_db())


def get_diet_mappings():
    return diet_mappings.load_diet_mappings(get_runfiles())


def get_conversions():
    return conversions.load_conversions(get_runfiles())


def get_substitutions():
    return substitutions.load_substitutions(get_runfiles())


def get_merchant_product_statuses(
) -> Optional[List[inventory_pb2.MerchantProductStatus]]:
    return db_lib.load_merchant_product_statuses(get_db())


def get_history():
    return db_lib.load_history(get_db())


def get_all_recipes():
    return recipes_lib.load_recipes_from_db(get_db())
