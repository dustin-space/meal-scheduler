import datetime
import itertools
from typing import List

import ics
import pytz
from meal_scheduler.data import recipes as recipes_lib
from meal_scheduler.inventory_py_proto_pb import inventory_pb2


def export_plan_ics(plan: inventory_pb2.Plan,
                    all_recipes: List[recipes_lib.RecipeWrapper]) -> str:
    # TODO(agentydragon): write test
    calendar = ics.Calendar()
    first_day = datetime.date.today()
    tz = pytz.timezone('Europe/Zurich')
    time_begin = datetime.time(12, 00, tzinfo=tz)
    time_end = datetime.time(13, 00, tzinfo=tz)

    for i, planned_recipe in enumerate(plan.planned_recipe):
        day = first_day + datetime.timedelta(days=i)
        # TODO(agentydragon): add link to recipe UI if it exists
        event = ics.Event()
        recipe = recipes_lib.find_recipe_by_id(all_recipes,
                                               planned_recipe.recipe_id)
        event.name = recipe.title
        # TODO(agentydragon): set begin based on planned recipe time, once it's
        # implemented.
        event.begin = datetime.datetime.combine(day, time_begin)
        event.end = datetime.datetime.combine(day, time_end)
        event.description = f"""
Meal: {recipe.title}
URL: {recipe.source.url}
Portions: {planned_recipe.portions}
ID: {planned_recipe.recipe_id}

Created by Dustin Space Meal Scheduler.""".strip()
        calendar.events.add(event)

    return str(calendar)
