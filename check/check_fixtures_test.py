from absl.testing import absltest
from google.protobuf import text_format
from meal_scheduler import error_reporter as error_reporter_lib
from meal_scheduler.check import check_lib
from meal_scheduler.data import conversions as conversions_lib
from meal_scheduler.data import recipes
from meal_scheduler.data import stock as stock_lib
from meal_scheduler.db import db as db_lib
from meal_scheduler.db import import_recipes
from meal_scheduler.db import init as init_lib
from meal_scheduler.db import import_ingredient_types_lib
from meal_scheduler.inventory_py_proto_pb import inventory_pb2
from rules_python.python.runfiles import runfiles

RECIPE_SLUG_RUNFILES = "meal_scheduler/data/recipe_import.textproto"
INGREDIENT_TYPES_RUNFILES = "meal_scheduler/data/ingredient_types.textproto"


class CheckFixturesTest(absltest.TestCase):

    def setUp(self):
        self._db = db_lib.open_db(":memory:")
        init_lib.init_db(self._db)

        r = runfiles.Create()

        import_ingredient_types_lib.import_ingredient_types_from_file(
            db=self._db, path=r.Rlocation(INGREDIENT_TYPES_RUNFILES))

        self._ingredient_database = db_lib.load_ingredient_types(self._db)
        with open(r.Rlocation(RECIPE_SLUG_RUNFILES), 'rb') as f:
            recipe_import = text_format.Parse(f.read(),
                                              inventory_pb2.RecipeImport())

        import_recipes.import_recipes(self._db, recipe_import)

        self._all_recipes = recipes.load_recipes_from_db(self._db)
        self._conversions = conversions_lib.load_conversions(r)

    def test_all(self):
        error_reporter = error_reporter_lib.ErrorReporter()
        check_lib.check_all(self._ingredient_database,
                            stock_lib.StockDatabase([]),
                            self._all_recipes,
                            products=[],
                            convs=self._conversions,
                            error_reporter=error_reporter)
        error_reporter.log_errors()
        assert error_reporter.ok(), "Errors ... somewhere"


if __name__ == '__main__':
    absltest.main()
