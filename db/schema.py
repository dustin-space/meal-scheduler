from sqlalchemy import (Column, Integer, LargeBinary, MetaData, String, Table,
                        UniqueConstraint)

METADATA = MetaData()

STOCK_TABLE = Table('Stock', METADATA,
                    Column('ingredient_id', String, nullable=False),
                    Column('timestamp', Integer),
                    Column('stock_amount', LargeBinary(), nullable=False),
                    Column('comment', String))

RECIPE_TABLE = Table('Recipe', METADATA,
                     Column('id', String, nullable=False, primary_key=True),
                     Column('importer', String), Column('importer_id', String),
                     Column('recipe', LargeBinary(), nullable=False),
                     UniqueConstraint('importer', 'importer_id'))

PLAN_TABLE = Table('Plan', METADATA,
                   Column('plan_id', String, nullable=False, primary_key=True),
                   Column('created_at', Integer, nullable=False),
                   Column('plan', LargeBinary(), nullable=False),
                   Column('plan_request', LargeBinary(), nullable=False),
                   Column('comment', String))

PAST_COOKED_MEAL_TABLE = Table('PastCookedMeal', METADATA,
                               Column('recipe_id', String, nullable=False),
                               Column('date', String, nullable=False))

MERCHANT_PRODUCT_STATUS_TABLE = Table(
    'MerchantProductStatus', METADATA, Column('merchant',
                                              String,
                                              nullable=False),
    Column('merchant_product_id', String, nullable=False),
    Column('timestamp', Integer, nullable=False),
    Column('merchant_product_status', LargeBinary(), nullable=False))

PRODUCT_TABLE = Table('Product', METADATA,
                      Column('merchant', String, nullable=False),
                      Column('merchant_product_id', String, nullable=False),
                      Column('content', LargeBinary(), nullable=False),
                      Column('comment', String))

INGREDIENT_TYPE_TABLE = Table(
    'IngredientType', METADATA, Column('id', String, nullable=False),
    Column('ingredient_type', LargeBinary(), nullable=False),
    UniqueConstraint('id'))

INGREDIENT_MATTER_RULE_TABLE = Table(
    'IngredientMatterRule', METADATA,
    Column('ingredient_id_regex', String, nullable=False),
    Column('matter_type', Integer, nullable=False))

INGREDIENT_NAME_TABLE = Table('IngredientName', METADATA,
                              Column('ingredient_id', String, nullable=False),
                              Column('comment', String),
                              Column('proto', LargeBinary, nullable=False))
