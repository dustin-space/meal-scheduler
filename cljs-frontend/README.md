# {{name}}

## Run

``` shell
yarn install

yarn watch
```

## Clean

``` shell
yarn clean
```

## Release

``` shell
yarn release
```

to get release breakdown (report):

```shell
yarn run shadow-cljs run shadow.cljs.build-report app report.html
```

## Test

```
yarn run shadow-cljs compile ci
yarn run karma start --single-run
```

## Integration with Bazel

```
yarn run shadow-cljs release app
```

This builds `public/js/compiled/main.js`, which I then want to put into the
frontend...

## License

Copyright © 2017 FIXME

Distributed under the Eclipse Public License either version 1.0 or (at
your option) any later version.
