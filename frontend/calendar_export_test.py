from absl.testing import absltest
from google.protobuf import text_format
from meal_scheduler.data import recipes as recipes_lib
from meal_scheduler.frontend import calendar_export
from meal_scheduler.inventory_py_proto_pb import inventory_pb2


class CalendarExportTest(absltest.TestCase):

    def test_export_simple_plan(self):
        plan = text_format.Parse(
            r"""
          planned_recipe {
            recipe_id: "raw-egg"
            portions: 2
          }""", inventory_pb2.Plan())
        recipe = text_format.Parse(
            r"""
          id: "raw-egg"
          recipe { title: "Raw egg" }
          """, inventory_pb2.StoredRecipe())
        result = calendar_export.export_plan_ics(
            plan, [recipes_lib.RecipeWrapper(recipe)])
        self.assertIn(r"DESCRIPTION:Meal: Raw egg\nURL: \nPortions: 2\n",
                      result)
        self.assertIn("SUMMARY:Raw egg", result)


if __name__ == '__main__':
    absltest.main()
