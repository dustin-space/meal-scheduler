from absl.testing import absltest
from google.protobuf import text_format
from meal_scheduler.db import db as db_lib
from meal_scheduler.db import import_recipes
from meal_scheduler.db import init as init_lib
from meal_scheduler.inventory_py_proto_pb import inventory_pb2


def _make_recipe(title):
    return inventory_pb2.Recipe(title=title)


def _make_recipe_import(importer, ids_to_recipes):
    return inventory_pb2.RecipeImport(
        importer=importer,
        imported_recipe=[
            inventory_pb2.ImportedRecipe(importer_id=importer_id, recipe=recipe)
            for importer_id, recipe in ids_to_recipes.items()
        ])


class ImportRecipesTest(absltest.TestCase):

    def setUp(self):
        self._db = db_lib.open_db(":memory:")
        init_lib.init_db(self._db)

    def tearDown(self):
        self._db.close()

    def test_import_single(self):
        recipe = _make_recipe("Testing tasty recipe 123")
        recipe_import = _make_recipe_import(importer="test",
                                            ids_to_recipes={"123": recipe})

        stats = import_recipes.import_recipes(self._db, recipe_import)

        recipes = db_lib.load_all_recipes(self._db)
        self.assertEqual(stats, {'insert': 1, 'update': 0, 'delete': 0})
        self.assertEqual(len(recipes), 1)
        self.assertEqual(recipes[0].importer_key.importer, "test")
        self.assertEqual(recipes[0].importer_key.importer_id, "123")

    def test_import_single_twice(self):
        recipe = _make_recipe("Testing tasty recipe 123")
        recipe_import = _make_recipe_import(importer="test",
                                            ids_to_recipes={"123": recipe})

        stats = import_recipes.import_recipes(self._db, recipe_import)
        self.assertEqual(stats, {'insert': 1, 'update': 0, 'delete': 0})

        stats = import_recipes.import_recipes(self._db, recipe_import)
        self.assertEqual(stats, {'insert': 0, 'update': 0, 'delete': 0})

        recipes = db_lib.load_all_recipes(self._db)
        self.assertEqual(len(recipes), 1)
        self.assertEqual(recipes[0].importer_key.importer, "test")
        self.assertEqual(recipes[0].importer_key.importer_id, "123")

    def test_import_empty(self):
        recipe_import = _make_recipe_import(importer="test", ids_to_recipes={})
        stats = import_recipes.import_recipes(self._db, recipe_import)
        self.assertEqual(stats, {'insert': 0, 'update': 0, 'delete': 0})

    def test_import_one_then_delete(self):
        recipe = _make_recipe("Testing tasty recipe 123")
        recipe_import = _make_recipe_import(importer="test",
                                            ids_to_recipes={"123": recipe})

        stats = import_recipes.import_recipes(self._db, recipe_import)
        self.assertEqual(stats, {'insert': 1, 'update': 0, 'delete': 0})

        recipe_import = _make_recipe_import(importer="test", ids_to_recipes={})

        stats = import_recipes.import_recipes(self._db,
                                              recipe_import,
                                              delete_missing=True)
        self.assertEqual(stats, {'insert': 0, 'update': 0, 'delete': 1})

        recipes = db_lib.load_all_recipes(self._db)
        self.assertEqual(len(recipes), 0)


# TODO(agentydragon): test deletions

    def get_importer_key_to_title_map(self):
        recipes = db_lib.load_all_recipes(self._db)
        return {
            recipe.importer_key.importer_id: recipe.recipe.title
            for recipe in recipes
        }

    def test_update_two_recipes(self):
        stats = import_recipes.import_recipes(
            self._db,
            _make_recipe_import(importer="test",
                                ids_to_recipes={
                                    "123": _make_recipe("Recipe 1 before"),
                                    "456": _make_recipe("Recipe 2 before")
                                }))
        self.assertEqual(stats, {'insert': 2, 'delete': 0, 'update': 0})
        recipes = db_lib.load_all_recipes(self._db)

        stats = import_recipes.import_recipes(
            self._db,
            _make_recipe_import(importer="test",
                                ids_to_recipes={
                                    "123": _make_recipe("Recipe 1 after"),
                                    "456": _make_recipe("Recipe 2 after")
                                }))
        self.assertEqual(stats, {'insert': 0, 'delete': 0, 'update': 2})

        recipes = db_lib.load_all_recipes(self._db)
        self.assertEqual({
            "123": "Recipe 1 after",
            "456": "Recipe 2 after"
        }, self.get_importer_key_to_title_map())

    def test_delete_two_recipes(self):
        stats = import_recipes.import_recipes(
            self._db,
            _make_recipe_import(importer="test",
                                ids_to_recipes={
                                    "123": _make_recipe("Recipe 1"),
                                    "456": _make_recipe("Recipe 2"),
                                    "789": _make_recipe("Recipe 3")
                                }))
        self.assertEqual(stats, {'insert': 3, 'delete': 0, 'update': 0})
        recipes = db_lib.load_all_recipes(self._db)

        stats = import_recipes.import_recipes(
            self._db,
            _make_recipe_import(
                importer="test",
                ids_to_recipes={"456": _make_recipe("Recipe 2")}),
            delete_missing=True)
        self.assertEqual(stats, {'insert': 0, 'delete': 2, 'update': 0})

        recipes = db_lib.load_all_recipes(self._db)
        self.assertEqual({"456": "Recipe 2"},
                         self.get_importer_key_to_title_map())

if __name__ == '__main__':
    absltest.main()
