import time
from absl import logging
from meal_scheduler.frontend import celery_app


@celery_app.CELERY_APP.task
def test_task(x: str):
    logging.info("starting test_task for %s", x)
    time.sleep(30)
    logging.info("finished test_task for %s", x)
    return "Hello, " + x
