import types
from absl import logging
from meal_scheduler.frontend import hooks as hooks_lib


class SequentialHook(hooks_lib.ModuleHooks):
    """Implements ModuleHooks by calling each added hook class in sequence."""

    def __init__(self):
        self.hooks = []

    def _call_hook(self, name, *args, **kwargs):
        results = []
        for hook in self.hooks:
            results.append(getattr(hook, name)(*args, **kwargs))
        return results

    def add_hook(self, hook):
        self.hooks.append(hook)


def make_sequential_method(name):

    def sequential_method(self, *args, **kwargs):
        logging.info("Calling sequential method %s with %s, %s", name, args,
                     kwargs)
        return self._call_hook(name, *args, **kwargs)

    return sequential_method


for method_name in dir(hooks_lib.ModuleHooks):
    if method_name.startswith(('__', '_abc_')):
        continue
    method = getattr(hooks_lib.ModuleHooks, method_name)
    assert type(
        method
    ) is types.FunctionType, f"type of {method_name} is {type(method)}, expected function"

    # TODO(agentydragon): would be nicer to also pass the docstring, args, etc.

    setattr(SequentialHook, method_name, make_sequential_method(method_name))
