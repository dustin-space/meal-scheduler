from google.protobuf import text_format
from meal_scheduler.diet_mapping_py_proto_pb import diet_mapping_pb2

DIET_MAPPINGS_RUNFILES_PATH = 'meal_scheduler/data/diet_mappings.textproto'


def load_diet_mappings(runfiles):
    path = runfiles.Rlocation(DIET_MAPPINGS_RUNFILES_PATH)
    with open(path, 'rb') as f:
        return text_format.Parse(f.read(), diet_mapping_pb2.DietMappings())
