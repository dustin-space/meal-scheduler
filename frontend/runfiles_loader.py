import os.path

from jinja2 import exceptions, loaders


class RunfilesLoader(loaders.BaseLoader):

    def __init__(self, runfiles, root):
        self._runfiles = runfiles
        self._root = root

    def get_source(self, environment, template):
        path = self._runfiles.Rlocation(os.path.join(self._root, template))
        if not path:
            raise exceptions.TemplateNotFound(template)
        with open(path, 'r') as f:
            return f.read(), template, None
