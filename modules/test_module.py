from meal_scheduler.frontend import hooks as hooks_lib


class _TestModuleHooks(hooks_lib.ModuleHooks):

    def init_flask_app(self, flask_app):

        @flask_app.route('/test_module')
        def test_module_index():
            return "Hello, world!"


HOOKS = _TestModuleHooks()
