import os
import subprocess
import tempfile

from google.protobuf import text_format
from meal_scheduler.plan.planner_py_proto_pb.plan import planner_pb2
from rules_python.python.runfiles import runfiles as runfiles_lib


def run_plan_cc(plan_input: planner_pb2.Input) -> planner_pb2.Output:
    fd, input_path = tempfile.mkstemp(suffix=".input.textproto")
    with os.fdopen(fd, "w") as input_f:
        input_f.write(text_format.MessageToString(plan_input))

    fd, output_path = tempfile.mkstemp(suffix=".output.textproto")
    os.close(fd)

    r = runfiles_lib.Create()
    path = r.Rlocation("meal_scheduler/plan/planner_cc")

    subprocess.check_call([
        path, "--input_textproto_path", input_path, "--output_textproto_path",
        output_path
    ])

    with open(output_path, 'r') as output_f:
        return text_format.Parse(output_f.read(), planner_pb2.Output())
