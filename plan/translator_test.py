import hamcrest
import proto_matcher
from absl import logging
from absl.testing import absltest
from google.protobuf import text_format
from meal_scheduler import error_reporter as error_reporter_lib
from meal_scheduler.data import conversions as conversions_lib
from meal_scheduler.data import ingredient_types
from meal_scheduler.data import recipes as recipes_lib
from meal_scheduler.data import stock as stock_lib
from meal_scheduler.diet_mapping_py_proto_pb import diet_mapping_pb2
from meal_scheduler.inventory_py_proto_pb import inventory_pb2
from meal_scheduler.plan import translator as translator_lib


def _parse_unit_amount(textproto):
    return text_format.Parse(textproto, inventory_pb2.UnitAmount())


def _parse_ingredient_type(textproto):
    return text_format.Parse(textproto, inventory_pb2.IngredientType())


def _parse_stock_item(textproto):
    return text_format.Parse(textproto, inventory_pb2.StockItem())


def _parse_ingredient_types(textproto):
    return list(
        text_format.Parse(textproto,
                          inventory_pb2.IngredientTypes()).ingredient)


def _parse_stored_recipe(textproto):
    return text_format.Parse(textproto, inventory_pb2.StoredRecipe())


def _parse_plan_request(textproto):
    return text_format.Parse(textproto, inventory_pb2.PlanRequest())


def _parse_conversions(textproto):
    return text_format.Parse(textproto, inventory_pb2.Conversions())


EGG_INGREDIENT = _parse_ingredient_type(r"""id: '/ingredient/egg'""")
FIVE_EGGS_STOCK = _parse_stock_item(r"""
    ingredient_id: "/ingredient/egg"
    amount { exact { unit: PIECE amount: 5 } }
    """)
TEN_RAW_EGGS_RECIPE = _parse_stored_recipe(r"""
    id: "ten-raw-eggs"
    recipe {
      title: "10 raw eggs"
      serves { min: 1 max: 1 }
      ingredients {
        ingredient_id: "/ingredient/egg"
        amount { unit_amount { unit: PIECE amount: 10 } }
      }
    }
    """)
TEN_RAW_EGGS_SERVE_TEN_RECIPE = _parse_stored_recipe(r"""
    id: "ten-raw-eggs-serve-ten"
    recipe {
      title: "10 raw eggs serve 10 people"
      serves { min: 10 max: 10 }
      ingredients {
        ingredient_id: "/ingredient/egg"
        amount { unit_amount { unit: PIECE amount: 10 } }
      }
    }
    """)
EGGS_INGREDIENT_DATABASE = ingredient_types.IngredientDatabase(
    _parse_ingredient_types("""
      ingredient { id: '/ingredient/egg' }
    """),
    ingredient_names=[],
    ingredient_matter_rules=[])


class TranslatorTest(absltest.TestCase):
    """Tests behavior when connected components are set to have the same
    quantum."""

    def test_units_of_5_grams(self):
        plan_request = _parse_plan_request(r"""meal_request { portions: 1 }""")
        history = inventory_pb2.History()
        error_reporter = error_reporter_lib.ErrorReporter()

        # Stock contains 10 eggs, recipe asks for 5.
        translator = translator_lib.Translator(
            recipes=[recipes_lib.RecipeWrapper(TEN_RAW_EGGS_RECIPE)],
            products=[],
            stock=stock_lib.StockDatabase([FIVE_EGGS_STOCK]),
            convs=[],
            error_reporter=error_reporter,
            ingredient_database=EGGS_INGREDIENT_DATABASE,
            diet_map=diet_mapping_pb2.DietMappings(),
            force_same_quantum_for_connected_components=False)

        plan_input = translator.translate_to_plan_input(plan_request, history)

        # Scale for eggs will be set to 5 pieces = 1 unit
        hamcrest.assert_that(
            plan_input,
            # TODO: for some reason we can't ignore diet_bit's with
            # proto_matcher.partially. why?
            proto_matcher.equals_proto("""
                recipe {
                  item { amount: 2 }
                  name: "10_raw_eggs"
                }
                ingredient { name: "egg" stock: 1 }
                meal_request { portion_count: 1 }
                diet_bit {}
                diet_bit {}
                diet_bit {}
                diet_bit {}
                diet_bit {}
                diet_bit {}
                """))
        self.assertTrue(error_reporter.ok())

    def test_micro_units_per_portion(self):
        plan_request = _parse_plan_request(r"""meal_request { portions: 1 }""")
        history = inventory_pb2.History()
        error_reporter = error_reporter_lib.ErrorReporter()

        # Stock contains 5 eggs, recipe asks for 10 in total, but it serves 10
        # people.
        translator = translator_lib.Translator(
            recipes=[recipes_lib.RecipeWrapper(TEN_RAW_EGGS_SERVE_TEN_RECIPE)],
            products=[],
            stock=stock_lib.StockDatabase([FIVE_EGGS_STOCK]),
            convs=[],
            error_reporter=error_reporter,
            ingredient_database=EGGS_INGREDIENT_DATABASE,
            diet_map=diet_mapping_pb2.DietMappings(),
            force_same_quantum_for_connected_components=False)

        plan_input = translator.translate_to_plan_input(plan_request, history)

        # Scale for eggs will be set to 5 pieces = 1 unit
        hamcrest.assert_that(
            plan_input,
            # TODO: for some reason we can't ignore diet_bit's with
            # proto_matcher.partially. why?
            proto_matcher.equals_proto("""
                recipe {
                  item { amount: 1 }
                  name: "10_raw_eggs_serve_10_people"
                }
                ingredient { name: "egg" stock: 5 }
                meal_request { portion_count: 1 }
                diet_bit {}
                diet_bit {}
                diet_bit {}
                diet_bit {}
                diet_bit {}
                diet_bit {}
                """))

    # unconvertable amounts, recipe discarded
    def test_discard_recipe_with_unconvertable_amounts(self):
        plan_request = _parse_plan_request(r"""meal_request { portions: 1 }""")
        history = inventory_pb2.History()
        error_reporter = error_reporter_lib.ErrorReporter()

        # Stock contains 5 eggs, recipe asks for 10 in total, but it serves 10
        # people.
        translator = translator_lib.Translator(
            recipes=[
                recipes_lib.RecipeWrapper(
                    _parse_stored_recipe("""
                id: "r1"
                recipe {
                  title: "10 raw eggs serve 10 people"
                  serves { min: 10 max: 10 }
                  ingredients {
                    ingredient_id: "/ingredient/egg"
                    amount { unit_amount { unit: PIECE amount: 10 } }
                  }
                }
            """)),
                recipes_lib.RecipeWrapper(
                    _parse_stored_recipe("""
                id: "r2"
                recipe {
                  title: "100 grams of eggs serve 1 person"
                  serves { min: 1 max: 1 }
                  ingredients {
                    ingredient_id: "/ingredient/egg"
                    amount { unit_amount { unit: GRAM amount: 100 } }
                  }
                }
            """)),
            ],
            products=[],
            stock=stock_lib.StockDatabase([FIVE_EGGS_STOCK]),
            convs=inventory_pb2.Conversions(),
            error_reporter=error_reporter,
            ingredient_database=EGGS_INGREDIENT_DATABASE,
            diet_map=diet_mapping_pb2.DietMappings(),
            force_same_quantum_for_connected_components=False)

        plan_input = translator.translate_to_plan_input(plan_request, history)

        # Scale for eggs will be set to 5 pieces = 1 unit.
        # Note that recipe with 100 grams has been discarded.
        hamcrest.assert_that(
            plan_input,
            # TODO: for some reason we can't ignore diet_bit's with
            # proto_matcher.partially. why?
            proto_matcher.equals_proto("""
                recipe {
                  item { amount: 1 }
                  name: "10_raw_eggs_serve_10_people"
                }
                ingredient { name: "egg" stock: 5 }
                meal_request { portion_count: 1 }
                diet_bit {}
                diet_bit {}
                diet_bit {}
                diet_bit {}
                diet_bit {}
                diet_bit {}
                """))
        self.assertIn(
            'Discarding recipe 100 grams of eggs serve 1 person due to '
            'unconvertable amount of /ingredient/egg: no conversion from '
            'GRAM to PIECE', error_reporter.errors)

    # TODO(agentydragon): test diet mappings - for ingredients

    def test_find_common_units_simple(self):
        ingredient_unit_amounts = {
            'apple': {
                'stock': [_parse_unit_amount("unit: PIECE amount: 20")],
                'recipe 1': [_parse_unit_amount("unit: PIECE amount: 15")]
            }
        }
        convs = inventory_pb2.Conversions()
        ingredient_database = ingredient_types.IngredientDatabase(
            _parse_ingredient_types("ingredient { id: 'apple' }"),
            ingredient_names=[],
            ingredient_matter_rules=[])
        error_reporter = error_reporter_lib.ErrorReporter()
        translators = translator_lib.find_common_units(
            ingredient_unit_amounts,
            convs,
            ingredient_database,
            error_reporter=error_reporter,
            force_same_quantum_for_connected_components=False)
        self.assertEqual(len(translators), 1)
        apple_translator = translators['apple']
        # 5 apples should be chosen as the quantum.
        self.assertEqual(
            apple_translator.to_universal_unit(
                _parse_unit_amount("unit: PIECE amount: 5")), 1)
        self.assertTrue(error_reporter.ok())

    def test_find_common_units_no_mentions(self):
        ingredient_unit_amounts = {'apple': {}}
        convs = inventory_pb2.Conversions()
        ingredient_database = ingredient_types.IngredientDatabase(
            _parse_ingredient_types("ingredient { id: 'apple' }"),
            ingredient_names=[],
            ingredient_matter_rules=[])
        error_reporter = error_reporter_lib.ErrorReporter()
        translators = translator_lib.find_common_units(
            ingredient_unit_amounts,
            convs,
            ingredient_database,
            error_reporter=error_reporter,
            force_same_quantum_for_connected_components=False)
        # No translator will be generated, but no errors reported either.
        self.assertEqual(len(translators), 0)
        self.assertTrue(error_reporter.ok())

    def test_find_ingredient_components_empty(self):
        ingredient_database = ingredient_types.IngredientDatabase(
            ingredient_types=[],
            ingredient_names=[],
            ingredient_matter_rules=[])
        self.assertEqual(
            translator_lib.find_ingredient_components(ingredient_database,
                                                      set()), set())

    def test_find_ingredient_components_nonempty(self):
        ingredient_database = ingredient_types.IngredientDatabase(
            _parse_ingredient_types(r"""
                ingredient { id: 'a' subtypes: ['1', '2'] }
                ingredient { id: 'b' subtypes: '1' }
                ingredient { id: 'c' subtypes: ['2', '3'] }
                ingredient { id: '1' }
                ingredient { id: '2' }
                ingredient { id: '3' subtypes: '3x' }
                ingredient { id: '3x' }
                ingredient { id: 'disconnected' }
              """),
            ingredient_names=[],
            ingredient_matter_rules=[])
        self.assertEqual(
            translator_lib.find_ingredient_components(
                ingredient_database,
                {'a', 'b', 'c', '1', '2', '3', '3x', 'disconnected'}), {
                    frozenset({'a', 'b', 'c', '1', '2', '3', '3x'}),
                    frozenset({'disconnected'})
                })


class SameQuantumForConnectedComponentTest(absltest.TestCase):
    # TODO(agentydragon): test preference for stock

    def test_find_common_units_simple(self):
        error_reporter = error_reporter_lib.ErrorReporter()
        ingredient_unit_amounts = {
            'apple': {
                'stock': [_parse_unit_amount("unit: PIECE amount: 20")],
                'recipe 1': [_parse_unit_amount("unit: PIECE amount: 15")]
            }
        }
        convs = inventory_pb2.Conversions()
        ingredient_database = ingredient_types.IngredientDatabase(
            _parse_ingredient_types("ingredient { id: 'apple' }"),
            ingredient_names=[],
            ingredient_matter_rules=[])
        translators = translator_lib.find_common_units(
            ingredient_unit_amounts,
            convs,
            ingredient_database,
            error_reporter,
            force_same_quantum_for_connected_components=False)
        self.assertEqual(len(translators), 1)
        apple_translator = translators['apple']
        # 5 apples should be chosen as the quantum.
        self.assertEqual(
            apple_translator.to_universal_unit(
                _parse_unit_amount("unit: PIECE amount: 5")), 1)

        self.assertTrue(error_reporter.ok())

    def test_find_common_units_gram_equivalent(self):
        # Green apple weighs 15 grams.
        # Red apple weighs 20 grams.
        # Both can be used in a recipe that needs 40 grams of apples.
        # The quantum will be 5 grams.
        #
        # We have 3 green and 5 red apples in stock.
        # 3 green apples = 45 grams. 5 red apples = 100 grams.
        ingredient_unit_amounts = {
            'apple': {
                'recipe 1': [_parse_unit_amount("unit: GRAM amount: 40")]
            },
            'apple/green': {
                'stock': [_parse_unit_amount("unit: PIECE amount: 3")]
            },
            'apple/red': {
                'stock': [_parse_unit_amount("unit: PIECE amount: 5")]
            },
        }
        convs = _parse_conversions("""
        conversion {
          ingredient_id: 'apple/green'
          equivalent { unit: PIECE amount: 1 }
          equivalent { unit: GRAM amount: 15 }
        }
        conversion {
          ingredient_id: 'apple/red'
          equivalent { unit: PIECE amount: 1 }
          equivalent { unit: GRAM amount: 20 }
        }
      """)
        ingredient_database = ingredient_types.IngredientDatabase(
            _parse_ingredient_types("""
            ingredient {
              id: 'apple'
              subtypes: ['apple/red', 'apple/green']
              canonical_unit: GRAM
            }
            ingredient { id: 'apple/red' }
            ingredient { id: 'apple/green' }
          """),
            ingredient_names=[],
            ingredient_matter_rules=[])
        error_reporter = error_reporter_lib.ErrorReporter()
        translators = translator_lib.find_common_units(
            ingredient_unit_amounts,
            convs,
            ingredient_database,
            error_reporter,
            force_same_quantum_for_connected_components=True)

        self.assertEqual(len(translators), 3)

        red_apple_translator = translators['apple/red']
        self.assertEqual(
            red_apple_translator.to_universal_unit(
                _parse_unit_amount("unit: GRAM amount: 5")), 1)
        # 1 red apple = 20 grams = 4 units
        self.assertEqual(
            red_apple_translator.to_universal_unit(
                _parse_unit_amount("unit: PIECE amount: 1")), 4)

        green_apple_translator = translators['apple/green']
        self.assertEqual(
            green_apple_translator.to_universal_unit(
                _parse_unit_amount("unit: GRAM amount: 5")), 1)
        # 1 green apple = 15 grams = 3 units
        self.assertEqual(
            green_apple_translator.to_universal_unit(
                _parse_unit_amount("unit: PIECE amount: 1")), 3)

        apple_translator = translators['apple']
        self.assertEqual(
            apple_translator.to_universal_unit(
                _parse_unit_amount("unit: GRAM amount: 5")), 1)
        # Generic apples should not afford conversion from pieces, because they
        # have none.
        with self.assertRaises(conversions_lib.NoConversionException):
            apple_translator.to_universal_unit(
                _parse_unit_amount("unit: PIECE amount: 1"))

        self.assertTrue(error_reporter.ok())


if __name__ == '__main__':
    absltest.main()
