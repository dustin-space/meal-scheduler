import datetime
import sqlite3
import uuid
from typing import List, NewType

import sqlalchemy
from google.protobuf import timestamp_pb2
from meal_scheduler import time_util
from meal_scheduler.data import ingredient_types
from meal_scheduler.data import stock as stock_lib
from meal_scheduler.db import schema
from meal_scheduler.inventory_py_proto_pb import inventory_pb2


def open_db(path) -> sqlalchemy.engine.base.Connection:
    engine = sqlalchemy.create_engine('sqlite:///' + path)
    return engine.connect()


def _load_stock_items(
        conn: sqlalchemy.engine.base.Connection
) -> List[inventory_pb2.StockItem]:
    rows = conn.execute(
        sqlalchemy.text("""
        WITH
          StockWithNumber AS (
            SELECT
              ingredient_id,
              timestamp,
              stock_amount,
              comment,
              ROW_NUMBER() OVER (
                  PARTITION BY ingredient_id ORDER BY timestamp DESC)
                  AS row_number
            FROM Stock
          )
        SELECT
          ingredient_id,
          timestamp,
          stock_amount,
          comment
        FROM StockWithNumber
        WHERE row_number = 1
        ORDER BY ingredient_id ASC
    """))
    items = []
    for row in rows:
        amount = inventory_pb2.StockAmount()
        amount.ParseFromString(row['stock_amount'])
        if row['timestamp']:
            ts = time_util.timestamp_proto_from_epoch_nanos(row['timestamp'])
        else:
            ts = None
        items.append(
            inventory_pb2.StockItem(ingredient_id=row['ingredient_id'],
                                    timestamp=ts,
                                    amount=amount,
                                    comment=row['comment']))
    return items


def check_stock_item(
        item: inventory_pb2.StockItem,
        ingredient_database: ingredient_types.IngredientDatabase) -> None:
    if item.ingredient_id not in ingredient_database.ingredient_ids:
        raise Exception(f"{item.ingredient_id} is not a known ingredient")
    # TODO(agentydragon): further checks?
    # TODO(agentydragon): duplication with check.py.


def update_stock(conn: sqlalchemy.engine.base.Connection,
                 item: inventory_pb2.StockItem) -> None:
    # TODO(agentydragon): check_stock_item should happen here...
    assert not (item.timestamp.seconds and
                item.date.year), "Date and timestamp cannot be both set"
    if item.timestamp.seconds:
        time = time_util.timestamp_proto_to_datetime(item.timestamp)
    elif item.date.year:
        time = time_util.date_to_datetime(item.date)
    else:
        time = datetime.datetime.now()

    result = conn.execute(schema.STOCK_TABLE.insert(),
                          ingredient_id=item.ingredient_id,
                          timestamp=time_util.datetime_to_nanos(time),
                          stock_amount=item.amount.SerializeToString(),
                          comment=item.comment)
    assert result.rowcount == 1


def load_stock(conn: sqlalchemy.engine.base.Connection):
    return stock_lib.StockDatabase(_load_stock_items(conn))


PlanId = NewType('PlanId', str)
RecipeId = NewType('RecipeId', str)


def new_uuid() -> str:
    return str(uuid.uuid4())


def insert_plan(conn: sqlalchemy.engine.base.Connection,
                plan: inventory_pb2.Plan,
                plan_request: inventory_pb2.PlanRequest) -> PlanId:
    # TODO(agentydragon): check plan & plan_request for validity?
    plan_id = new_uuid()
    time = datetime.datetime.now()
    conn.execute(schema.PLAN_TABLE.insert(),
                 plan_id=plan_id,
                 created_at=time_util.datetime_to_nanos(time),
                 plan=plan.SerializeToString(),
                 plan_request=plan_request.SerializeToString())
    return plan_id


def _parse_binary_proto(proto_type, serialized):
    # TODO(agentydragon): Use this everywhere.
    proto = proto_type()
    proto.ParseFromString(serialized)
    return proto


def get_plan(conn: sqlalchemy.engine.base.Connection,
             plan_id: PlanId) -> inventory_pb2.StoredPlan:
    row = conn.execute(
        sqlalchemy.select([schema.PLAN_TABLE],
                          schema.PLAN_TABLE.c.plan_id == plan_id)).fetchone()
    assert row
    return inventory_pb2.StoredPlan(
        plan_id=plan_id,
        plan=_parse_binary_proto(inventory_pb2.Plan, row['plan']),
        plan_request=_parse_binary_proto(inventory_pb2.PlanRequest,
                                         row['plan_request']),
        created_at=time_util.timestamp_proto_from_epoch_nanos(
            row['created_at']),
        comment=(row['comment'] or ''))


def load_all_plans(conn: sqlalchemy.engine.base.Connection):
    result = []
    for row in conn.execute(
            sqlalchemy.select([schema.PLAN_TABLE]).order_by(
                sqlalchemy.asc(schema.PLAN_TABLE.c.created_at))):
        result.append(
            inventory_pb2.StoredPlan(
                plan_id=row['plan_id'],
                plan=_parse_binary_proto(inventory_pb2.Plan, row['plan']),
                plan_request=_parse_binary_proto(inventory_pb2.PlanRequest,
                                                 row['plan_request']),
                created_at=time_util.timestamp_proto_from_epoch_nanos(
                    row['created_at']),
                comment=(row['comment'] or '')))
    return result


def delete_plan(conn: sqlalchemy.engine.base.Connection, plan_id):
    result = conn.execute(schema.PLAN_TABLE.delete().where(
        schema.PLAN_TABLE.c.plan_id == plan_id))
    assert result.rowcount == 1


def update_plan_comment(conn: sqlalchemy.engine.base.Connection, plan_id,
                        comment):
    result = conn.execute(schema.PLAN_TABLE.update().values(
        comment=comment).where(schema.PLAN_TABLE.c.plan_id == plan_id))
    assert result.rowcount == 1


def load_all_recipes(
    conn: sqlalchemy.engine.base.Connection
) -> List[inventory_pb2.StoredRecipe]:
    result = []
    for row in conn.execute(
            sqlalchemy.select([schema.RECIPE_TABLE]).order_by(
                sqlalchemy.asc(schema.RECIPE_TABLE.c.importer),
                sqlalchemy.asc(schema.RECIPE_TABLE.c.importer_id),
                sqlalchemy.asc(schema.RECIPE_TABLE.c.id))):
        result.append(
            inventory_pb2.StoredRecipe(id=row['id'],
                                       importer_key=inventory_pb2.ImporterKey(
                                           importer=row['importer'],
                                           importer_id=row['importer_id']),
                                       recipe=_parse_binary_proto(
                                           inventory_pb2.Recipe,
                                           row['recipe'])))
    return result


def insert_recipe(conn: sqlalchemy.engine.base.Connection, recipe_id: RecipeId,
                  recipe: inventory_pb2.Recipe):
    """Used for testing only."""
    conn.execute(schema.RECIPE_TABLE.insert(),
                 id=recipe_id,
                 recipe=recipe.SerializeToString())


def get_recipe(conn: sqlalchemy.engine.base.Connection,
               recipe_id: RecipeId) -> inventory_pb2.StoredRecipe:
    row = conn.execute(
        sqlalchemy.select([schema.RECIPE_TABLE],
                          schema.RECIPE_TABLE.c.id == recipe_id)).fetchone()
    assert row
    return inventory_pb2.StoredRecipe(id=recipe_id,
                                      importer_key=inventory_pb2.ImporterKey(
                                          importer=row['importer'],
                                          importer_id=row['importer_id'],
                                      ),
                                      recipe=_parse_binary_proto(
                                          inventory_pb2.Recipe, row['recipe']))


def load_history(
        conn: sqlalchemy.engine.base.Connection) -> inventory_pb2.History:
    items = []
    for row in conn.execute(
            sqlalchemy.select([schema.PAST_COOKED_MEAL_TABLE]).order_by(
                schema.PAST_COOKED_MEAL_TABLE.c.date,
                schema.PAST_COOKED_MEAL_TABLE.c.recipe_id)):
        items.append(
            inventory_pb2.PastCookedMeal(recipe_id=row['recipe_id'],
                                         date=time_util.date_proto_from_iso8601(
                                             row['date'])))
    return inventory_pb2.History(past_cooked_meal=items)


def validate_past_cooked_meal(conn: sqlalchemy.engine.base.Connection,
                              past_cooked_meal: inventory_pb2.PastCookedMeal):
    # TODO(agentydragon): more elegant existence check?
    assert get_recipe(conn, past_cooked_meal.recipe_id) is not None


def insert_past_cooked_meal(conn: sqlalchemy.engine.base.Connection,
                            past_cooked_meal: inventory_pb2.PastCookedMeal):
    validate_past_cooked_meal(conn, past_cooked_meal)
    result = conn.execute(schema.PAST_COOKED_MEAL_TABLE.insert(),
                          recipe_id=past_cooked_meal.recipe_id,
                          date=time_util.date_proto_to_iso8601(
                              past_cooked_meal.date))
    assert result.rowcount == 1


def insert_product(conn: sqlalchemy.engine.base.Connection,
                   product: inventory_pb2.Product):
    """Used for testing only."""
    conn.execute(schema.PRODUCT_TABLE.insert(),
                 merchant=product.merchant,
                 merchant_product_id=str(product.merchant_product_id),
                 content=product.contents.SerializeToString(),
                 comment=product.comment)


def load_products(conn: sqlalchemy.engine.base.Connection):
    result = []
    for row in conn.execute(
            sqlalchemy.select([schema.PRODUCT_TABLE]).order_by(
                sqlalchemy.asc(schema.PRODUCT_TABLE.c.merchant_product_id))):
        result.append(
            inventory_pb2.Product(
                # TODO(agentydragon): fix to str
                merchant_product_id=int(row['merchant_product_id']),
                merchant=row['merchant'],
                contents=_parse_binary_proto(inventory_pb2.InventoryLine,
                                             row['content']),
                comment=row['comment']))
    return result


def validate_merchant_product_status(
        merchant_product_status: inventory_pb2.MerchantProductStatus) -> None:
    assert merchant_product_status.available_exact >= 0
    assert merchant_product_status.available_lower_bound >= 0


def update_merchant_product_status(
        conn: sqlalchemy.engine.base.Connection,
        merchant_product_status: inventory_pb2.MerchantProductStatus) -> None:
    time = time_util.timestamp_proto_to_datetime(
        merchant_product_status.timestamp)

    validate_merchant_product_status(merchant_product_status)
    result = conn.execute(
        schema.MERCHANT_PRODUCT_STATUS_TABLE.insert(),
        merchant=merchant_product_status.merchant,
        merchant_product_id=merchant_product_status.product_id,
        timestamp=time_util.datetime_to_nanos(time),
        merchant_product_status=merchant_product_status.SerializeToString())
    assert result.rowcount == 1


def load_product_merchant_status_history(
        conn: sqlalchemy.engine.base.Connection, merchant: str,
        merchant_product_id: str) -> List[inventory_pb2.MerchantProductStatus]:
    items = []
    for row in conn.execute(
            sqlalchemy.select(
                [schema.MERCHANT_PRODUCT_STATUS_TABLE],
                sqlalchemy.and_(
                    schema.MERCHANT_PRODUCT_STATUS_TABLE.c.merchant == merchant,
                    schema.MERCHANT_PRODUCT_STATUS_TABLE.c.merchant_product_id
                    == merchant_product_id)).order_by(
                        sqlalchemy.desc(
                            schema.MERCHANT_PRODUCT_STATUS_TABLE.c.timestamp))):
        mps = inventory_pb2.MerchantProductStatus()
        mps.ParseFromString(row['merchant_product_status'])
        mps.timestamp.MergeFrom(
            time_util.timestamp_proto_from_epoch_nanos(row['timestamp']))
        mps.merchant = row['merchant']
        mps.product_id = row['merchant_product_id']
        items.append(mps)
    return items


def load_merchant_product_statuses(
    conn: sqlalchemy.engine.base.Connection
) -> List[inventory_pb2.MerchantProductStatus]:
    rows = conn.execute(
        sqlalchemy.text("""
        WITH
          MerchantProductStatesWithNumber AS (
            SELECT
              merchant,
              merchant_product_id,
              timestamp,
              merchant_product_status,
              ROW_NUMBER() OVER (
                  PARTITION BY merchant, merchant_product_id
                  ORDER BY timestamp DESC)
                  AS row_number
            FROM MerchantProductStatus
          )
        SELECT
          merchant,
          merchant_product_id,
          timestamp,
          merchant_product_status
        FROM MerchantProductStatesWithNumber
        WHERE row_number = 1
        ORDER BY merchant, merchant_product_id ASC
    """))
    items = []
    for row in rows:
        mps = inventory_pb2.MerchantProductStatus()
        mps.ParseFromString(row['merchant_product_status'])
        mps.timestamp.MergeFrom(
            time_util.timestamp_proto_from_epoch_nanos(row['timestamp']))
        mps.merchant = row['merchant']
        mps.product_id = row['merchant_product_id']
        items.append(mps)
    return items


def delete_product(conn: sqlalchemy.engine.base.Connection, merchant,
                   merchant_product_id):
    result = conn.execute(schema.PRODUCT_TABLE.delete().where(
        sqlalchemy.and_(
            schema.PRODUCT_TABLE.c.merchant == merchant,
            schema.PRODUCT_TABLE.c.merchant_product_id == merchant_product_id)))
    assert result.rowcount == 1


def _load_ingredient_type_protos(
    conn: sqlalchemy.engine.base.Connection
) -> List[inventory_pb2.IngredientType]:
    items = []
    for row in conn.execute(sqlalchemy.select([schema.INGREDIENT_TYPE_TABLE])):
        db_ingredient_type = inventory_pb2.DbIngredientType()
        db_ingredient_type.ParseFromString(row['ingredient_type'])

        items.append(
            inventory_pb2.IngredientType(
                comment=db_ingredient_type.comment,
                subtypes=db_ingredient_type.subtypes,
                canonical_unit=db_ingredient_type.canonical_unit,
                id=row['id']))
    return items


def _load_ingredient_matter_rules(
    conn: sqlalchemy.engine.base.Connection
) -> List[inventory_pb2.IngredientMatterRule]:
    items = []
    for row in conn.execute(
            sqlalchemy.select([schema.INGREDIENT_MATTER_RULE_TABLE])):
        items.append(
            inventory_pb2.IngredientMatterRule(
                ingredient_id_regex=row['ingredient_id_regex'],
                matter_type=row['matter_type']))
    return items


def _load_ingredient_names(
    conn: sqlalchemy.engine.base.Connection
) -> List[inventory_pb2.IngredientName]:
    items = []
    for row in conn.execute(sqlalchemy.select([schema.INGREDIENT_NAME_TABLE])):
        name = inventory_pb2.IngredientName()
        name.ParseFromString(row['proto'])
        name.comment = row['comment']
        name.ingredient_id = row['ingredient_id']
        items.append(name)
    return items


def insert_ingredient_matter_rule(
        conn: sqlalchemy.engine.base.Connection,
        rule: inventory_pb2.IngredientMatterRule) -> None:
    result = conn.execute(schema.INGREDIENT_MATTER_RULE_TABLE.insert(),
                          ingredient_id_regex=rule.ingredient_id_regex,
                          matter_type=rule.matter_type)
    assert result.rowcount == 1


def insert_ingredient_name(
        conn: sqlalchemy.engine.base.Connection,
        ingredient_name: inventory_pb2.IngredientName) -> None:
    result = conn.execute(schema.INGREDIENT_NAME_TABLE.insert(),
                          ingredient_id=ingredient_name.ingredient_id,
                          comment=ingredient_name.comment,
                          proto=ingredient_name.SerializeToString())
    assert result.rowcount == 1


def insert_ingredient_type(
        conn: sqlalchemy.engine.base.Connection,
        ingredient_type: inventory_pb2.IngredientType) -> None:
    # TODO: check not duplicated
    result = conn.execute(
        schema.INGREDIENT_TYPE_TABLE.insert(),
        id=ingredient_type.id,
        ingredient_type=inventory_pb2.DbIngredientType(
            comment=ingredient_type.comment,
            subtypes=ingredient_type.subtypes,
            canonical_unit=ingredient_type.canonical_unit).SerializeToString())
    assert result.rowcount == 1


def update_ingredient_type(
        conn: sqlalchemy.engine.base.Connection,
        ingredient_type: inventory_pb2.IngredientType) -> None:
    # TODO: check not duplicated
    result = conn.execute(schema.INGREDIENT_TYPE_TABLE.update().values(
        ingredient_type=inventory_pb2.DbIngredientType(
            comment=ingredient_type.comment,
            subtypes=ingredient_type.subtypes,
            canonical_unit=ingredient_type.canonical_unit).SerializeToString(
            )).where(schema.INGREDIENT_TYPE_TABLE.c.id == ingredient_type.id))
    assert result.rowcount == 1


def load_ingredient_types(
    conn: sqlalchemy.engine.base.Connection,
    extra_names: List[inventory_pb2.IngredientName] = None
) -> ingredient_types.IngredientDatabase:
    types = _load_ingredient_type_protos(conn)
    rules = _load_ingredient_matter_rules(conn)
    names = _load_ingredient_names(conn)
    if extra_names:
        names += extra_names
    return ingredient_types.IngredientDatabase(ingredient_types=types,
                                               ingredient_matter_rules=rules,
                                               ingredient_names=names)
