load("@rules_python//python:python.bzl", "py_binary", "py_library")
load("@rules_proto_grpc//python:defs.bzl", "python_proto_library")
load("@my_deps//:requirements.bzl", "requirement")

proto_library(
    name = "planner_proto",
    srcs = ["planner.proto"],
    deps = [
        "@com_google_protobuf//:empty_proto",
    ],
)

cc_proto_library(
    name = "planner_cc_proto",
    deps = [":planner_proto"],
)

python_proto_library(
    name = "planner_py_proto",
    protos = [":planner_proto"],
    visibility = [":__subpackages__"],
)

py_library(
    name = "planner",
    srcs = ["planner.py"],
    deps = [
        ":planner_py_proto",
        "//data:products",
        "//data:stock",
        requirement("z3-solver"),
    ],
)

py_library(
    name = "translator",
    srcs = ["translator.py"],
    srcs_version = "PY3",
    visibility = [
        "//check:__pkg__",
    ],
    deps = [
        ":approximate_gcd",
        ":planner_py_proto",
        "//:diet_mapping_py_proto",
        "//:inventory_py_proto",
        "//:unit",
        "//data:conversions",
        "//data:products",
        "//data:recipes",
        "//data:stock",
        requirement("absl-py"),
    ],
)

py_library(
    name = "cc_ortools_backend",
    srcs = ["cc_ortools_backend.py"],
    data = [":planner_cc"],
    deps = [
        ":planner_py_proto",
        "@rules_python//python/runfiles",
        requirement("protobuf"),
    ],
)

py_library(
    name = "wrapper",
    srcs = ["wrapper.py"],
    visibility = ["//visibility:public"],
    deps = [
        ":planner",
        ":planner_py_proto",
        ":translator",
        ":validate",
        "//:diet_mapping_py_proto",
        "//:error_reporter",
        "//:inventory_py_proto",
        "//:recipe_amount",
        "//:unit_amount",
        "//data:conversions",
        "//data:diet_mappings",
        "//data:ingredient_types",
        "//data:recipes",
        "//data:stock",
        "//data:substitutions",
        "//db",
        "@rules_python//python/runfiles",
        requirement("absl-py"),
        requirement("protobuf"),
    ],
)

py_library(
    name = "validate",
    srcs = ["validate.py"],
)

py_test(
    name = "wrapper_test",
    srcs = ["wrapper_test.py"],
    deps = [
        ":cc_ortools_backend",
        ":translator",
        ":wrapper",
        "//:diet_mapping_py_proto",
        "//:inventory_py_proto",
        "//data:ingredient_types",
        "//data:stock",
        "//plan/minizinc:minizinc_planner",
        "@rules_python//python/runfiles",
        requirement("freezegun"),
        requirement("proto_matcher"),
        requirement("PyHamcrest"),
        requirement("python-dateutil"),
    ],
)

py_library(
    name = "approximate_gcd",
    srcs = ["approximate_gcd.py"],
    deps = [
        requirement("absl-py"),
    ],
)

py_test(
    name = "approximate_gcd_test",
    srcs = ["approximate_gcd_test.py"],
    deps = [
        ":approximate_gcd",
        requirement("absl-py"),
        requirement("six"),
    ],
)

py_test(
    name = "translator_test",
    srcs = ["translator_test.py"],
    deps = [
        ":translator",
        "//:error_reporter",
        "//:inventory_py_proto",
        "//data:ingredient_types",
        "//data:stock",
        requirement("proto_matcher"),
        requirement("PyHamcrest"),
    ],
)

cc_binary(
    name = "planner_cc",
    srcs = ["planner.cc"],
    deps = [
        ":planner_cc_proto",
        "@com_google_absl//absl/strings",
        "@ortools//ortools/base",
        "@ortools//ortools/sat:cp_model",
        "@ortools//ortools/sat:model",
    ],
)
