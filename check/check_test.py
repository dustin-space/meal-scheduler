from absl.testing import absltest
from google.protobuf import text_format
from meal_scheduler import error_reporter as error_reporter_lib
from meal_scheduler.check import check_lib
from meal_scheduler.data import ingredient_types
from meal_scheduler.data import recipes as recipes_lib
from meal_scheduler.data import stock as stock_lib
from meal_scheduler.inventory_py_proto_pb import inventory_pb2
from meal_scheduler.unit_py_proto_pb import unit_pb2
from rules_python.python.runfiles import runfiles


def _parse_stock_item(textproto):
    return text_format.Parse(textproto, inventory_pb2.StockItem())


def _parse_stored_recipe(textproto):
    return text_format.Parse(textproto, inventory_pb2.StoredRecipe())


class CheckTest(absltest.TestCase):

    def _load_ingredient_database(self):
        return

    def test_validate_liquid_prohibits_piece(self):
        error_reporter = error_reporter_lib.ErrorReporter()
        ingredient_database = ingredient_types.IngredientDatabase(
            ingredient_types=[
                inventory_pb2.IngredientType(id="/ingredient/oil/olive")
            ],
            ingredient_matter_rules=[
                inventory_pb2.IngredientMatterRule(
                    ingredient_id_regex="/ingredient/oil/.*",
                    matter_type=inventory_pb2.LIQUID)
            ],
            ingredient_names=[])
        check_lib.validate_ingredient_id_unit("/ingredient/oil/olive",
                                              unit_pb2.PIECE,
                                              ingredient_database,
                                              error_reporter)
        self.assertIn(
            "/ingredient/oil/olive is a LIQUID, can't have a PIECE of it",
            error_reporter.errors)

    def test_finds_duplicated_name(self):
        names = text_format.Parse(
            """
name { ingredient_id: "/salt" name: "salt" }
name { ingredient_id: "/pepper" name: "salt" }
""", inventory_pb2.IngredientNames())
        types = text_format.Parse(
            """
ingredient { id: "/salt" }
ingredient { id: "/pepper" }
""", inventory_pb2.IngredientTypes())
        ingredient_database = ingredient_types.IngredientDatabase(
            ingredient_types=list(types.ingredient),
            ingredient_matter_rules=[],
            ingredient_names=list(names.name))
        error_reporter = error_reporter_lib.ErrorReporter()
        check_lib.check_ingredients(ingredient_database, error_reporter)
        self.assertIn("Duplicated ingredient names: ['salt']",
                      error_reporter.errors)

    def test_finds_name_referencing_missing_ingredient(self):
        names = text_format.Parse(
            """
name { ingredient_id: "/salt" name: "salt" }
""", inventory_pb2.IngredientNames())
        ingredient_database = ingredient_types.IngredientDatabase(
            ingredient_types=[],
            ingredient_matter_rules=[],
            ingredient_names=names.name)
        error_reporter = error_reporter_lib.ErrorReporter()
        check_lib.check_ingredients(ingredient_database, error_reporter)
        self.assertIn("name declared for unknown ingredient /salt",
                      error_reporter.errors)

    def test_finds_cluster_without_canonical_unit(self):
        types = text_format.Parse(
            """
ingredient { id: "cluster"  subtypes: "type" }
ingredient { id: "type" }
""", inventory_pb2.IngredientTypes())
        ingredient_database = ingredient_types.IngredientDatabase(
            ingredient_types=list(types.ingredient),
            ingredient_matter_rules=[],
            ingredient_names=[])
        error_reporter = error_reporter_lib.ErrorReporter()

        recipe = _parse_stored_recipe(r"""
    recipe {
      ingredients {
        ingredient_id: "cluster"
        amount { unit_amount { unit: GRAM amount: 100 } }
      }
      serves { min: 1 max: 1 }
    }
    """)
        check_lib.check_common_units(
            ingredient_database,
            recipes=[recipes_lib.RecipeWrapper(recipe)],
            products=[],
            stock=stock_lib.StockDatabase(stock_items=[
                _parse_stock_item(r"""
ingredient_id: "type" amount { exact { unit: GRAM amount: 100 } }
"""),
            ]),
            convs=inventory_pb2.Conversions(),
            error_reporter=error_reporter)
        self.assertIn("component cluster, type does not set a canonical unit",
                      error_reporter.errors)

    def test_finds_weird_ingredient_id(self):
        ingredient_database = ingredient_types.IngredientDatabase(
            ingredient_types=[inventory_pb2.IngredientType(id="$&#@!()%")],
            ingredient_matter_rules=[],
            ingredient_names=[])
        error_reporter = error_reporter_lib.ErrorReporter()
        check_lib.check_ingredients(ingredient_database, error_reporter)
        self.assertIn('Invalid ingredient ID format: $&#@!()%',
                      error_reporter.errors)

    def test_finds_repeated_merchant_product_id(self):
        # TODO(agentydragon): this should work across merchants only
        types = inventory_pb2.IngredientTypes()
        ingredient_database = ingredient_types.IngredientDatabase(
            ingredient_types=[],
            ingredient_matter_rules=[],
            ingredient_names=[])
        error_reporter = error_reporter_lib.ErrorReporter()
        products = text_format.Parse(
            """
product { merchant_product_id: 12345 }
product { merchant_product_id: 12345 }""", inventory_pb2.Products())
        check_lib.check_products(ingredient_database, list(products.product),
                                 error_reporter)
        self.assertIn('LeShop id 12345 is in multiple products!',
                      error_reporter.errors)

    # TODO(agentydragon): having no common unit is OK if it's an ingredient
    # we have enough of.


if __name__ == '__main__':
    absltest.main()
