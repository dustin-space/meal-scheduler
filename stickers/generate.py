import collections
import io
import math
import os.path
import re

import qrcode
import qrcode.image.svg
import svgwrite
from absl import logging
from google.protobuf import text_format
from meal_scheduler import recipe_amount, time_util
from meal_scheduler.data import recipes as recipes_lib
from meal_scheduler.data import stock
from meal_scheduler.inventory_py_proto_pb import inventory_pb2


class RectCollectImage(qrcode.image.base.BaseImage):

    def drawrect(self, row, col):
        self._img.append((row, col))

    def new_image(self, **kwargs):
        return []


# Sticker paper has A4 size (297 * 210 mm).
# The printable area has top, bottom, left and right margins.

ROWS = 8
COLUMNS = 3
A4_WIDTH_MM = 210
A4_HEIGHT_MM = 297

A4_IMAGEABLE_AREA_REGEX = re.compile(
    r'\*ImageableArea A4: "([0-9.]+) ([0-9.]+) ([0-9.]+) ([0-9.]+)"')
A4_PAPER_DIMENSION_REGEX = re.compile(
    r'\*PaperDimension A4: "([0-9.]+) ([0-9.]+)"')

#CUPS_PPD_PATH = '/etc/cups/ppd/Brother_HL_L2350DW_series.ppd'
CUPS_PPD_PATH = '/etc/cups/ppd/HP_LaserJet_M15w_D9EDFF_.ppd'


def load_printable_area(path):
    # TODO(agentydragon): add a test
    # Find '*ImageableArea A4: "... ... ... ..."'
    # Find '*PaperDimension A4: "... ..."'
    try:
        with open(path, 'r') as f:
            printable_left_cups_units = None
            width_cups_units = None
            for line in f:
                line = line.strip()

                match = re.fullmatch(A4_IMAGEABLE_AREA_REGEX, line)
                # YAPF does not support assignment expressions :(
                if match:
                    printable_left_cups_units = float(match[1])
                    printable_top_cups_units = float(match[2])
                    printable_right_cups_units = float(match[3])
                    printable_bottom_cups_units = float(match[4])
                match = re.fullmatch(A4_PAPER_DIMENSION_REGEX, line)
                # YAPF does not support assignment expressions :(
                if match:
                    width_cups_units = float(match[1])
                    height_cups_units = float(match[2])

            if not printable_left_cups_units:
                raise Exception(
                    "could not parse A4 CUPS unit printable area from PPD")
            if not width_cups_units:
                raise Exception("could not parse A4 CUPS unit size from PPD")

            printable_left_mm = printable_left_cups_units / width_cups_units * A4_WIDTH_MM
            printable_top_mm = printable_top_cups_units / height_cups_units * A4_HEIGHT_MM
            printable_right_mm = printable_right_cups_units / width_cups_units * A4_WIDTH_MM
            printable_bottom_mm = printable_bottom_cups_units / \
                height_cups_units * A4_HEIGHT_MM
        return printable_left_mm, printable_top_mm, printable_right_mm, printable_bottom_mm
    except PermissionError:
        return None


def draw_sticker(dwg, lines, sticker_left_printable_mm: float,
                 sticker_top_printable_mm: float,
                 sticker_right_printable_mm: float,
                 sticker_bottom_printable_mm: float):
    sticker_width_mm = sticker_right_printable_mm - sticker_left_printable_mm
    sticker_height_mm = sticker_bottom_printable_mm - sticker_top_printable_mm

    dwg.add(
        dwg.rect(
            insert=(f'{sticker_left_printable_mm}mm',
                    f'{sticker_top_printable_mm}mm'),
            size=(f'{sticker_width_mm}mm', f'{sticker_height_mm}mm'),
            fill='white'  # To stroke the rects: , stroke='black'
        ))

    sticker_padding_left_mm = 3
    sticker_padding_top_mm = 3

    text_element = dwg.text(
        '',
        insert=(f'{sticker_left_printable_mm + sticker_padding_left_mm}mm',
                f'{sticker_top_printable_mm + sticker_padding_top_mm}mm'),
        stroke='black')

    line_height = '3.5mm'
    font_size = '3mm'

    for i, line in enumerate(lines):
        text_element.add(
            dwg.tspan(
                line,
                x=[f'{sticker_left_printable_mm + sticker_padding_left_mm}mm'],
                dy=[line_height],
                font_family="Arial",
                style="font-size: " + font_size,
                font_weight="lighter" if i > 0 else "bold",
                letter_spacing="1.3"))

    dwg.add(text_element)

    ## Add qrcode (text, experimental)
    #if x == 2 and y == 7:
    #    data = "Some text that you want to store in the qrcode"
    #    rects = qrcode.make(data, image_factory=RectCollectImage)

    #    rows = max(row for row, _ in rects._img) + 1
    #    cols = max(col for _, col in rects._img) + 1

    #    group = dwg.svg(
    #        x=x_middle, y=y_x, viewBox=f"0 0 {cols} {rows}",
    #        width="1cm", height="1cm"
    #    )
    #    for row, col in rects._img:
    #        group.add(dwg.rect(
    #            insert=(col, row), size=(1, 1), fill='black'
    #        ))
    #    dwg.add(group)


def create_stickers_drawing(stickers_lines):
    # TODO(agentydragon): This is ugly, let's refactor.
    if os.path.isfile(CUPS_PPD_PATH):
        logging.info("loading printable area from PPD")
        dimensions = load_printable_area(CUPS_PPD_PATH)
        if dimensions:
            printable_left_mm, printable_top_mm, printable_right_mm, printable_bottom_mm = dimensions
        else:
            logging.info("falling back to Rai's snapshot")
            printable_left_mm, printable_top_mm, printable_right_mm, printable_bottom_mm = 4.32, 4.32, 205.68, 292.68
    else:
        logging.info("falling back to Rai's snapshot")
        printable_left_mm, printable_top_mm, printable_right_mm, printable_bottom_mm = 4.32, 4.32, 205.68, 292.68
    logging.info("printable area: %r %r %r %r", printable_left_mm,
                 printable_top_mm, printable_right_mm, printable_bottom_mm)
    printable_width_mm = printable_right_mm - printable_left_mm
    printable_height_mm = printable_bottom_mm - printable_top_mm

    dwg = svgwrite.Drawing(
        # A4 paper size
        size=(f'{printable_width_mm}mm', f'{printable_height_mm}mm'))

    def a4_mm_top_left_to_printable_top_left(a4_x, a4_y):
        x = min(max(a4_x - printable_left_mm, 0),
                printable_right_mm - printable_left_mm)
        y = min(max(a4_y - printable_top_mm, 0),
                printable_bottom_mm - printable_top_mm)
        return (x, y)

    for row_index in range(ROWS):
        for column_index in range(COLUMNS):
            sticker_top_a4_mm = (A4_HEIGHT_MM / ROWS) * row_index
            sticker_left_a4_mm = (A4_WIDTH_MM / COLUMNS) * column_index
            sticker_bottom_a4_mm = (A4_HEIGHT_MM / ROWS) * (row_index + 1)
            sticker_right_a4_mm = (A4_WIDTH_MM / COLUMNS) * (column_index + 1)
            sticker_left_printable_mm, sticker_top_printable_mm = a4_mm_top_left_to_printable_top_left(
                sticker_left_a4_mm, sticker_top_a4_mm)
            sticker_right_printable_mm, sticker_bottom_printable_mm = a4_mm_top_left_to_printable_top_left(
                sticker_right_a4_mm, sticker_bottom_a4_mm)

            sticker_index = (row_index * COLUMNS) + column_index

            if sticker_index < len(stickers_lines):
                draw_sticker(dwg, stickers_lines[sticker_index],
                             sticker_left_printable_mm,
                             sticker_top_printable_mm,
                             sticker_right_printable_mm,
                             sticker_bottom_printable_mm)

    return dwg


def generate_stickers(plan: inventory_pb2.Plan, s: stock.StockDatabase,
                      all_recipes):
    ingredient_stickers = collections.defaultdict(list)

    for planned_recipe in plan.planned_recipe:
        for commitment in planned_recipe.commitment:
            if commitment.ingredient_id in s.ingredient_ids_with_enough:
                # Sticker not needed for stocked items
                continue
            # TODO(agentydragon): nice human-readable ingredient ID
            ingredient_stickers[commitment.ingredient_id].append(
                (recipe_amount.amount_to_str(commitment.amount),
                 recipes_lib.find_recipe_by_id(all_recipes,
                                               planned_recipe.recipe_id).title,
                 time_util.date_proto_to_iso8601(planned_recipe.date)))

    sticker_lines = []
    for ingredient, uses in ingredient_stickers.items():
        lines = [ingredient.replace('/ingredient', '')]
        for amount, title, date in uses:
            lines.append(f'{date}: {amount}')
            lines.append(title)
        sticker_lines.append(lines)

    STICKERS_PER_PAGE = ROWS * COLUMNS

    logging.info("stickers needed: %d", len(sticker_lines))

    file_contents = []
    for page in range(math.ceil(len(sticker_lines) / STICKERS_PER_PAGE)):
        logging.info("page %d", page)

        page_stickers = sticker_lines[STICKERS_PER_PAGE *
                                      page:STICKERS_PER_PAGE * (page + 1)]
        with io.StringIO() as string_io:
            create_stickers_drawing(page_stickers).write(string_io, pretty=True)
            file_contents.append(string_io.getvalue())
    return file_contents
