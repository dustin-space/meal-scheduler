from alembic import op
from sqlalchemy import Column, Integer, LargeBinary, String, UniqueConstraint

revision = '2031d92c9f4a'
down_revision = '4078f498e56f'
branch_labels = None
depends_on = None


def upgrade():
    op.create_table('IngredientType', Column('id', String, nullable=False),
                    Column('ingredient_type', LargeBinary, nullable=False),
                    UniqueConstraint('id'))

    op.create_table('IngredientMatterRule',
                    Column('ingredient_id_regex', String, nullable=False),
                    Column('matter_type', Integer, nullable=False))

    op.create_table('IngredientName',
                    Column('ingredient_id', String, nullable=False),
                    Column('comment', String),
                    Column('proto', LargeBinary, nullable=False))


def downgrade():
    op.drop_table('IngredientType')
    op.drop_table('IngredientMatterRule')
