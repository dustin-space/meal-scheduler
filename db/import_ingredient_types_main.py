"""
bazel run //db:import_ingredient_types_main -- \
  --alsologtostderr \
  --sqlite_db=$(pwd)/db.sqlite \
  --ingredient_types=$(pwd)/ingredient_types.textproto
"""

from absl import app, flags
from meal_scheduler.db import db as db_lib
from meal_scheduler.db import import_ingredient_types_lib

_SQLITE_DB = flags.DEFINE_string("sqlite_db", None, "path to sqlite db")
_INGREDIENT_TYPES = flags.DEFINE_string(
    "ingredient_types", None, "textproto to insert ingredient types from")
_INGREDIENT_NAMES = flags.DEFINE_string(
    "ingredient_names", None, "textproto to insert ingredient names from")


def main(_):
    db = db_lib.open_db(_SQLITE_DB.value)
    if _INGREDIENT_TYPES.value:
        import_ingredient_types_lib.import_ingredient_types_from_file(
            db=db, path=_INGREDIENT_TYPES.value)
    if _INGREDIENT_NAMES.value:
        import_ingredient_types_lib.import_ingredient_names_from_file(
            db=db, path=_INGREDIENT_NAMES.value)
    db.close()


if __name__ == '__main__':
    flags.mark_flags_as_required([_SQLITE_DB.name, _INGREDIENT_TYPES.name])
    app.run(main)
