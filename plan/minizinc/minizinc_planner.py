import glob
import json
import itertools
import os
import os.path
import pathlib
import re
import subprocess
import tempfile
import uuid
from typing import Optional

from absl import logging
from meal_scheduler.plan.planner_py_proto_pb.plan import planner_pb2
from rules_python.python.runfiles import runfiles as runfiles_lib


def minizinc_2d_array(x, comments=None):
    x = list(x)
    if not x:
        # TODO(agenydragon): can this be joined with the other case?
        return '[]'
    s = "[|"
    if comments is None:
        comments = itertools.repeat(None)
    for i, (item, comment) in enumerate(zip(x, comments)):
        if comment:
            s += '% ' + comment + '\n'
        s += ', '.join(str(subitem) for subitem in item)
        if i < len(x) - 1:
            s += ' |\n'
        else:
            s += ' |]'
    return s


def make_recipe_ingredients_comment(recipe):
    ingredients = ', '.join(
        f'{item.amount}x I{item.ingredient}' for item in recipe.item)
    return f"{recipe.name}: {ingredients}"


def minizinc_1d_array(x):
    return '[' + ', '.join(str(item) for item in x) + ']'


def sum_ingredient_amounts(amounts, num_ingredients):
    ingredient_use = [0 for _ in range(num_ingredients)]
    for item in amounts:
        ingredient_use[item.ingredient] += item.amount
    return ingredient_use


def get_ortools_backend_config(r):
    backend_path = r.Rlocation('ortools/ortools/flatzinc/fz')
    mznlib_path = r.Rlocation('ortools/ortools/flatzinc/mznlib')
    logging.debug(f'OR-Tools: {backend_path=} {mznlib_path=}')

    return {
        'id': 'org.ortools.ortools',
        'name': 'OR Tools',
        "description": "Or Tools FlatZinc executable",
        "version": "8.0/stable",
        "mznlib": mznlib_path,
        "executable": backend_path,
        "tags": ["cp", "int", "lcg", "or-tools"],
        "stdFlags": ["-a", "-n", "-p", "-f", "-r", "-v", "-l", "-s"],
        "supportsMzn": False,
        "supportsFzn": True,
        "needsSolns2Out": True,
        "needsMznExecutable": False,
        "needsStdlibDir": False,
        "isGUIApplication": False
    }


def get_gecode_backend_config(r):
    backend_path = r.Rlocation(
        'meal_scheduler/plan/minizinc/gecode/bin/fzn-gecode')
    representative_path = r.Rlocation('gecode/gecode/flatzinc/mznlib/count.mzn')
    assert representative_path
    mznlib_path = os.path.dirname(representative_path)
    logging.debug(f'Gecode: {backend_path=} {mznlib_path=}')
    # TODO(agentydragon): cannot get runfiles dir path :(
    assert backend_path
    assert mznlib_path

    return {
        "id": "org.gecode.gecode",
        "name": "Gecode",
        "description": "Gecode FlatZinc executable",
        "version": "6.2.0",
        "mznlib": mznlib_path,
        "executable": backend_path,
        "tags": ["cp", "int", "float", "set", "restart"],
        "stdFlags": ["-a", "-f", "-n", "-p", "-r", "-s", "-t"],
        "supportsMzn": False,
        "supportsFzn": True,
        "needsSolns2Out": True,
        "needsMznExecutable": False,
        "needsStdlibDir": False,
        "isGUIApplication": False
    }


def minizinc_prefixed_enum_set(prefix, num):
    return '{ ' + ', '.join(f'{prefix}{i}' for i in range(num)) + ' }'


def _recipe_index_to_enum(recipe_index: int, plan_input: planner_pb2.Input):
    return f'R{recipe_index}_{plan_input.recipe[recipe_index].name}'


def _recipe_index_from_enum(enum: str) -> int:
    return int(re.fullmatch(r'^R(\d+)_.*', enum)[1])


def plan(plan_input: planner_pb2.Input,
         tmp_dir: Optional[str] = None) -> planner_pb2.Output:
    r = runfiles_lib.Create()
    MODEL_RUNFILES_PATH = r.Rlocation(
        'meal_scheduler/plan/minizinc/simple-planning.mzn')

    MINIZINC_FRONTEND_PATH = r.Rlocation(
        'meal_scheduler/plan/minizinc/minizinc_frontend/bin/minizinc')
    # TODO(agentydragon): cannot get runfiles dir path :(
    #MINIZINC_STDLIB_PATH = r.Rlocation('libminizinc/share/minizinc')
    MINIZINC_REPRESENTATIVE_PATH = r.Rlocation(
        'libminizinc/share/minizinc/std/write.mzn')
    assert MINIZINC_REPRESENTATIVE_PATH, "Minizinc stdlib representative not found"
    MINIZINC_STDLIB_PATH = os.path.dirname(
        os.path.dirname(MINIZINC_REPRESENTATIVE_PATH))

    assert MINIZINC_FRONTEND_PATH, "Minizinc frontend not found"
    assert MINIZINC_STDLIB_PATH, "Minizinc stdlib not found"

    num_ingredients = len(plan_input.ingredient)

    tmp_dir = pathlib.Path(tmp_dir or '/tmp')

    tmpdir = tmp_dir / ('minizinc-planner-%s' % uuid.uuid4())
    os.makedirs(tmpdir)
    json_config_path = tmpdir / 'json-config.msc'
    data_path = tmpdir / 'input.dzn'
    fzn_path = tmpdir / 'intermediate.fzn'

    logging.info("Minizinc planner working path: %s", tmpdir)

    with open(json_config_path, 'w') as f:
        json.dump(get_gecode_backend_config(r), f)
        # json.dump(get_ortools_backend_config(r), f)

    num_timesteps = len(plan_input.meal_request)
    with open(data_path, "w") as f:
        f.write(f'timesteps = {num_timesteps};\n')
        f.write(
            f'max_of_recipes_constraints = {len(plan_input.max_of_recipes)};\n')
        # TODO(agentydragon): human-readable names for products, diet bits.
        f.write("Recipes = { " + ', '.join(
            _recipe_index_to_enum(i, plan_input)
            for i in range(len(plan_input.recipe))) + ' };\n')
        f.write("Ingredients = { " + ', '.join(
            f'I{ingredient.name}' for ingredient in plan_input.ingredient) +
                " };\n")
        f.write("Products = { " +
                ', '.join(f'P{i}_{product.name}'
                          for i, product in enumerate(plan_input.product)) +
                ' };\n')
        f.write(
            "DietBits = " +
            minizinc_prefixed_enum_set('DietBit', len(plan_input.diet_bit)) +
            ';\n')

        f.write("recipe_input_per_portion = " + minizinc_2d_array(
            (sum_ingredient_amounts(recipe.item, num_ingredients)
             for recipe in plan_input.recipe),
            comments=(make_recipe_ingredients_comment(recipe)
                      for recipe in plan_input.recipe)) + ';\n')
        f.write('product_contents = ' + minizinc_2d_array(
            sum_ingredient_amounts(product.content, num_ingredients)
            for product in plan_input.product) + ';\n')

        # TODO(agentydragon): move 100 to constant
        f.write('product_max_buyable = array1d(Products, ' + minizinc_1d_array(
            [(product.max_buyable if product.HasField('max_buyable') else 100)
             for product in plan_input.product]) + ');\n')

        f.write('initial_stock = array1d(Ingredients, ' + minizinc_1d_array(
            [ingredient.stock for ingredient in plan_input.ingredient]) +
                ');\n')
        f.write('allowed_to_obtain_externally = array1d(Ingredients, ' +
                minizinc_1d_array([
                    'false' if ingredient.banned_external else 'true'
                    for ingredient in plan_input.ingredient
                ]) + ');\n')
        f.write('required_to_use = array1d(Ingredients, ' + minizinc_1d_array([
            'true' if ingredient.is_required else 'false'
            for ingredient in plan_input.ingredient
        ]) + ');\n')

        f.write(f'portions_cooked = array1d(TIMESTEP, ' +
                minizinc_1d_array(meal_request.portion_count
                                  for meal_request in plan_input.meal_request) +
                ');\n')
        diet_bit_is_blocked = [[
            ('true' if j in meal_request.blocked_diet_bits else 'false')
            for j in range(len(plan_input.diet_bit))
        ]
                               for meal_request in plan_input.meal_request]
        f.write(f'diet_bit_is_blocked = array2d(TIMESTEP, DietBits, ' +
                minizinc_2d_array(diet_bit_is_blocked) + ');\n')
        f.write(
            f'max_obtain_externally = {plan_input.max_ingredients_obtained_externally};\n'
        )
        f.write('max_of_recipes_count = array1d(MAX_OF_RECIPES_ITEMS, ' +
                minizinc_1d_array(
                    max_of_recipes.max_meals
                    for max_of_recipes in plan_input.max_of_recipes) + ');\n')
        f.write(f'max_of_recipes_recipes = array1d(MAX_OF_RECIPES_ITEMS, [' +
                ', '.join(
                    ('{' + ', '.join(
                        _recipe_index_to_enum(recipe, plan_input)
                        for recipe in max_of_recipes.recipe) + '}')
                    for max_of_recipes in plan_input.max_of_recipes) + ']);\n')
        f.write('type_subtypes = array1d(Ingredients, [' + ', '.join(
            ('{' + ', '.join(f'I{plan_input.ingredient[j].name}'
                             for j in superingredient.subtypes) + '}')
            for superingredient in plan_input.ingredient) + ']);\n')
        f.write('banned_recipes = {' + ', '.join(
            _recipe_index_to_enum(recipe, plan_input)
            for recipe in plan_input.banned_recipe) + '};\n')
        ingredient_has_diet_bit = [[
            ('true' if j in ingredient.ingredient_bits else 'false')
            for j in range(len(plan_input.diet_bit))
        ]
                                   for ingredient in plan_input.ingredient]
        f.write('ingredient_has_diet_bit = array2d(Ingredients, DietBits, ' +
                minizinc_2d_array(ingredient_has_diet_bit) + ');')

    if plan_input.timeout_seconds > 0:
        timeout_seconds = plan_input.timeout_seconds
    else:
        timeout_seconds = 60

    args = [
        MINIZINC_FRONTEND_PATH,
        MODEL_RUNFILES_PATH,
        data_path,
        '--output-mode',
        'json',
        '--solver',
        str(json_config_path),
        '--output-fzn-to-file',
        str(fzn_path),
        '--stdlib-dir',
        MINIZINC_STDLIB_PATH,
        # Solve for up to 1 minute.
        '--time-limit',
        '%d' % (timeout_seconds * 1000),
        # Print intermediate solutions of increasing quality.
        '--all-solutions'
    ]

    logging.info("running %s", args)

    with subprocess.Popen(args, stdout=subprocess.PIPE,
                          stderr=subprocess.PIPE) as proc:
        collected_json = ""
        model = None
        solution_number = 0
        for line in proc.stdout:
            line = line.decode('utf-8').rstrip()
            if line == '----------':
                # end of solution
                # TODO
                model = json.loads(collected_json)
                with open(tmpdir / ('solution%d.json' % solution_number),
                          'w') as f:
                    json.dump(model, f, indent=2)
                solution_number += 1
                collected_json = ""
                logging.info("end of solution with %d bought products",
                             model["total_products_bought"])
            elif line == '==========':
                # end of output?
                # TODO
                logging.info("end of output")
            elif line == '=====UNSATISFIABLE=====':
                logging.info("output: UNSATISFIABLE")
                raise Exception("not satisfiable")
            else:
                collected_json += line
        if collected_json != "":
            logging.info("collected some extra json! %s", collected_json)
        logging.info("end of stdout")
        logging.info("stderr:")
        for line in proc.stderr:
            logging.info("%s", line)
        logging.info("end of stderr; waiting for term")
        proc.wait()
        if proc.returncode < 0:
            raise Exception("terminated with signal %d" % -proc.returncode)
        logging.info("return code: %s", proc.returncode)

    if not model:
        raise Exception("no model found!")

    plan = planner_pb2.Plan(
        ingredient=[
            planner_pb2.PlanIngredient(
                total_use=sum(
                    # TODO(agentydragon): extract directly from a model variable
                    model["used_directly"][timestep][i] +
                    sum(model["generalization_to_from"][timestep][supertype][i]
                        for supertype in range(num_ingredients))
                    for timestep in range(num_timesteps)),
                obtain_externally=model["obtain_externally"][i])
            for i in range(num_ingredients)
        ],
        products=model["products_bought_overall"],
        meal=[
            planner_pb2.PlanMeal(
                recipe=_recipe_index_from_enum(
                    model["recipe_cooked"][timestep]["e"]),
                portions=meal_request.portion_count,
                ingredient_use=[
                    # Show each ingredient by:
                    #   - how it was used directly
                    #   - how it was used generalized to any supertype
                    model["used_directly"][timestep][i] +
                    sum(model["generalization_to_from"][timestep][supertype][i]
                        for supertype in range(num_ingredients))
                    for i in range(num_ingredients)
                ])
            for timestep, meal_request in enumerate(plan_input.meal_request)
        ])

    return planner_pb2.Output(plan=plan)
