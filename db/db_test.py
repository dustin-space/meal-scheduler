import random

import hamcrest
import proto_matcher
from absl.testing import absltest
from google.protobuf import text_format
from meal_scheduler import unit as unit_lib
from meal_scheduler.db import db as db_lib
from meal_scheduler.db import init as init_lib
from meal_scheduler.inventory_py_proto_pb import inventory_pb2
from meal_scheduler.unit_py_proto_pb import unit_pb2


class StockItemsTest(absltest.TestCase):

    def setUp(self):
        self._db = db_lib.open_db(":memory:")
        init_lib.init_db(self._db)

    def tearDown(self):
        self._db.close()

    def test_single_stock_item_with_timestamp(self):
        stock_item = text_format.Parse(
            r"""
        ingredient_id: "/ingredient/salt"
        amount { exact { unit: GRAM amount: 100 } }
        timestamp { seconds: 12345 }
        """, inventory_pb2.StockItem())
        db_lib.update_stock(self._db, stock_item)
        stock = db_lib.load_stock(self._db)
        self.assertEqual(len(stock.stock), 1)
        hamcrest.assert_that(
            stock.stock,
            hamcrest.contains(proto_matcher.equals_proto(stock_item)))

    def test_newer_entry_returned(self):
        """Tests that only the latest entry for any ingredient is returned."""
        entries = []
        for amount, seconds in ((100.0, 11111), (50.0, 22222), (75.0, 33333)):
            entry = text_format.Parse(
                r"""
            ingredient_id: "/ingredient/salt"
            amount { exact { unit: GRAM amount: 100 } }
            """, inventory_pb2.StockItem())
            entry.amount.exact.amount = amount
            entry.timestamp.seconds = seconds
            entries.append(entry)
        last_entry = entries[-1]
        random.shuffle(entries)

        for entry in entries:
            db_lib.update_stock(self._db, entry)

        stock = db_lib.load_stock(self._db)
        hamcrest.assert_that(
            stock.stock,
            hamcrest.contains(proto_matcher.equals_proto(last_entry)))


class MerchantProductStatesTest(absltest.TestCase):

    def setUp(self):
        self._db = db_lib.open_db(":memory:")
        init_lib.init_db(self._db)

    def tearDown(self):
        self._db.close()

    def test_empty(self):
        self.assertEmpty(db_lib.load_merchant_product_statuses(self._db))

    def test_single_merchant_product_status(self):
        mps = text_format.Parse(
            r"""
            merchant: "leshop"
            product_id: "123545"
            available_exact: 10
            price_rappen: 540
            timestamp { seconds: 12345 }
            """, inventory_pb2.MerchantProductStatus())
        db_lib.update_merchant_product_status(self._db, mps)
        states = db_lib.load_merchant_product_statuses(self._db)
        self.assertEqual(len(states), 1)
        hamcrest.assert_that(states,
                             hamcrest.contains(proto_matcher.equals_proto(mps)))

    def test_newer_entry_returned(self):
        """Tests that only the latest entry for any product is returned."""
        entries = []
        for amount, seconds in ((100, 11111), (50, 22222), (75, 33333)):
            mps = text_format.Parse(
                r"""
            merchant: "leshop"
            product_id: "12345"
            price_rappen: 500
            """, inventory_pb2.MerchantProductStatus())
            mps.available_exact = amount
            mps.timestamp.seconds = seconds
            entries.append(mps)
        last_entry = entries[-1]
        random.shuffle(entries)

        for entry in entries:
            db_lib.update_merchant_product_status(self._db, entry)

        states = db_lib.load_merchant_product_statuses(self._db)
        hamcrest.assert_that(
            states, hamcrest.contains(proto_matcher.equals_proto(last_entry)))


class PlansTest(absltest.TestCase):

    def setUp(self):
        self._db = db_lib.open_db(":memory:")
        init_lib.init_db(self._db)

    def tearDown(self):
        self._db.close()

    def test_store_retrieve_plan(self):
        plan_request = inventory_pb2.PlanRequest(max_added_ingredients=12345)
        plan = inventory_pb2.Plan(total_ingredient_use=[
            inventory_pb2.PlanIngredientUse(ingredient_id="/ingredient/salt")
        ])
        plan_id = db_lib.insert_plan(self._db, plan, plan_request)

        stored_plan = db_lib.get_plan(self._db, plan_id)
        hamcrest.assert_that(stored_plan.plan, proto_matcher.equals_proto(plan))
        hamcrest.assert_that(stored_plan.plan_request,
                             proto_matcher.equals_proto(plan_request))
        # TODO(agentydragon): test timestamp

    def test_update_plan_comment(self):
        plan_id = db_lib.insert_plan(self._db, inventory_pb2.Plan(),
                                     inventory_pb2.PlanRequest())
        stored_plan = db_lib.get_plan(self._db, plan_id)
        self.assertEqual(stored_plan.comment, '')

        db_lib.update_plan_comment(self._db, plan_id, 'hello world')
        stored_plan = db_lib.get_plan(self._db, plan_id)
        self.assertEqual(stored_plan.comment, 'hello world')

    # TODO(agentydragon): test for unknown ingredient


class ProductsTest(absltest.TestCase):

    def setUp(self):
        self._db = db_lib.open_db(":memory:")
        init_lib.init_db(self._db)

    def tearDown(self):
        self._db.close()

    def test_store_retrieve_product(self):
        product = inventory_pb2.Product(
            merchant="testmerchant",
            merchant_product_id=12345,
            contents=inventory_pb2.InventoryLine(
                ingredient_id="/ingredient/test",
                amount=inventory_pb2.ProductUnitAmount(unit=unit_pb2.PIECE,
                                                       amount_exact=1)),
            comment="testcomment")
        db_lib.insert_product(self._db, product)

        products = db_lib.load_products(self._db)
        assert len(products) == 1
        hamcrest.assert_that(products[0], proto_matcher.equals_proto(product))


if __name__ == '__main__':
    absltest.main()
