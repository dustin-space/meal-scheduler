import datetime
import os
import re
import subprocess
import tempfile
from typing import List, Optional, Tuple

from absl import logging
from google.protobuf import text_format
from meal_scheduler import error_reporter as error_reporter_lib
from meal_scheduler import time_util
from meal_scheduler.data import recipes
from meal_scheduler.data import stock as stock_lib
from meal_scheduler.diet_mapping_py_proto_pb import diet_mapping_pb2
from meal_scheduler.inventory_py_proto_pb import inventory_pb2
from meal_scheduler.plan import translator as translator_lib
from meal_scheduler.plan import validate as validate_lib
from meal_scheduler.plan.planner_py_proto_pb.plan import planner_pb2
from rules_python.python.runfiles import runfiles as runfiles_lib

##NONVEGAN_BITS = {
##    BITS_DICT['DAIRY'], BITS_DICT['HONEY'], BITS_DICT['MEAT'], BITS_DICT['FISH']
##}

# TODO: some recipes can be made vegan or non-vegan


def run_plan_cc(plan_input: planner_pb2.Input) -> planner_pb2.Output:
    fd, input_path = tempfile.mkstemp(suffix=".input.textproto")
    with os.fdopen(fd, "w") as input_f:
        input_f.write(text_format.MessageToString(plan_input))

    fd, output_path = tempfile.mkstemp(suffix=".output.textproto")
    os.close(fd)

    r = runfiles_lib.Create()
    path = r.Rlocation("meal_scheduler/plan/planner_cc")

    subprocess.check_call([
        path, "--input_textproto_path", input_path, "--output_textproto_path",
        output_path
    ])

    with open(output_path, 'r') as output_f:
        return text_format.Parse(output_f.read(), planner_pb2.Output())


def validate_date_proto(date_proto):
    if date_proto.year <= 1970:
        raise Exception("no year set in date proto, date proto probably unset")


def validate_plan_request(plan_request: inventory_pb2.PlanRequest):
    date_protos = [
        meal_request.date for meal_request in plan_request.meal_request
    ]
    for date_proto in date_protos:
        validate_date_proto(date_proto)
    dates = [
        time_util.date_proto_to_date(date_proto) for date_proto in date_protos
    ]
    if list(sorted(dates)) != dates:
        raise Exception(f"Dates are not sorted: {dates}")
    if any(date < datetime.date.today() for date in dates):
        raise Exception(f"Some dates are in the past")


def run_plan(
    s: stock_lib.StockDatabase,
    all_recipes,
    plan_request: inventory_pb2.PlanRequest,
    history: inventory_pb2.History,
    diet_map: diet_mapping_pb2.DietMappings,
    subs: inventory_pb2.Substitutions,
    products: inventory_pb2.Products,
    convs: inventory_pb2.Conversions,
    ingredient_database,
    force_same_quantum_for_connected_components: bool,
    backend=run_plan_cc,
    merchant_product_statuses: Optional[List[
        inventory_pb2.MerchantProductStatus]] = None
) -> inventory_pb2.Plan:
    validate_plan_request(plan_request)

    logging.info("Recipe count: %d", len(all_recipes))

    error_reporter = error_reporter_lib.ErrorReporter()

    translator = translator_lib.Translator(
        all_recipes,
        products,
        s,
        convs,
        error_reporter,
        ingredient_database,
        diet_map=diet_map,
        force_same_quantum_for_connected_components=(
            force_same_quantum_for_connected_components),
        merchant_product_statuses=merchant_product_statuses)
    plan_input = translator.translate_to_plan_input(plan_request, history)
    error_reporter.log_errors()

    validate_lib.validate_plan_input(plan_input)

    # TODO(agentydragon): minimize amount of needed cooking

    plan_output = backend(plan_input)
    if not plan_output.HasField('plan'):
        raise Exception("no solution found")
    return translator.translate_plan(plan_output.plan, plan_request)
