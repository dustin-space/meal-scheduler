import celery

CELERY_APP = celery.Celery('meal_scheduler.frontend',
                           include=['meal_scheduler.frontend.tasks'])
