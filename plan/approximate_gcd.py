import functools

from absl import logging


def represented_closely(gcd, x, tolerance):
    x_approx = (x // gcd) * gcd
    lower = 1.0 - tolerance / 2
    upper = 1.0 + tolerance / 2
    return lower <= (x_approx / x) <= upper


def approximate_gcd(inputs, tolerance=0.1):
    remainders = list(inputs)

    candidates = set()
    for divisor in [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16]:
        candidates = candidates.union(set(x / divisor for x in inputs))

    for candidate_gcd in reversed(sorted(candidates)):
        if all(
                represented_closely(candidate_gcd, x, tolerance)
                for x in remainders):
            # logging.info("all inputs are represented closely enough by %f",
            #              candidate_gcd)
            return candidate_gcd
        else:
            # logging.info("not all inputs closely represented by %f",
            #              candidate_gcd)
            pass

    while True:
        candidate_gcd = min(remainders)
        if all(
                represented_closely(candidate_gcd, x, tolerance)
                for x in remainders):
            # logging.info("all inputs are represented closely enough")
            return candidate_gcd

        remainders_new = []
        for x in remainders:
            if x == candidate_gcd:
                remainders_new.append(x)
            else:
                div, mod = divmod(x, candidate_gcd)
                # if mod != 0:
                # TODO(agentydragon): workaround for [32.46670894102727, 8.116677235256818, 12.5, 225.0]
                if mod > 0.0001:
                    remainders_new.append(mod)
        remainders = remainders_new
