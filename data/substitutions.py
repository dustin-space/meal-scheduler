from google.protobuf import text_format
from meal_scheduler.inventory_py_proto_pb import inventory_pb2

SUBSTITUTIONS_RUNFILES_PATH = 'meal_scheduler/data/substitutions.textproto'


def load_substitutions(runfiles):
    path = runfiles.Rlocation(SUBSTITUTIONS_RUNFILES_PATH)
    with open(path, 'rb') as f:
        return text_format.Parse(f.read(), inventory_pb2.Substitutions())
