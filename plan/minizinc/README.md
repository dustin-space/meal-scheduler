```bash
minizinc simple-planning.mzn simple-planning-data.dzn
```

Also: `--output-mode json` for machine-readable output.
